package me.windleafy.kity.android.utils;

import android.app.Activity;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.widget.Toast;

import com.blankj.utilcode.util.AppUtils;

import java.util.Date;


public class BackKit {

    private BackKit() {
        /**cannot be instantiated **/
        throw new UnsupportedOperationException("cannot be instantiated");
    }

    private static long mPreTime;

    /**
     * 双击退出程序
     *
     * @usage: Activity中调用
     *      @Override
     *     public boolean onKeyDown(int keyCode, KeyEvent event) {
     *         return BackKit.setDoubleBackPressedFinish(...);
     *     }
     */
    public static boolean setDoubleBackPressedFinish(Activity activity, String toastMsg, long time, int keyCode) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            long currentTime = new Date().getTime();
            // 如果时间间隔大于time, 不处理
            if ((currentTime - mPreTime) > time) {
                Toast.makeText(activity, TextUtils.isEmpty(toastMsg) ? "再按一次退出" : toastMsg, Toast.LENGTH_LONG).show();// 显示消息
                mPreTime = currentTime;// 更新时间
            } else {
                mPreTime = 0;
                activity.finish();
            }
        }
        return true;// 截获事件,不再处理
    }

    /**
     * 双击退出程序
     *
     * @usage: Activity中调用
     *      @Override
     *     public boolean onKeyDown(int keyCode, KeyEvent event) {
     *         return BackKit.setDoubleBackPressedFinish(...);
     *     }
     */
    public static boolean setDoubleBackPressedFinish(Call call, long time, int keyCode) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            long currentTime = new Date().getTime();
            // 如果时间间隔大于time, 不处理
            if ((currentTime - mPreTime) > time) {
                if (call != null) {
                    call.notice(null);
                }
                mPreTime = currentTime;// 更新时间
            } else {
                mPreTime = 0;
                if (call != null) {
                    call.finish(null);
                }
            }
        }
        return true;// 截获事件,不再处理
    }

    /**
     * 双击退出程序
     *
     * @usage: Activity中调用
     *      @Override
     *     public boolean onKeyDown(int keyCode, KeyEvent event) {
     *         return BackKit.setDoubleBackPressedFinish(keyCode);
     *     }
     */
    public static boolean setDoubleBackPressedFinish(int keyCode) {
        return setDoubleBackPressedFinish(new BackKit.Call() {
            @Override
            public void notice(Object o) {
                ToastKit.show("再按一次退出");
            }

            @Override
            public void finish(Object o) {
                AppUtils.exitApp();
            }
        }, 1000, keyCode);
    }

    /**
     * 双击退出程序
     *
     * @usage: Activity中调用
     *      @Override
     *     public void onBackPressed() {
     *         BackKit.setDoubleBackPressedFinish();
     *     }
     */
    public static boolean setDoubleBackPressedFinish() {
        return setDoubleBackPressedFinish(KeyEvent.KEYCODE_BACK);
    }

    public interface Call<T> {
        void notice(T t);

        void finish(T t);
    }


}
