package me.windleafy.kity.android.utils.log;

import android.util.Log;

/**
 * @description: 日志，打印多行
 * @author: YangYong
 * @sence: 2022/1/4
 * @version: 2.3
 */
public class LineLogKit {

    private LineLogKit() {
        /* cannot be instantiated */
        throw new UnsupportedOperationException("cannot be instantiated");
    }

    public static boolean isEnable = true;// 是否需要打印bug，可以在application的onCreate函数里面初始化

    private static boolean isLog = true;

    private static String PrintSeparator = " : ";

    private static final String TAG = "LineLogKit";


    public static void v(String... msg) {
        vg(null, null, msg);
    }

    public static void d(String... msg) {
        dg(null, null, msg);
    }

    public static void i(String... msg) {
        ig(null, null, msg);
    }

    public static void w(String... msg) {
        wg(null, null, msg);
    }

    public static void e(String... msg) {
        eg(null, null, msg);
    }

    public static void vg(String group, String... msg) {
        vg(null, group, msg);
    }

    public static void dg(String group, String... msg) {
        dg(null, group, msg);
    }

    public static void ig(String group, String... msg) {
        ig(null, group, msg);
    }

    public static void wg(String group, String... msg) {
        wg(null, group, msg);
    }

    public static void eg(String group, String... msg) {
        eg(null, group, msg);
    }

    public static void v(Class<?> clazz, String... msg) {
        vg(clazz, null, msg);
    }

    public static void d(Class<?> clazz, String... msg) {
        dg(clazz, null, msg);
    }

    public static void i(Class<?> clazz, String... msg) {
        ig(clazz, null, msg);
    }

    public static void w(Class<?> clazz, String... msg) {
        wg(clazz, null, msg);
    }

    public static void e(Class<?> clazz, String... msg) {
        eg(clazz, null, msg);
    }


    public static void vg(Class<?> clazz, String group, String... msg) {
        if (isEnable) {
            if (isLog) Log.v(getTag(clazz, group), getMsg(clazz, group, msg));
        }
    }

    public static void dg(Class<?> clazz, String group, String... msg) {
        if (isEnable) {
            if (isLog) Log.d(getTag(clazz, group), getMsg(clazz, group, msg));
        }
    }

    public static void ig(Class<?> clazz, String group, String... msg) {
        if (isEnable) {
            if (isLog) Log.i(getTag(clazz, group), getMsg(clazz, group, msg));
        }
    }

    public static void wg(Class<?> clazz, String group, String... msg) {
        if (isEnable) {
            if (isLog) Log.w(getTag(clazz, group), getMsg(clazz, group, msg));
        }
    }

    public static void eg(Class<?> clazz, String group, String... msg) {
        if (isEnable) {
            if (isLog) Log.e(getTag(clazz, group), getMsg(clazz, group, msg));
        }
    }


    private static String getTag(Class<?> clazz, String group) {
        return (clazz != null ? "C" : "")
                + (group != null ? "G" : "")
                + ((clazz == null && group == null) ? "D" : "")
                + TAG;
    }

    private static String getMsg(Class<?> clazz, String group, String... msg) {
        return (clazz != null ? "\n" + "<" + clazz.getSimpleName() + ">" + " " : " ")
                + (group != null ? "\n" + "[" + group + "]" + " " : " ")
                + line(msg);
    }

    private static String line(String[] messages) {
        StringBuilder stringBuffer = new StringBuilder();
        stringBuffer.append(" \n");
        for (String msg : messages) {
            stringBuffer.append("\n").append(msg);
        }
        return stringBuffer.toString();
    }
}
