package me.windleafy.kity.android.utils;

import android.view.View;
import android.widget.CheckBox;

/**
 * 多个CheckBox单选/多选设置
 */
public final class CheckBoxKit {

    private CheckBoxKit() {
        throw new UnsupportedOperationException("u can't instantiate me...");
    }


    /**
     * 单选监听
     */
    public interface OnSingleClickListener {
        /**
         * @param checkBox CheckBox
         * @param clickPos 点击位置
         */
        void onItemClick(CheckBox checkBox, int clickPos);
    }

    /**
     * 多选监听
     */
    public interface OnMultiClickListener {
        /**
         * @param checkBox   CheckBox
         * @param checkedNum 未点击时已选择
         * @param clickPos   点击位置
         */
        void onItemClick(CheckBox checkBox, int checkedNum, int clickPos);
    }

    /**
     * 单选模式
     *
     * @param onSingleClickListener
     * @param checkBoxList
     */
    public static void setSingleMode(OnSingleClickListener onSingleClickListener, CheckBox... checkBoxList) {
        for (int i = 0; i < checkBoxList.length; i++) {
            int finalI = i;
            CheckBox checkBox = checkBoxList[finalI];
            checkBox.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    //当前checkbox已改变状态
                    setCheckBoxList(false, checkBoxList);
                    checkBox.setChecked(true);
                    if (onSingleClickListener != null)
                        onSingleClickListener.onItemClick((CheckBox) view, finalI);
                }
            });
        }
    }

    /**
     * 单选模式
     *
     * @param checkedPos
     * @param onSingleClickListener
     * @param checkBoxList
     */
    public static void setSingleMode(int checkedPos, OnSingleClickListener onSingleClickListener, CheckBox... checkBoxList) {
        for (int i = 0; i < checkBoxList.length; i++) {
            int finalI = i;
            CheckBox checkBox = checkBoxList[finalI];
            checkBox.setChecked(checkedPos == i);
            checkBox.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    //当前checkbox已改变状态
                    setCheckBoxList(false, checkBoxList);
                    checkBox.setChecked(true);
                    if (onSingleClickListener != null)
                        onSingleClickListener.onItemClick((CheckBox) view, finalI);
                }
            });
        }
    }


    /**
     * 单选模式（可取消选择）
     *
     * @param onSingleClickListener
     * @param checkBoxList
     */
    public static void setSingleCancelMode(OnSingleClickListener onSingleClickListener, CheckBox... checkBoxList) {
        for (int i = 0; i < checkBoxList.length; i++) {
            int finalI = i;
            CheckBox checkBox = checkBoxList[finalI];
            checkBox.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    //当前checkbox已改变状态
                    boolean isChecked = checkBox.isChecked();
                    setCheckBoxList(false, checkBoxList);
                    checkBox.setChecked(isChecked);
                    if (onSingleClickListener != null)
                        onSingleClickListener.onItemClick((CheckBox) view, finalI);
                }
            });
        }
    }

    /**
     * 单选模式（可取消选择）
     *
     * @param checkedPos
     * @param onSingleClickListener
     * @param checkBoxList
     */
    public static void setSingleCancelMode(int checkedPos, OnSingleClickListener onSingleClickListener, CheckBox... checkBoxList) {
        for (int i = 0; i < checkBoxList.length; i++) {
            int finalI = i;
            CheckBox checkBox = checkBoxList[finalI];
            checkBox.setChecked(checkedPos == i);
            checkBox.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    //当前checkbox已改变状态
                    boolean isChecked = checkBox.isChecked();
                    setCheckBoxList(false, checkBoxList);
                    checkBox.setChecked(isChecked);
                    if (onSingleClickListener != null)
                        onSingleClickListener.onItemClick((CheckBox) view, finalI);
                }
            });
        }
    }

    /**
     * 多选模式
     *
     * @param min                  最小选择项(0或者1)
     * @param max                  最多选择项
     * @param onMultiClickListener
     * @param checkBoxList
     */
    public static void setMultipleMode(int min, int max, OnMultiClickListener onMultiClickListener, CheckBox... checkBoxList) {
        for (int i = 0; i < checkBoxList.length; i++) {
            int finalI = i;
            CheckBox checkBox = checkBoxList[finalI];
            checkBox.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    //已选择数，包含当前checkbox已改变状态
                    int checkedNum = getCheckedNum(checkBoxList);
                    if (checkedNum > max) {
                        checkBox.setChecked(false);
                    }
                    if (checkedNum < min) {
                        checkBox.setChecked(true);
                    }
                    if (onMultiClickListener != null)
                        onMultiClickListener.onItemClick((CheckBox) view, getCheckedNum(checkBoxList), finalI);
                }
            });
        }

    }

    /**
     * 设置所有CheckBox的状态
     *
     * @param check
     * @param checkBoxList
     */
    public static void setCheckBoxList(boolean check, CheckBox... checkBoxList) {
        for (CheckBox checkBox : checkBoxList) checkBox.setChecked(check);
    }

    /**
     * 获取已选中的个数
     *
     * @param checkBoxList
     * @return
     */
    public static int getCheckedNum(CheckBox... checkBoxList) {
        int checkedMun = 0;
        for (CheckBox checkBox : checkBoxList) if (checkBox.isChecked()) checkedMun++;
        return checkedMun;
    }

    /**
     * 设置pos为check状态，其它为!check状态
     *
     * @param pos
     * @param check
     * @param checkBoxList
     */
    public static void setCheckBoxList(int pos, boolean check, CheckBox... checkBoxList) {
        for (int i = 0; i < checkBoxList.length; i++) {
            CheckBox checkBox = checkBoxList[i];
            checkBox.setChecked((pos == i) == check);
        }
    }

}
