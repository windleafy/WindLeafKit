package me.windleafy.kity.android.utils;

import android.graphics.Point;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;

import me.windleafy.kity.java.judge.BoolKit;
import me.windleafy.kity.java.judge.EmptyKit;

/**
 * 跟App相关的辅助类
 */
public class ViewKit {

    private ViewKit() {
        /**cannot be instantiated **/
        throw new UnsupportedOperationException("cannot be instantiated");
    }

    public static class PARAMS {
        public static final int MATCH_PARENT = ViewGroup.LayoutParams.MATCH_PARENT;
        public static final int WRAP_CONTENT = ViewGroup.LayoutParams.WRAP_CONTENT;
    }

    /**
     * 在onCreate中获取view宽度
     *
     * @param view
     * @return
     */
    public static int getWrapWidthOnCreate(View view) {
        int w = View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED);
        int h = View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED);
        view.measure(w, h);
        return view.getMeasuredWidth();
    }

    /**
     * 在onCreate中获取view宽度
     *
     * @param view
     * @return
     */
    public static int getWrapHeightOnCreate(View view) {
        int w = View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED);
        int h = View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED);
        view.measure(w, h);
        return view.getMeasuredHeight();
    }


    /**
     * @param view
     * @param runnable
     */
    public static void getWidthHeightOnCreate(View view, final Runnable runnable) {
        view.post(runnable);
    }


    @Deprecated
    private static int getMode(int layoutParams) {
        if (layoutParams == PARAMS.MATCH_PARENT) {
            return View.MeasureSpec.AT_MOST;
        } else if (layoutParams == PARAMS.WRAP_CONTENT) {
            return View.MeasureSpec.UNSPECIFIED;
        }
        return 0;
    }


    /**
     * 设置宽高
     *
     * @param view
     * @param widthPx
     * @param heightPx
     */
    public static void setViewWidthHeight(View view, int widthPx, int heightPx) {
        ViewGroup.LayoutParams params = view.getLayoutParams();
        params.width = widthPx;
        params.height = heightPx;
        view.setLayoutParams(params);
    }

    /**
     * 设置坐标点，相对于父控件
     *
     * @param view
     * @param x
     * @param y
     */
    public static void setPositionInParent(View view, int x, int y) {
        view.setX(x);
        view.setY(y);
    }


    public static void setPositionInAppScreen(View view, int x, int y) {
        view.setTranslationX(x);
        view.setTranslationY(y);
    }

    public static void move(View view, int offsetX, int offsetY) {
        view.offsetLeftAndRight(offsetX);
        view.offsetTopAndBottom(offsetY);
    }


    /**
     * 获取在当前窗口内的绝对坐标,当前activity显示的大小
     *
     * @param view
     * @return [x, y]
     */
    public static Point getLocationInWindow(View view) {
        int[] location = new int[2];
        view.getLocationInWindow(location);
        return new Point(location[0], location[1]);
    }

    /**
     * 获取在整个屏幕内的绝对坐标,包括通知栏
     *
     * @param view
     * @return [x, y]
     */
    public static Point getLocationOnScreen(View view) {
        int[] location = new int[2];
        view.getLocationOnScreen(location);
        return new Point(location[0], location[1]);
    }


    public static void setLayoutInParent(View view, int l, int t, int r, int b) {
        view.layout(l, t, r, b);
    }


    /**
     * 设置view宽高（未验证）
     *
     * @param view
     * @param width
     * @param height
     * @param <LP>
     */
    @Deprecated
    public static <LP extends ViewGroup.LayoutParams> void setWidthHeight(View view, int width, int height) {
        LP lp = (LP) view.getLayoutParams();
        lp.width = width;
        lp.height = height;
        view.setLayoutParams(lp);
    }

    /**
     * 设置view宽（未验证）
     *
     * @param view
     * @param width
     * @param <LP>
     */
    public static <LP extends ViewGroup.LayoutParams> void setWidth(View view, int width) {
        LP lp = (LP) view.getLayoutParams();
        lp.width = width;
        view.setLayoutParams(lp);
    }

    /**
     * 设置view高（未验证）
     *
     * @param view
     * @param height
     * @param <LP>
     */
    public static <LP extends ViewGroup.LayoutParams> void setHeight(View view, int height) {
        LP lp = (LP) view.getLayoutParams();
        lp.height = height;
        view.setLayoutParams(lp);
    }

    /**
     * 获取在整个屏幕内的绝对坐标
     *
     * @param view
     * @return [x, y]
     */
    public static int[] getLocation(View view) {
        int[] location = new int[2];
        view.getLocationOnScreen(location);
        return location;
    }

    /**
     * view重绘时回调
     *
     * @param view
     * @param listener
     */
    public static void addOnDrawListener(View view, ViewTreeObserver.OnDrawListener listener) {
        view.getViewTreeObserver().addOnDrawListener(listener);
    }

    /**
     * view加载完成时回调
     *
     * @param view
     * @param listener
     */
    public static void addOnGlobalLayoutListener(View view, ViewTreeObserver.OnGlobalLayoutListener listener) {
        view.getViewTreeObserver().addOnGlobalLayoutListener(listener);
    }


    /**
     * 设置 可见Visible/不存在Gone
     */
    public static void setVisible(View view, Boolean bool) {
        view.setVisibility(VMKit.setVisible(bool));
    }

    public static void setVisible(View view, Boolean... bools) {
        view.setVisibility(VMKit.setVisible(BoolKit.allTrue(bools)));
    }

    public static void setVisible(View view, String param) {
        view.setVisibility(VMKit.setVisible(EmptyKit.notEmpty(param)));
    }

    public static void setVisible(View view, String param1, String param2) {
        view.setVisibility(VMKit.setVisible(param1, param2));
    }

    public static void setVisible(View view, String param1, int param2) {
        view.setVisibility(VMKit.setVisible(param1, param2));
    }

    public static void setVisible(View view, int param1, int param2) {
        view.setVisibility(VMKit.setVisible(param1, param2));
    }

    /**
     * 设置 不存在Gone/可见Visible
     */
    public static void setGone(View view, Boolean bool) {
        view.setVisibility(VMKit.setGone(bool));
    }

    public static void setGone(View view, Boolean... bools) {
        view.setVisibility(VMKit.setGone(BoolKit.allTrue(bools)));
    }

    public static void setGone(View view, String param) {
        view.setVisibility(VMKit.setGone(EmptyKit.notEmpty(param)));
    }

    public static void setGone(View view, String param1, String param2) {
        view.setVisibility(VMKit.setGone(param1, param2));
    }

    public static void setGone(View view, String param1, int param2) {
        view.setVisibility(VMKit.setGone(param1, param2));
    }

    public static void setGone(View view, int param1, int param2) {
        view.setVisibility(VMKit.setGone(param1, param2));
    }

    /**
     * 设置 可见Visible/不可见Invisible
     */
    public static void setVisibleOrNot(View view, Boolean visible) {
        view.setVisibility(VMKit.setVisibleOrNot(visible));
    }

    public static void setVisibleOrNot(View view, String param1, String param2) {
        view.setVisibility(VMKit.setVisibleOrNot(param1, param2));
    }

    public static void setVisibleOrNot(View view, String param1, int param2) {
        view.setVisibility(VMKit.setVisibleOrNot(param1, param2));
    }

    public static void setVisibleOrNot(View view, int param1, int param2) {
        view.setVisibility(VMKit.setVisibleOrNot(param1, param2));
    }

    /**
     * 设置 不可见Invisible/可见Visible
     */
    public static void setInvisible(View view, Boolean visible) {
        view.setVisibility(VMKit.setInvisible(visible));
    }

    public static void setInvisible(View view, String param1, String param2) {
        view.setVisibility(VMKit.setInvisible(param1, param2));
    }

    public static void setInvisible(View view, String param1, int param2) {
        view.setVisibility(VMKit.setInvisible(param1, param2));
    }

    public static void setInvisible(View view, int param1, int param2) {
        view.setVisibility(VMKit.setInvisible(param1, param2));
    }

    /**
     * 设置是否可见
     *
     * @param bool true：visible1， null or false：visible2
     */
    public static void setVisible(View view, Boolean bool, int visible1, int visible2) {
        view.setVisibility(bool != null && bool ? visible1 : visible2);
    }

    public static void setVisible(View view, String param1, String param2, int visible1, int visible2) {
        view.setVisibility(VMKit.equals(param1, param2) ? visible1 : visible2);
    }

    public static void setVisible(View view, String param1, int param2, int visible1, int visible2) {
        view.setVisibility(VMKit.equals(param1, param2) ? visible1 : visible2);
    }

    public static void setVisible(View view, int param1, int param2, int visible1, int visible2) {
        view.setVisibility(VMKit.equals(param1, param2) ? visible1 : visible2);
    }

}