package me.windleafy.kity.android.utils;

import android.content.Context;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.support.annotation.ColorInt;
import android.support.annotation.IntDef;
import android.text.Html;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.ForegroundColorSpan;
import android.text.style.StyleSpan;
import android.widget.TextView;


/**
 * @description: TextView相关工具类
 * @author: YangYong
 * @sence: 2021/10/26
 * @version: 2.3
 */
public class TextViewKit {

    @IntDef(value = {Direction.Left, Direction.Top, Direction.Right, Direction.Bottom})
    public @interface Direction {
        int Left = 1;
        int Top = 2;
        int Right = 3;
        int Bottom = 4;
    }


    private TextViewKit() {
        /**cannot be instantiated **/
        throw new UnsupportedOperationException("cannot be instantiated");
    }

    /**
     * 设置drawable
     *
     * @param context
     * @param textView
     * @param resId
     * @param direction
     */
    public static void setDrawable(Context context, TextView textView, int resId, @Direction int direction) {
        Drawable drawable = context.getResources().getDrawable(resId);
        drawable.setBounds(0, 0, drawable.getMinimumWidth(), drawable.getMinimumHeight());
        setDrawable(textView, drawable, direction);
    }

    /**
     * 设置drawable
     *
     * @param textView
     * @param drawable
     * @param direction
     */
    public static void setDrawable(TextView textView, Drawable drawable, @Direction int direction) {
        textView.setCompoundDrawables(
                direction == Direction.Left ? drawable : null,
                direction == Direction.Top ? drawable : null,
                direction == Direction.Right ? drawable : null,
                direction == Direction.Bottom ? drawable : null
        );
    }

    /**
     * 设置有颜色的TextView
     *
     * @param textView
     * @param textBefore
     * @param textButton    按钮文字
     * @param textAfter
     * @param buttonColor   按钮颜色
     * @param clickableSpan 点击监听
     */
    public static void setSpannableString(TextView textView, String textBefore, String textButton, String textAfter, @ColorInt int buttonColor, ClickableSpan clickableSpan) {
        final SpannableStringBuilder builder = new SpannableStringBuilder();
        //设置文字
        builder.append(textBefore).append(textButton).append(textAfter);
        //设置部分文字点击事件
        builder.setSpan(clickableSpan, textBefore.length(), (textBefore + textButton).length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        //设置部分文字颜色
        ForegroundColorSpan foregroundColorSpan = new ForegroundColorSpan(buttonColor);
        builder.setSpan(foregroundColorSpan, textBefore.length(), (textBefore + textButton).length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        //配置给TextView
        textView.setMovementMethod(LinkMovementMethod.getInstance());
        textView.setText(builder);
//        setSpannableString(textView, textBefore + textButton + textAfter, textBefore.length(), (textBefore + textButton).length(), color, clickableSpan);
    }

    /**
     * 设置有颜色的TextView
     *
     * @param textView
     * @param text          所有文字
     * @param buttonStart   按钮起始
     * @param buttonEnd     按钮截止
     * @param buttonColor   按钮颜色
     * @param clickableSpan 点击监听
     */
    public static void setSpannableString(TextView textView, String text, int buttonStart, int buttonEnd, @ColorInt int buttonColor, ClickableSpan clickableSpan) {
        final SpannableStringBuilder builder = new SpannableStringBuilder();
        //设置文字
        builder.append(text);
        //设置部分文字点击事件
        builder.setSpan(clickableSpan, buttonStart, buttonEnd, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        //设置部分文字颜色
        ForegroundColorSpan foregroundColorSpan = new ForegroundColorSpan(buttonColor);
        builder.setSpan(foregroundColorSpan, buttonStart, buttonEnd, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        //配置给TextView
        textView.setMovementMethod(LinkMovementMethod.getInstance());
        textView.setText(builder);
    }


    /**
     * 字体加粗
     *
     * @param textView
     * @param isBold
     */
    public static void setTextBold(TextView textView, Boolean isBold) {
        textView.getPaint().setFakeBoldText(isBold);
    }

    /**
     * 字体加粗
     */
    public static SpannableString getSpannableStringBold(String str, boolean bold) {
        SpannableString spStr = new SpannableString(str);
        StyleSpan styleSpan = new StyleSpan(bold ? Typeface.BOLD : Typeface.NORMAL);
        spStr.setSpan(styleSpan, 0, str.length(), Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
        return spStr;

    }

    /**
     * 设置html文字
     *
     * @param textView
     * @param htmlText h5格式的字符串
     */
    public static void setHtmlText(TextView textView, String htmlText) {
//        textView.setText(Jsoup.parse(StringKit.valid(htmlText)).text());
        textView.setText(Html.fromHtml(htmlText));
    }

}