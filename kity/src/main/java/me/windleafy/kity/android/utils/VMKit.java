package me.windleafy.kity.android.utils;

import android.arch.lifecycle.MutableLiveData;
import android.view.View;

import java.util.List;

import me.windleafy.kity.java.judge.BoolKit;
import me.windleafy.kity.java.judge.CompareKit;
import me.windleafy.kity.java.judge.EmptyKit;

/**
 * @description: VM相关工具类
 * @author: YangYong
 * @sence: 2021/1/4
 * @version: 2.0
 * @usage: 使用方法
 * data中导入：<import type="com.benwunet.base.kit.VMKit" />
 * xml中的控件设置：android:visibility="@{VMKit.setVisibleOrGone(viewModel.xxx, 0)}"
 */
public class VMKit {

    private VMKit() {
        /**cannot be instantiated **/
        throw new UnsupportedOperationException("cannot be instantiated");
    }

    /**
     * 刷新liveData，绑定相应数据的界面会更新
     */
    public static <T> void refreshLiveData(MutableLiveData<T> liveData) {
        liveData.setValue(liveData.getValue());
    }

    public static class Visible {

    }

    public static class Resource {

    }

    /**
     * 显示字符串
     */
    public static String string(int param) {
        return String.valueOf(param);
    }

    /**
     * 显示字符串
     */
    public static String string(float param) {
        return String.valueOf(param);
    }


    /**
     * 设置 可见Visible/不存在Gone
     */
    public static int setVisible(Boolean bool) {
        return setVisible(bool, View.VISIBLE, View.GONE);
    }

    public static int setVisible(Boolean... bools) {
        return setVisible(BoolKit.allTrue(bools), View.VISIBLE, View.GONE);
    }

    public static int setVisibleHasTrue(Boolean... bools) {
        return setVisible(BoolKit.containTrue(bools), View.VISIBLE, View.GONE);
    }

    public static int setVisible(String param) {
        return setVisible(EmptyKit.notEmpty(param), View.VISIBLE, View.GONE);
    }

    public static int setVisible(String param1, String param2) {
        return setVisible(param1, param2, View.VISIBLE, View.GONE);
    }

    public static int setVisible(String param1, int param2) {
        return setVisible(param1, param2, View.VISIBLE, View.GONE);
    }

    public static int setVisible(int param1, int param2) {
        return setVisible(param1, param2, View.VISIBLE, View.GONE);
    }

    /**
     * 设置 不存在Gone/可见Visible
     */
    public static int setGone(Boolean bool) {
        return setVisible(bool, View.GONE, View.VISIBLE);
    }

    public static int setGone(Boolean... bools) {
        return setVisible(BoolKit.allTrue(bools), View.GONE, View.VISIBLE);
    }

    public static int setGone(String param) {
        return setVisible(EmptyKit.notEmpty(param), View.GONE, View.VISIBLE);
    }

    public static int setGone(String param1, String param2) {
        return setVisible(param1, param2, View.GONE, View.VISIBLE);
    }

    public static int setGone(String param1, int param2) {
        return setVisible(param1, param2, View.GONE, View.VISIBLE);
    }

    public static int setGone(int param1, int param2) {
        return setVisible(param1, param2, View.GONE, View.VISIBLE);
    }

    /**
     * 设置 可见Visible/不可见Invisible
     */
    public static int setVisibleOrNot(Boolean visible) {
        return setVisible(visible, View.VISIBLE, View.INVISIBLE);
    }

    public static int setVisibleOrNot(String param) {
        return setVisible(EmptyKit.notEmpty(param), View.VISIBLE, View.INVISIBLE);
    }

    public static int setVisibleOrNot(String param1, String param2) {
        return setVisible(param1, param2, View.VISIBLE, View.INVISIBLE);
    }

    public static int setVisibleOrNot(String param1, int param2) {
        return setVisible(param1, param2, View.VISIBLE, View.INVISIBLE);
    }

    public static int setVisibleOrNot(int param1, int param2) {
        return setVisible(param1, param2, View.VISIBLE, View.INVISIBLE);
    }

    /**
     * 设置 不可见Invisible/可见Visible
     */
    public static int setInvisible(Boolean visible) {
        return setVisible(visible, View.INVISIBLE, View.VISIBLE);
    }

    public static int setInvisible(String param1, String param2) {
        return setVisible(param1, param2, View.INVISIBLE, View.VISIBLE);
    }

    public static int setInvisible(String param1, int param2) {
        return setVisible(param1, param2, View.INVISIBLE, View.VISIBLE);
    }

    public static int setInvisible(int param1, int param2) {
        return setVisible(param1, param2, View.INVISIBLE, View.VISIBLE);
    }

    /**
     * 设置是否可见
     *
     * @param bool true：visible1， null or false：visible2
     */
    public static int setVisible(Boolean bool, int visible1, int visible2) {
        return bool != null && bool ? visible1 : visible2;
    }

    public static int setVisible(String param1, String param2, int visible1, int visible2) {
        return equals(param1, param2) ? visible1 : visible2;
    }

    public static int setVisible(String param1, int param2, int visible1, int visible2) {
        return equals(param1, param2) ? visible1 : visible2;
    }

    public static int setVisible(int param1, int param2, int visible1, int visible2) {
        return equals(param1, param2) ? visible1 : visible2;
    }

    /**
     * 设置字符不为空时可见Visible / 设置字符为空时不存在Gone
     */
    public static int setVisibleWhenNotEmpty(String str) {
        return setVisible(notEmpty(str));
    }

    public static int setVisibleWhenOneNotEmpty(String... strings) {
        return setVisible(!EmptyKit.isEmpty(strings));
    }

    public static int setVisibleWhenAllNotEmpty(String... strings) {
        return setVisible(EmptyKit.notEmpty(strings));
    }

    /**
     * 设置字符为空时可见Visible / 设置字符不为空时不存在Gone
     */
    public static int setVisibleWhenIsEmpty(String str) {
        return setVisible(isEmpty(str));
    }

    public static int setVisibleWhenOneIsEmpty(String... strings) {
        return setVisible(!EmptyKit.notEmpty(strings));
    }

    public static int setVisibleWhenAllIsEmpty(String... strings) {
        return setVisible(EmptyKit.isEmpty(strings));
    }

    /**
     * 设置字符长度是否满足时可见Visible/不存在Gone
     */
    public static int setVisibleLessThan(String str, int length) {
        return setVisible(str != null && str.length() < length, View.VISIBLE, View.GONE);
    }

    public static int setVisibleMoreThan(String str, int length) {
        return setVisible(str != null && str.length() > length, View.VISIBLE, View.GONE);
    }

    /**
     * 判断是否相等
     *
     * @param param1
     * @param param2
     * @return
     */
    public static boolean equals(String param1, String param2) {
        if (param1 == null || param2 == null) return false;
        return param1.equals(param2);
    }

    public static boolean equals(String param1, int param2) {
        if (param1 == null) return false;
        return param1.equals(param2 + "");
    }

    public static boolean equals(int param1, int param2) {
        return param1 == param2;
    }

    public static boolean notEquals(String param1, String param2) {
        return !equals(param1, param2);
    }

    public static boolean notEquals(String param1, int param2) {
        return !equals(param1, param2);
    }

    public static boolean notEquals(int param1, int param2) {
        return !equals(param1, param2);
    }

    /**
     * 判断是否包含
     *
     * @param param
     * @param params
     * @return
     */
    public static boolean contain(String param, String... params) {
        if (param == null || params == null) return false;
        return CompareKit.contain(param, params);
    }

    public static boolean contain(String param, Integer... params) {
        if (param == null) return false;
        return CompareKit.contain(Integer.valueOf(param), params);
    }

    public static boolean contain(Integer param, Integer... params) {
        return CompareKit.contain(param, params);
    }


    /**
     * 空判断
     */
    public static boolean notEmpty(String str) {
        return !isEmpty(str);
    }


    public static boolean isEmpty(String str) {
        return str == null || str.length() == 0;
    }

    public static <T> boolean notEmpty(List<T> list) {
        return !isEmpty(list);
    }

    public static <T> boolean isEmpty(List<T> list) {
        if (list == null || list.size() == 0) {
            return true;
        }
        return false;
    }



}