package me.windleafy.kity.android.utils;

import android.widget.EditText;

/**
 * @description: TextView相关工具类
 * @author: YangYong
 * @sence: 2021/10/26
 * @version: 2.3
 */
public class EditTextKit {

    private EditTextKit() {
        /**cannot be instantiated **/
        throw new UnsupportedOperationException("cannot be instantiated");
    }

    /**
     * 取消焦点
     */
    public void clearEditTextCursor(EditText editText) {
        editText.clearFocus();
//        View root = activity.getWindow().getDecorView().findViewById(android.R.id.content);
//        root.setFocusable(true);
//        root.setFocusableInTouchMode(true);
    }

}