package me.windleafy.kity.android.utils.log;

import android.util.Log;

/**
 * @description: 日志，用于生命周期
 * @author: YangYong
 * @sence: 2022/1/4
 * @version: 2.3
 */
public class LifeLogKit {

    private LifeLogKit() {
        /* cannot be instantiated */
        throw new UnsupportedOperationException("cannot be instantiated");
    }

    public static boolean isEnable = false;// 是否需要打印bug，可以在application的onCreate函数里面初始化

    private static boolean isLog = true;

    private static String PrintSeparator = " : ";

    private static final String TAG = "LifeLogKit";


    public static void v(String msg) {
        v(null, null, msg);
    }

    public static void d(String msg) {
        d(null, null, msg);
    }

    public static void i(String msg) {
        i(null, null, msg);
    }

    public static void w(String msg) {
        w(null, null, msg);
    }

    public static void e(String msg) {
        e(null, null, msg);
    }


    public static void v(String group, String msg) {
        v(null, group, msg);
    }

    public static void d(String group, String msg) {
        d(null, group, msg);
    }

    public static void i(String group, String msg) {
        i(null, group, msg);
    }

    public static void w(String group, String msg) {
        w(null, group, msg);
    }

    public static void e(String group, String msg) {
        e(null, group, msg);
    }


    public static void v(Class<?> clazz, String msg) {
        v(clazz, null, msg);
    }

    public static void d(Class<?> clazz, String msg) {
        d(clazz, null, msg);
    }

    public static void i(Class<?> clazz, String msg) {
        i(clazz, null, msg);
    }

    public static void w(Class<?> clazz, String msg) {
        w(clazz, null, msg);
    }

    public static void e(Class<?> clazz, String msg) {
        e(clazz, null, msg);
    }


    public static void v(Class<?> clazz, String group, String msg) {
        if (isEnable) {
            if (isLog) Log.v(getTag(clazz, group), getMsg(clazz, group, msg));
        }
    }

    public static void d(Class<?> clazz, String group, String msg) {
        if (isEnable) {
            if (isLog) Log.d(getTag(clazz, group), getMsg(clazz, group, msg));
        }
    }

    public static void i(Class<?> clazz, String group, String msg) {
        if (isEnable) {
            if (isLog) Log.i(getTag(clazz, group), getMsg(clazz, group, msg));
        }
    }

    public static void w(Class<?> clazz, String group, String msg) {
        if (isEnable) {
            if (isLog) Log.w(getTag(clazz, group), getMsg(clazz, group, msg));
        }
    }

    public static void e(Class<?> clazz, String group, String msg) {
        if (isEnable) {
            if (isLog) Log.e(getTag(clazz, group), getMsg(clazz, group, msg));
        }
    }


    private static String getTag(Class<?> clazz, String group) {
        return (clazz != null ? "C" : "")
                + (group != null ? "G" : "")
                + ((clazz == null && group == null) ? "D" : "")
                + TAG;
    }

    private static String getMsg(Class<?> clazz, String group, String msg) {
        return (clazz != null ? "<" + clazz.getSimpleName() + ">" + " " : " ")
                + (group != null ? "[" + group + "]" + " " : " ")
                + msg;
    }

}
