package me.windleafy.kity.android.utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.widget.ImageView;


import com.bumptech.glide.GenericTransitionOptions;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.bitmap.CenterCrop;
import com.bumptech.glide.load.resource.bitmap.FitCenter;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.bumptech.glide.request.RequestOptions;

import java.util.concurrent.ExecutionException;

import me.windleafy.kity.R;

/**
 * @description: Glide控件
 * @author: YangYong
 * @sence: 2021/2/6
 * @version: 2.0
 */
public class GlideKit {

    /**
     * 加载圆形图片
     */
    public static void loadCircleCrop(ImageView imageView, String url) {
        loadThumb(imageView, url, new RequestOptions().circleCrop());
    }

    /**
     * 加载图片
     */
    public static void loadFitCenter(ImageView imageView, String url) {
        loadThumb(imageView, url, new RequestOptions().fitCenter());
    }

    /**
     * 加载图片
     */
    public static void loadFitCenter(ImageView imageView, String url, int cornerRadius) {
        loadThumb(imageView, url, new RequestOptions().transforms(new FitCenter(), new RoundedCorners(cornerRadius)));
    }

    /**
     * 加载图片
     */
    public static void loadCenterCrop(ImageView imageView, String url) {
        loadThumb(imageView, url, new RequestOptions().centerCrop());
    }

    /**
     * 加载图片
     */
    public static void loadCenterCrop(ImageView imageView, String url, int cornerRadius) {
        loadThumb(imageView, url, new RequestOptions().transforms(new CenterCrop(), new RoundedCorners(cornerRadius)));
    }

    /**
     * 加载图片
     */
    public static void loadCenterInside(ImageView imageView, String url) {
        loadThumb(imageView, url, new RequestOptions().centerInside());
    }

    /**
     * 加载图片，缓存所有
     */
    public static void loadFitCache(ImageView imageView, String url) {
        loadThumb(imageView, url, new RequestOptions().fitCenter().diskCacheStrategy(DiskCacheStrategy.ALL));
    }

    /**
     * 加载图片，缓存所有
     */
    public static void loadCache(ImageView imageView, String url) {
        loadThumb(imageView, url, new RequestOptions().diskCacheStrategy(DiskCacheStrategy.ALL));
    }

    /**
     * 加载视频第一帧
     */
    public static void loadVideoFitCache(ImageView imageView, String url) {
        load(imageView, url, new RequestOptions().frame(1).fitCenter().diskCacheStrategy(DiskCacheStrategy.ALL));
    }

    /**
     * 加载图片，缩略
     */
    public static void loadThumb(ImageView imageView, String url) {
        loadThumb(imageView, url, 0.1f);
    }

    /**
     * 加载图片，缩略，带参数
     */
    public static void loadThumb(ImageView imageView, String url, float thumbFloat) {
        load(imageView, url, thumbFloat, new RequestOptions());
    }

    /**
     * 加载图片，缩略，带参数
     */
    public static void loadThumb(ImageView imageView, String url, RequestOptions options) {
        load(imageView, url, 0.1f, options);
    }


    /**
     * 加载图片
     */
    public static void load(ImageView imageView, String url) {
        if (url == null || url.trim().equals("")) return;
        Glide.with(imageView.getContext())
                .load(url)
                .transition(GenericTransitionOptions.with(R.anim.fade_in))
                .into(imageView);
    }

    /**
     * 加载图片，带参数
     */
    public static void load(ImageView imageView, String url, RequestOptions options) {
        if (url == null || url.trim().equals("")) return;
        Glide.with(imageView.getContext())
                .load(url)
                .apply(options)
                .transition(GenericTransitionOptions.with(R.anim.fade_in))
                .into(imageView);
    }

    /**
     * 加载图片，缩略，带参数
     */
    public static void load(ImageView imageView, String url, Float thumbFloat, RequestOptions options) {
        if (url == null || url.trim().equals("")) return;
        Glide.with(imageView.getContext())
                .load(url)
                .apply(options)
                .thumbnail(thumbFloat)
                .transition(GenericTransitionOptions.with(R.anim.fade_in))
                .into(imageView);
    }


    /**
     * 通过URL得到 Drawable
     * 这是一个耗时的操作需要异步处理
     */
    public static Drawable getDrawable(Context context, String url) {
        try {
            return Glide.with(context)
                    .load(url)
                    .submit()
                    .get();
        } catch (ExecutionException | InterruptedException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 通过URL得到 Bitmap
     * 这是一个耗时的操作需要异步处理
     */
    public static Bitmap getBitmap(Context context, String url) {
        try {
            return Glide.with(context)
                    .asBitmap()
                    .load(url)
                    .submit()
                    .get();
        } catch (ExecutionException | InterruptedException e) {
            e.printStackTrace();
        }
        return null;
    }


    //Glide中的大部分设置项都可以通过 RequestOptions 类和 apply() 方法来应用到程序中。
    //RequestOptions options = new RequestOptions()
    //                .placeholder(R.mipmap.ic_launcher)                //加载成功之前占位图
    //                .error(R.mipmap.ic_launcher)                    //加载错误之后的错误图
    //                .override(400,400)                                //指定图片的尺寸
    //                //指定图片的缩放类型为fitCenter （等比例缩放图片，宽或者是高等于ImageView的宽或者是高。）
    //                .fitCenter()
    //                //指定图片的缩放类型为centerCrop （等比例缩放图片，直到图片的宽高都大于等于ImageView的宽度，然后截取中间的显示。）
    //                .centerCrop()
    //                .circleCrop()//指定图片的缩放类型为centerCrop （圆形）
    //                .skipMemoryCache(true)                            //跳过内存缓存
    //                .diskCacheStrategy(DiskCacheStrategy.ALL)        //缓存所有版本的图像
    //                .diskCacheStrategy(DiskCacheStrategy.NONE)        //跳过磁盘缓存
    //                .diskCacheStrategy(DiskCacheStrategy.DATA)        //只缓存原来分辨率的图片
    //                .diskCacheStrategy(DiskCacheStrategy.RESOURCE)    //只缓存最终的图片
    public void usage() {
    }
}
