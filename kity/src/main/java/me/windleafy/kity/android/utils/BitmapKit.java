package me.windleafy.kity.android.utils;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.net.Uri;
import android.os.Environment;
import android.view.View;
import android.webkit.WebView;

import com.blankj.utilcode.util.ActivityUtils;
import com.blankj.utilcode.util.AppUtils;
import com.blankj.utilcode.util.ScreenUtils;

import java.io.File;
import java.io.FileOutputStream;

/**
 * @description: View相关工具类
 * @author: YangYong
 * @sence: 2021/1/4
 * @version: 2.0
 */
public class BitmapKit {

    private BitmapKit() {
        /**cannot be instantiated **/
        throw new UnsupportedOperationException("cannot be instantiated");
    }

    /**
     * 获取bitmap
     * @param drawableId
     * @return
     */
    public static Bitmap getBitmap(int drawableId) {
        if (drawableId == 0) {
            return null;
        }
       return BitmapFactory.decodeResource(ActivityUtils.getTopActivity().getResources(), drawableId);
    }

    /**
     * 获取控件截图（View或ViewGroup类型都可以获取到）
     */
    public static Bitmap shotView(View v) {
        if (v == null) {
            return null;
        }
        v.setDrawingCacheEnabled(true);
        v.buildDrawingCache();
        return v.getDrawingCache();
    }

    /**
     * 获取WebView控件截图（无效）
     */
    public static Bitmap shotWebView(WebView webView) {
        if (webView == null) {
            return null;
        }
        webView.measure(View.MeasureSpec.makeMeasureSpec(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED),
                View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED));
        webView.layout(0, 0, webView.getMeasuredWidth(), webView.getMeasuredHeight());
        webView.setDrawingCacheEnabled(true);
        webView.buildDrawingCache();
        Bitmap longImage = Bitmap.createBitmap(webView.getMeasuredWidth(),
                webView.getMeasuredHeight(), Bitmap.Config.ARGB_8888);

        Canvas canvas = new Canvas(longImage);  // 画布的宽高和 WebView 保持一致
        Paint paint = new Paint();
        canvas.drawBitmap(longImage, 0, webView.getMeasuredHeight(), paint);
        webView.draw(canvas);

        return longImage;
    }

    /**
     * 获取当前屏幕截图，
     *
     * @param containStatus 截图是否包含状态栏
     */
    public static Bitmap shotScreen(Activity activity, boolean containStatus) {
        //通过window的源码可以看出：检索顶层窗口的装饰视图，可以作为一个窗口添加到窗口管理器
        View view = activity.getWindow().getDecorView();
        //SYSTEM_UI_FLAG_FULLSCREEN表示全屏的意思，也就是会将状态栏隐藏
        //设置系统UI元素的可见性
        view.setSystemUiVisibility(View.SYSTEM_UI_FLAG_FULLSCREEN);
        //启用或禁用绘图缓存
        view.setDrawingCacheEnabled(true);
        //创建绘图缓存
        view.buildDrawingCache();
        //拿到绘图缓存
        Bitmap bitmap = view.getDrawingCache();

        Rect frame = new Rect();
        activity.getWindow().getDecorView().getWindowVisibleDisplayFrame(frame);

        //以上代码是去掉状态栏的截屏，如果不想去的话，bp = Bitmap.createScaledBitmap(bitmap, width, height - statusBarHeight,true);这里，height不要减去statusBarHeight即可。

        //状态栏高度
        int statusBarHeight = containStatus ? 0 : frame.top;
        int width = ScreenUtils.getScreenWidth();
        int height = ScreenUtils.getScreenHeight();

        Bitmap bp = null;
//        bp = Bitmap.createBitmap(bitmap, 0, 0, width, height - statusBarHeight);
        bp = Bitmap.createScaledBitmap(bitmap, width, height - statusBarHeight, true);
        view.destroyDrawingCache();
        view.setSystemUiVisibility(View.VISIBLE);
        return bp;
    }

    /**
     * 保存图片
     */
    public static boolean saveBitmap(Activity activity, Bitmap bitmap) {
        return saveBitmap(activity, bitmap, null, null, null);
    }

    /**
     * 保存图片
     *
     * @param activity
     * @param bitmap
     * @param storePath
     * @param fileName
     * @param quality   保存质量（1-100）
     * @return
     */
    public static boolean saveBitmap(Activity activity, Bitmap bitmap, String storePath, String fileName, Integer quality) {
        if (bitmap == null) {
            return false;
        }
        if (storePath == null) {
            storePath = Environment.getExternalStorageDirectory().getAbsolutePath() + File.separator + "benwunet";
        }
        File appDir = new File(storePath);
        if (!appDir.exists()) {
            appDir.mkdir();
        }

        if (fileName == null) {
            fileName = AppUtils.getAppName() + System.currentTimeMillis() + ".jpg";
        }
        File file = new File(appDir, fileName);

        try {
            FileOutputStream fos = new FileOutputStream(file);
            //通过io流的方式来压缩保存图片
            boolean isSuccess = bitmap.compress(Bitmap.CompressFormat.PNG, quality != null ? quality : 80, fos);
            fos.flush();
            fos.close();

            //保存图片后发送广播通知更新数据库
            Uri uri = Uri.fromFile(file);
            activity.getApplicationContext().sendBroadcast(new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, uri));

            return isSuccess;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    /**
     * 下载文件
     * 下载图片
     */
    @Deprecated
    public static boolean downloadBitmap(Activity activity, String url, String storePath, String fileName) {
        return false;
    }


}