package me.windleafy.kity.android.base.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;

import com.billy.android.swipe.SmartSwipe;
import com.billy.android.swipe.SmartSwipeBack;
import com.billy.android.swipe.SwipeConsumer;

import me.windleafy.kity.android.manager.ActivityController;
import me.windleafy.kity.android.utils.log.LogKit;
import me.yokeyword.fragmentation.SupportActivity;


public abstract class BaseSupportActivity extends SupportActivity {

    public Activity mActivity;
    public Context mContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        LogKit.e("Life Activity ----> "+getClass().getSimpleName());
        log("onCreate");
        super.onCreate(savedInstanceState);
        ActivityController.addActivity(this);
        mActivity = this;
        mContext = this;
        //activity侧滑返回
//        SmartSwipe.wrap(this)
//                .addConsumer(new ActivitySlidingBackConsumer(this))
//                .setRelativeMoveFactor(0.5F)
//                .enableLeft();

//        SmartSwipe.wrap(this)
//                .removeAllConsumers()
//                .addConsumer(new ActivitySlidingBackConsumer(this))
//                .setRelativeMoveFactor(0.2F)
//                .enableAllDirections()
//                .as(ActivitySlidingBackConsumer.class)
//        ;
//        SmartSwipeBack.activitySlidingBack(getApplication(), null);    //仿微信带联动效果的透明侧滑返回

        // edgeSize：边缘触发区域尺寸像素值（dp需转换为px），若设置为0，则表示整个activity区域都可触发
        // direction：开启侧滑的方向，可设置为上下左右中的一个或多个，其取值可参考侧滑方向，为0则不会触发侧滑
        // scrimColor: 玻璃颜色，显示在前一个activity之上的半透明遮罩颜色值, 默认为透明色
        // shadowColor: 在当前activity移动的边缘显示的阴影颜色，默认为透明色
        // shadowSize: shadowColor显示的大小像素值，默认为10dp
        // factor: 关联移动系数
        //        0:         前一个activity保持不动，当前activity随着手势滑动而平移，移动后可透视前一个activity
        //        (0,1):     前一个activity与当前activity有关联移动效果，具体效果如上图所示
        //        1:         前一个activity跟随当前activity一起平移（类似于ViewPager的默认平移效果）
        SmartSwipeBack.activitySlidingBack(getApplication(), null
                , 0, 0x80000000, 0x80000000,
                SmartSwipe.dp2px(10, this), 0.5f, SwipeConsumer.DIRECTION_LEFT);
    }

    @Override
    protected void onStart() {
        log("onStart");
        super.onStart();
    }

    @Override
    protected void onRestart() {
        log("onRestart");
        super.onRestart();
    }

    @Override
    protected void onResume() {
        log("onResume");
        super.onResume();
    }

    @Override
    protected void onPause() {
        log("onPause");
        super.onPause();
    }

    @Override
    protected void onStop() {
        log("onStop");
        super.onStop();
        if (isFinishing())
            releaseResource();
    }

    @Override
    protected void onDestroy() {
        log("onDestroy");
        super.onDestroy();
        ActivityController.removeActivity(this);
    }

    /**
     * 释放资源
     */
    protected void releaseResource() {

    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        log("onSaveInstanceState");
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onSaveInstanceState(Bundle outState, PersistableBundle outPersistentState) {
        log("onSaveInstanceState");
        super.onSaveInstanceState(outState, outPersistentState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        log("onRestoreInstanceState");
        super.onRestoreInstanceState(savedInstanceState);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        log("onCreateOptionsMenu");
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        log("onOptionsItemSelected");
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        log("onActivityResult");
        super.onActivityResult(requestCode, resultCode, data);
    }


    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        log("onKeyDown");
//        if (keyCode == KeyEvent.KEYCODE_BACK && event.getAction() == KeyEvent.ACTION_DOWN) {
//            doDouubleBackClickAction();
//        }
        return super.onKeyDown(keyCode, event);
    }


    // 是否双击退出
    private boolean isDoubleClickExitEnable = false;
    private DoubleBackClick mDoubleBackClick;
    private long mDoubleBackClickLastMillis;
    private long mDoubleBackClickDiff;

    /**
     * 是否设置双击退出
     *
     * @param doubleBackClick 回调
     */
    protected void startDoubleBackClick(DoubleBackClick doubleBackClick) {
        setDoubleBackClick(doubleBackClick, 1500);
    }

    /**
     * 设置双击退出
     * <p>
     * 设置：在需要双击退出的地方设置，例如onKeyDown，onBackPressed中
     *
     * @param doubleBackClick 回调
     * @param diff
     */
    protected void setDoubleBackClick(DoubleBackClick doubleBackClick, long diff) {
        //设置参数
        isDoubleClickExitEnable = true;
        mDoubleBackClick = doubleBackClick;
        mDoubleBackClickDiff = diff;
        //执行
        doDouubleBackClickAction();
    }


    /**
     * 双击返回事件
     */
    protected void doDouubleBackClickAction() {
        //是否执行双击退出
        if (isDoubleClickExitEnable) {
            if ((System.currentTimeMillis() - mDoubleBackClickLastMillis) > mDoubleBackClickDiff) {
                mDoubleBackClickLastMillis = System.currentTimeMillis();
                //提示再按一次后退键退出程序
                if (mDoubleBackClick != null) {
                    mDoubleBackClick.onToast();
                }
            } else {
                //退出需要执行的代码
                if (mDoubleBackClick != null)
                    mDoubleBackClick.onExited();
            }
        }

    }

    public interface DoubleBackClick {
        /**
         * 提示再按一次后退键退出程序
         */
        void onToast();

        /**
         * 退出需要执行的代码
         */
        void onExited();
    }

    /**
     * 日志
     *
     * @param msg
     */
    private void log(String msg) {
//        if (((BaseApplication) getApplication()).isLog()) {
//            //终端Log
//            CatLog.d(getClass().getSimpleName(), msg);
//        }
    }

}
