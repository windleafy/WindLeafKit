package me.windleafy.kity.android.utils;

import com.blankj.utilcode.util.ToastUtils;

/**
 * Toast统一管理类【可用】
 */
public class ToastKit {

    private ToastKit() {
        /** cannot be instantiated**/
        throw new UnsupportedOperationException("cannot be instantiated");
    }

    public static boolean isShow = true;

    public static void show(CharSequence message) {
        if (isShow) {
            ToastUtils.showShort(message);
        }
    }

    public static void show(int message) {
        if (isShow) {
            ToastUtils.showShort(message);
        }
    }

    public static void showLong(CharSequence message) {
        if (isShow) {
            ToastUtils.showLong(message);
        }
    }

    public static void showLong(int message) {
        if (isShow) {
            ToastUtils.showLong(message);
        }
    }


}
