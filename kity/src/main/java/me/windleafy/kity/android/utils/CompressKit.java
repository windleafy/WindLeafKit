package me.windleafy.kity.android.utils;

import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.provider.OpenableColumns;
import android.text.TextUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * @description: 图片压缩工具类
 * @author: YangYong
 * @sence: 2021/11/25
 * @version: 2.3
 */
public class CompressKit {

    private CompressKit() {
        /**cannot be instantiated **/
        throw new UnsupportedOperationException("cannot be instantiated");
    }

    public static File compress(Context context, File oldFile) {
        return CompressHelper.getDefault(context).compressToFile(oldFile);
    }


    public static File compress(Context context, File oldFile, String newFileName) {
        return new CompressHelper.Builder(context)
                .setMaxWidth(720)  // 默认最大宽度为720
                .setMaxHeight(960) // 默认最大高度为960
                .setQuality(80)    // 默认压缩质量为80
                .setFileName(newFileName) // 文件名称
                .setCompressFormat(Bitmap.CompressFormat.JPEG) // 设置默认压缩为jpg格式
                .setDestinationDirectoryPath(Environment.getExternalStoragePublicDirectory(
                        Environment.DIRECTORY_PICTURES).getAbsolutePath())//路径
                .build()
                .compressToFile(oldFile);
    }


    /**
     * https://www.cnblogs.com/zhangqie/p/9815140.html
     */
    public static class CompressHelper {
        private static volatile CompressHelper INSTANCE;

        private Context context;
        /**
         * 最大宽度，默认为720
         */
        private float maxWidth = 720.0f;
        /**
         * 最大高度,默认为960
         */
        private float maxHeight = 960.0f;
        /**
         * 默认压缩后的方式为JPEG
         */
        private Bitmap.CompressFormat compressFormat = Bitmap.CompressFormat.JPEG;

        /**
         * 默认的图片处理方式是ARGB_8888
         */
        private Bitmap.Config bitmapConfig = Bitmap.Config.ARGB_8888;
        /**
         * 默认压缩质量为80
         */
        private int quality = 80;
        /**
         * 存储路径
         */
        private String destinationDirectoryPath;
        /**
         * 文件名前缀
         */
        private String fileNamePrefix;
        /**
         * 文件名
         */
        private String fileName;

        public static CompressHelper getDefault(Context context) {
            if (INSTANCE == null) {
                synchronized (CompressHelper.class) {
                    if (INSTANCE == null) {
                        INSTANCE = new CompressHelper(context);
                    }
                }
            }
            return INSTANCE;
        }


        private CompressHelper(Context context) {
            this.context = context;
            destinationDirectoryPath = context.getCacheDir().getPath() + File.pathSeparator + "CompressHelper";
        }

        /**
         * 压缩成文件
         *
         * @param file 原始文件
         * @return 压缩后的文件
         */
        public File compressToFile(File file) {
            return compressImage(context, Uri.fromFile(file), maxWidth, maxHeight,
                    compressFormat, bitmapConfig, quality, destinationDirectoryPath,
                    fileNamePrefix, fileName);
        }

        /**
         * 压缩为Bitmap
         *
         * @param file 原始文件
         * @return 压缩后的Bitmap
         */
        public Bitmap compressToBitmap(File file) {
            return getScaledBitmap(context, Uri.fromFile(file), maxWidth, maxHeight, bitmapConfig);
        }

        Bitmap getScaledBitmap(Context context, Uri imageUri, float maxWidth, float maxHeight, Bitmap.Config bitmapConfig) {
            String filePath = getRealPathFromURI(context, imageUri);
            Bitmap scaledBitmap = null;

            BitmapFactory.Options options = new BitmapFactory.Options();

            //by setting this field as true, the actual bitmap pixels are not loaded in the memory. Just the bounds are loaded. If
            //you try the use the bitmap here, you will get null.
            options.inJustDecodeBounds = true;
            Bitmap bmp = BitmapFactory.decodeFile(filePath, options);
            if (bmp == null) {
                InputStream inputStream = null;
                try {
                    inputStream = new FileInputStream(filePath);
                    BitmapFactory.decodeStream(inputStream, null, options);
                    inputStream.close();
                } catch (FileNotFoundException exception) {
                    exception.printStackTrace();
                } catch (IOException exception) {
                    exception.printStackTrace();
                }
            }

            int actualHeight = options.outHeight;
            int actualWidth = options.outWidth;

            if (actualHeight == -1 || actualWidth == -1) {
                try {
                    ExifInterface exifInterface = new ExifInterface(filePath);
                    actualHeight = exifInterface.getAttributeInt(ExifInterface.TAG_IMAGE_LENGTH, ExifInterface.ORIENTATION_NORMAL);//获取图片的高度
                    actualWidth = exifInterface.getAttributeInt(ExifInterface.TAG_IMAGE_WIDTH, ExifInterface.ORIENTATION_NORMAL);//获取图片的宽度
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            if (actualWidth <= 0 || actualHeight <= 0) {
                Bitmap bitmap2 = BitmapFactory.decodeFile(filePath);
                if (bitmap2 != null) {
                    actualWidth = bitmap2.getWidth();
                    actualHeight = bitmap2.getHeight();
                } else {
                    return null;
                }
            }

            float imgRatio = (float) actualWidth / actualHeight;
            float maxRatio = maxWidth / maxHeight;

            //width and height values are set maintaining the aspect ratio of the image
            if (actualHeight > maxHeight || actualWidth > maxWidth) {
                if (imgRatio < maxRatio) {
                    imgRatio = maxHeight / actualHeight;
                    actualWidth = (int) (imgRatio * actualWidth);
                    actualHeight = (int) maxHeight;
                } else if (imgRatio > maxRatio) {
                    imgRatio = maxWidth / actualWidth;
                    actualHeight = (int) (imgRatio * actualHeight);
                    actualWidth = (int) maxWidth;
                } else {
                    actualHeight = (int) maxHeight;
                    actualWidth = (int) maxWidth;
                }
            }

            //setting inSampleSize value allows to load a scaled down version of the original image
            options.inSampleSize = calculateInSampleSize(options, actualWidth, actualHeight);

            //inJustDecodeBounds set to false to load the actual bitmap
            options.inJustDecodeBounds = false;

            //this options allow android to claim the bitmap memory if it runs low on memory
            options.inPurgeable = true;
            options.inInputShareable = true;
            options.inTempStorage = new byte[16 * 1024];

            try {
                // load the bitmap getTempFile its path
                bmp = BitmapFactory.decodeFile(filePath, options);
                if (bmp == null) {
                    InputStream inputStream = null;
                    try {
                        inputStream = new FileInputStream(filePath);
                        BitmapFactory.decodeStream(inputStream, null, options);
                        inputStream.close();
                    } catch (IOException exception) {
                        exception.printStackTrace();
                    }
                }
            } catch (OutOfMemoryError exception) {
                exception.printStackTrace();
            }
            if (actualHeight <= 0 || actualWidth <= 0) {
                return null;
            }

            try {
                scaledBitmap = Bitmap.createBitmap(actualWidth, actualHeight, bitmapConfig);
            } catch (OutOfMemoryError exception) {
                exception.printStackTrace();
            }

            float ratioX = actualWidth / (float) options.outWidth;
            float ratioY = actualHeight / (float) options.outHeight;

            Matrix scaleMatrix = new Matrix();
            scaleMatrix.setScale(ratioX, ratioY, 0, 0);

            Canvas canvas = new Canvas(scaledBitmap);
            canvas.setMatrix(scaleMatrix);
            canvas.drawBitmap(bmp, 0, 0, new Paint(Paint.FILTER_BITMAP_FLAG));

            // 采用 ExitInterface 设置图片旋转方向
            ExifInterface exif;
            try {
                exif = new ExifInterface(filePath);
                int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, 0);
                Matrix matrix = new Matrix();
                if (orientation == 6) {
                    matrix.postRotate(90);
                } else if (orientation == 3) {
                    matrix.postRotate(180);
                } else if (orientation == 8) {
                    matrix.postRotate(270);
                }
                scaledBitmap = Bitmap.createBitmap(scaledBitmap, 0, 0,
                        scaledBitmap.getWidth(), scaledBitmap.getHeight(),
                        matrix, true);
            } catch (IOException e) {
                e.printStackTrace();
            }

            return scaledBitmap;
        }


        /**
         * 获取真实的路径
         *
         * @param context 上下文
         * @param uri     uri
         * @return 文件路径
         */
        static String getRealPathFromURI(Context context, Uri uri) {
            Cursor cursor = context.getContentResolver().query(uri, null, null, null, null);
            if (cursor == null) {
                return uri.getPath();
            } else {
                cursor.moveToFirst();
                int index = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
                String realPath = cursor.getString(index);
                cursor.close();
                return realPath;
            }
        }


        /**
         * 计算inSampleSize
         */
        private static int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
            final int height = options.outHeight;
            final int width = options.outWidth;
            int inSampleSize = 1;

            if (height > reqHeight || width > reqWidth) {
                final int heightRatio = Math.round((float) height / (float) reqHeight);
                final int widthRatio = Math.round((float) width / (float) reqWidth);
                inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;
            }

            final float totalPixels = width * height;
            final float totalReqPixelsCap = reqWidth * reqHeight * 2;

            while (totalPixels / (inSampleSize * inSampleSize) > totalReqPixelsCap) {
                inSampleSize++;
            }

            return inSampleSize;
        }

        File compressImage(Context context, Uri imageUri, float maxWidth, float maxHeight,
                           Bitmap.CompressFormat compressFormat, Bitmap.Config bitmapConfig,
                           int quality, String parentPath, String prefix, String fileName) {
            FileOutputStream out = null;
            String filename = generateFilePath(context, parentPath, imageUri, compressFormat.name().toLowerCase(), prefix, fileName);
            try {
                out = new FileOutputStream(filename);
                // 通过文件名写入
                Bitmap newBmp = getScaledBitmap(context, imageUri, maxWidth, maxHeight, bitmapConfig);
                if (newBmp != null) {
                    newBmp.compress(compressFormat, quality, out);
                }

            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } finally {
                try {
                    if (out != null) {
                        out.close();
                    }
                } catch (IOException ignored) {
                }
            }

            return new File(filename);
        }

        private static String generateFilePath(Context context, String parentPath, Uri uri,
                                               String extension, String prefix, String fileName) {
            File file = new File(parentPath);
            if (!file.exists()) {
                file.mkdirs();
            }
            /** if prefix is null, set prefix "" */
            prefix = TextUtils.isEmpty(prefix) ? "" : prefix;
            /** reset fileName by prefix and custom file name */
            fileName = TextUtils.isEmpty(fileName) ? prefix + splitFileName(getFileName(context, uri))[0] : fileName;
            return file.getAbsolutePath() + File.separator + fileName + "." + extension;
        }

        /**
         * 获取文件名称
         *
         * @param context 上下文
         * @param uri     uri
         * @return 文件名称
         */
        static String getFileName(Context context, Uri uri) {
            String result = null;
            if (uri.getScheme().equals("content")) {
                Cursor cursor = context.getContentResolver().query(uri, null, null, null, null);
                try {
                    if (cursor != null && cursor.moveToFirst()) {
                        result = cursor.getString(cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    if (cursor != null) {
                        cursor.close();
                    }
                }
            }
            if (result == null) {
                result = uri.getPath();
                int cut = result.lastIndexOf(File.separator);
                if (cut != -1) {
                    result = result.substring(cut + 1);
                }
            }
            return result;
        }

        /**
         * 截取文件名称
         *
         * @param fileName 文件名称
         */
        static String[] splitFileName(String fileName) {
            String name = fileName;
            String extension = "";
            int i = fileName.lastIndexOf(".");
            if (i != -1) {
                name = fileName.substring(0, i);
                extension = fileName.substring(i);
            }

            return new String[]{name, extension};
        }

        /**
         * 采用建造者模式，设置Builder
         */
        public static class Builder {
            private CompressHelper mCompressHelper;

            public Builder(Context context) {
                mCompressHelper = new CompressHelper(context);
            }

            /**
             * 设置图片最大宽度
             *
             * @param maxWidth 最大宽度
             */
            public Builder setMaxWidth(float maxWidth) {
                mCompressHelper.maxWidth = maxWidth;
                return this;
            }

            /**
             * 设置图片最大高度
             *
             * @param maxHeight 最大高度
             */
            public Builder setMaxHeight(float maxHeight) {
                mCompressHelper.maxHeight = maxHeight;
                return this;
            }

            /**
             * 设置压缩的后缀格式
             */
            public Builder setCompressFormat(Bitmap.CompressFormat compressFormat) {
                mCompressHelper.compressFormat = compressFormat;
                return this;
            }

            /**
             * 设置Bitmap的参数
             */
            public Builder setBitmapConfig(Bitmap.Config bitmapConfig) {
                mCompressHelper.bitmapConfig = bitmapConfig;
                return this;
            }

            /**
             * 设置压缩质量，建议80
             *
             * @param quality 压缩质量，[0,100]
             */
            public Builder setQuality(int quality) {
                mCompressHelper.quality = quality;
                return this;
            }

            /**
             * 设置目的存储路径
             *
             * @param destinationDirectoryPath 目的路径
             */
            public Builder setDestinationDirectoryPath(String destinationDirectoryPath) {
                mCompressHelper.destinationDirectoryPath = destinationDirectoryPath;
                return this;
            }

            /**
             * 设置文件前缀
             *
             * @param prefix 前缀
             */
            public Builder setFileNamePrefix(String prefix) {
                mCompressHelper.fileNamePrefix = prefix;
                return this;
            }

            /**
             * 设置文件名称
             *
             * @param fileName 文件名
             */
            public Builder setFileName(String fileName) {
                mCompressHelper.fileName = fileName;
                return this;
            }

            public CompressHelper build() {
                return mCompressHelper;
            }
        }
    }

}