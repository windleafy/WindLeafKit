package me.windleafy.kity.java.collection;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.TreeSet;

import me.windleafy.kity.java.judge.EmptyKit;

/**
 * @description: 列表工具类
 * @author: YangYong
 * @sence: 2021/1/19
 * @version: 2.0
 */
public class ListKit {

    @SuppressWarnings("unchecked")
    public static <T> List<T> convert(List<?> list) {
        return (List<T>) list;
    }

    /**
     * 创建list
     *
     * @param items
     * @param <T>
     * @return
     */
    public static <T> List<T> newList(T... items) {
        return new ArrayList<>(Arrays.asList(items));
    }


    /**
     * 列表类型转换
     *
     * @param <OLD> 原类型
     * @param <NEW> 新类型
     */
    public interface ItemTransform<OLD, NEW> {
        /**
         * 类型转换
         *
         * @param oldItem 原类型
         * @param index
         * @return 新类型
         */
        NEW transform(OLD oldItem, int index);
    }

    /**
     * 列表项类型转换
     *
     * @param oldList   原列表
     * @param transform 原类型OLD转新类型NEW
     * @param <OLD>     原列表item类型
     * @param <NEW>     新列表item类型
     * @return
     */
    public static <OLD, NEW> List<NEW> convertList(List<OLD> oldList, ItemTransform<OLD, NEW> transform) {
        if (oldList == null) {
            return null;
        }
        List<NEW> newList;
        try {
            newList = oldList.getClass().newInstance();
        } catch (IllegalAccessException | InstantiationException e) {
            e.printStackTrace();
            //暂时处理Arrays.asList(arrays)返回值传入oldList的报错：java.lang.Class<java.util.Arrays$ArrayList> is not accessible from java.lang...
            newList = new ArrayList<>();
        }
        for (int i = 0; i < oldList.size(); i++) {
            newList.add(transform.transform(oldList.get(i), i));
        }
        return newList;
    }

    /**
     * 列表项类型转换
     *
     * @param oldList   原列表
     * @param transform 原类型OLD转新类型NEW
     * @param <OLD>     原列表item类型
     * @param <NEW>     新列表item类型
     * @return
     */
    public static <OLD, NEW> ArrayList<NEW> convertArrayList(List<OLD> oldList, ItemTransform<OLD, NEW> transform) {
        if (oldList == null) {
            return null;
        }
        ArrayList<NEW> newList = new ArrayList<>();
        for (int i = 0; i < oldList.size(); i++) {
            newList.add(transform.transform(oldList.get(i), i));
        }
        return newList;
    }

    /**
     * list转为arraylist
     *
     * @param oldList
     * @param <T>
     * @return
     */
    public static <T> ArrayList<T> convertArrayList(List<T> oldList) {
        if (oldList == null) {
            return null;
        }
        ArrayList<T> newList = new ArrayList<>();
        for (int i = 0; i < oldList.size(); i++) {
            newList.add(oldList.get(i));
        }
        return newList;
    }


    /**
     * 数组转列表：Arrays.asList(arrays);
     *
     * @param arrays
     * @param <T>
     * @return
     */
    public static <T> List<T> split(T... arrays) {
        return Arrays.asList(arrays);
    }

    /**
     * 带分隔符的字符串转化为数组
     *
     * @param listString
     * @param split
     * @return
     */
    public static List<String> split(String listString, String split) {
        if (listString == null) {
            return new ArrayList<>();
        }
        return Arrays.asList(listString.split(split));
    }

    /**
     * 将数组转化为字符串，项之间加入分隔符
     *
     * @param list  字符串列表
     * @param split 分隔符
     * @return
     * @usage: ListKit.toString(list, ", ")
     */
    public static String convertString(List<String> list, String split) {
        StringBuilder stringBuffer = new StringBuilder();
        for (String str : list) {
            stringBuffer.append(split).append(str);
        }
        return stringBuffer.toString().replaceFirst(split, "");
    }


    /**
     * 去重
     */
    public static <T> List<T> delRepeat(List<T> list) {
        return new ArrayList<>(new TreeSet<>(list));
    }

    /**
     * 匹配条件
     *
     * @param <T>
     */
    public interface Matcher<T> {
        boolean match(T t);
    }

    /**
     * 匹配条件
     *
     * @param <T1>
     * @param <T2>
     */
    public interface Matcher2<T1, T2> {
        boolean match(T1 t1, T2 t2);
    }

    /**
     * 创建新返回对象
     *
     * @param <T1>
     */
    public interface NewItem<RET, T1> {
        RET create(T1 t1);
    }

    /**
     * 创建新返回对象
     *
     * @param <T1>
     * @param <T2>
     */
    public interface NewItem2<RET, T1, T2> {
        RET create(T1 t1, T2 t2);
    }


    /**
     * 获取index
     *
     * @param list
     * @param matcher
     * @param <T1>
     * @return
     */
    public static <T1, T2> int index(List<T1> list, T2 item, Matcher2<T1, T2> matcher) {
        for (int index = 0; index < list.size(); index++) {
            T1 t = list.get(index);
            if (matcher.match(t, item)) {
                return index;
            }
        }
        return -1;
    }

    /**
     * 获取index
     *
     * @param list
     * @param matcher
     * @param <T>
     * @return
     */
    public static <T> int index(List<T> list, Matcher<T> matcher) {
        for (int index = 0; index < list.size(); index++) {
            T t = list.get(index);
            if (matcher.match(t)) {
                return index;
            }
        }
        return -1;
    }

    /**
     * 获取符合条件的item
     *
     * @param list
     * @param matcher
     * @param <T>
     * @return
     */
    public static <T> T getMatchItem(List<T> list, Matcher<T> matcher) {
        for (T t : list) {
            if (matcher.match(t)) {
                return t;
            }
        }
        return null;
    }

    /**
     * 获取符合条件的item
     *
     * @param list1
     * @param item
     * @param matcher
     * @param <T1>
     * @param <T2>
     * @return
     */
    public static <T1, T2> T1 getMatchItem(List<T1> list1, T2 item, Matcher2<T1, T2> matcher) {
        for (T1 t : list1) {
            if (matcher.match(t, item)) {
                return t;
            }
        }
        return null;
    }

    /**
     * 获取符合条件的item
     *
     * @param list
     * @param matcher
     * @param <T>
     * @return
     */
    public static <T> List<T> getMatchList(List<T> list, Matcher<T> matcher) {
        List<T> newList = new ArrayList<>();
        for (T t : list) {
            if (matcher.match(t)) {
                newList.add(t);
            }
        }
        return newList;
    }


    /**
     * 获取符合条件的item
     *
     * @param list1
     * @param item
     * @param matcher
     * @param <T1>
     * @param <T2>
     * @return
     */
    public static <T1, T2> List<T1> getMatchList(List<T1> list1, T2 item, Matcher2<T1, T2> matcher) {
        List<T1> newList = new ArrayList<>();
        for (T1 t : list1) {
            if (matcher.match(t, item)) {
                newList.add(t);
            }
        }
        return newList;
    }

    /**
     * 查找符合条件的list
     * 将list1中的每个数据与list2做匹配，返回匹配成功的集合
     *
     * @param list1
     * @param list2
     * @param matcher
     * @param <T1>
     * @param <T2>
     * @return
     */
    public static <T1, T2> List<T1> getMatchList(List<T1> list1, List<T2> list2, Matcher2<T1, T2> matcher) {
        List<T1> newList = new ArrayList<>();
        for (T1 t1 : list1) {
            for (T2 t2 : list2) {
                if (matcher.match(t1, t2)) {
                    newList.add(t1);
                    break;
                }
            }
        }
        return newList;
    }


    /**
     * 查找符合条件的list
     *
     * @param list1
     * @param list2
     * @param matcher
     * @param newItem
     * @param <RET>
     * @param <T1>
     * @param <T2>
     * @return
     */
    public static <RET, T1, T2> List<RET> getMatchList(List<T1> list1, List<T2> list2, Matcher2<T1, T2> matcher, NewItem2<RET, T1, T2> newItem) {
        List<RET> newList = new ArrayList<>();
        for (T1 t1 : list1) {
            for (T2 t2 : list2) {
                if (matcher.match(t1, t2)) {
                    newList.add(newItem.create(t1, t2));
                    break;
                }
            }
        }
        return newList;
    }


    /**
     * 删除符合条件的list
     *
     * @param list1
     * @param list2
     * @param matcher
     * @param <T1>
     * @param <T2>
     * @return
     */
    public static <RET, T1, T2> List<RET> removeMatchList(List<T1> list1, List<T2> list2, Matcher2<T1, T2> matcher, NewItem<RET, T1> newItem) {
        List<RET> newList = new ArrayList<>();
        for (T1 t1 : list1) {
            if (!isMatchItem(list2, t1, (m2, m1) -> matcher.match(m1, m2))) {
                newList.add(newItem.create(t1));
            }
        }
        return newList;
    }

    /**
     * 删除符合条件的list
     *
     * @param list1
     * @param list2
     * @param matcher
     * @param <T1>
     * @param <T2>
     * @return
     */
    public static <T1, T2> List<T1> removeMatchList(List<T1> list1, List<T2> list2, Matcher2<T1, T2> matcher) {
        List<T1> newList = new ArrayList<>();
        for (T1 t1 : list1) {
            if (!isMatchItem(list2, t1, (m2, m1) -> matcher.match(m1, m2))) {
                newList.add(t1);
            }
        }
        return newList;
    }


    /**
     * 删除符合条件的item
     *
     * @param list1
     * @param item
     * @param matcher
     * @param <T1>
     * @param <T2>
     * @return
     */
    public static <T1, T2> List<T1> removeMatchItem(List<T1> list1, T2 item, Matcher2<T1, T2> matcher) {
        List<T1> newList = new ArrayList<>();
        for (T1 t : list1) {
            if (!matcher.match(t, item)) {
                newList.add(t);
            }
        }
        return newList;
    }


    /**
     * list去重（相邻去重，不相邻不能判断）
     *
     * @param list
     * @param matcher 判断条件
     * @param oldItem 非重复项
     * @param newItem 重复项
     * @param <RET>
     * @param <T>
     * @return
     */
    public static <RET, T> List<RET> removeAdjoinDuplication(List<T> list, Matcher2<T, T> matcher, NewItem<RET, T> oldItem, NewItem2<RET, RET, T> newItem) {
        List<RET> newList = new ArrayList<>();
        for (int index = 0; index < list.size(); index++) {
            T t = list.get(index);
            if (index == 0 || !isMatchItem(list.subList(0, index), t, matcher)) {
                newList.add(oldItem.create(t));
            } else {
                int lastIndex = newList.size() - 1;
                RET retItem = newList.get(lastIndex);
                newList.set(lastIndex, newItem.create(retItem, t));
            }
        }
        return newList;
    }

    /**
     * list去重（相邻去重，不相邻不能判断）
     *
     * @param list
     * @param matcher
     * @param <T>
     * @return
     */
    public static <T> List<T> removeAdjoinDuplication(List<T> list, Matcher2<T, T> matcher) {
        List<T> newList = new ArrayList<>();
        for (int index = 0; index < list.size(); index++) {
            T t = list.get(index);
            if (index == 0 || !isMatchItem(list.subList(0, index), t, matcher)) {
                newList.add(t);
            }
        }
        return newList;
    }

    /**
     * 判断是否符合条件
     *
     * @param list
     * @param matcher
     * @param <T>
     * @return
     */
    public static <T> boolean isMatchItem(List<T> list, Matcher<T> matcher) {
        for (T t : list) {
            if (matcher.match(t)) {
                return true;
            }
        }
        return false;
    }

    /**
     * 判断是否符合条件
     *
     * @param list1
     * @param item
     * @param matcher
     * @param <T1>
     * @param <T2>
     * @return
     */
    public static <T1, T2> boolean isMatchItem(List<T1> list1, T2 item, Matcher2<T1, T2> matcher) {
        for (T1 t : list1) {
            if (matcher.match(t, item)) {
                return true;
            }
        }
        return false;
    }


    /**
     * 有效的子list
     *
     * @param list
     * @param start
     * @param end
     * @param <T>
     * @return
     */
    public static <T> List<T> validSubList(List<T> list, int start, int end) {
        return list.subList(start, Math.min(list.size(), end));
    }

    /**
     * 获取子list，数量为list.size()和size中的最小值
     *
     * @param list
     * @param maxSize
     * @param <T>
     * @return
     */
    public static <T> List<T> validSubList(List<T> list, int maxSize) {
        return list.subList(0, Math.min(list.size(), maxSize));
    }

    public interface EachItem<T> {
        void item(T item, int index);
    }

    public interface EachMoreItem<T> {
        void item(T currentItem, T lastItem, T nextItem, int index);
    }

    /**
     * for循环
     *
     * @param list
     * @param eachItem
     * @param <T>
     */
    public static <T> void forLoop(List<T> list, EachItem<T> eachItem) {
        if (EmptyKit.notEmpty(list)) {
            int index = 0;
            for (T t : list) {
                eachItem.item(t, index++);
            }
        }
    }

    /**
     * for循环
     *
     * @param list
     * @param eachItem
     * @param <T>
     */
    public static <T> void forLoop(List<T> list, EachMoreItem<T> eachItem) {
        if (EmptyKit.notEmpty(list)) {
            for (int i = 0; i < list.size(); i++) {
                T lastItem = i > 0 ? list.get(i - 1) : null;
                T nextItem = (i < list.size() - 1) ? list.get(i + 1) : null;
                eachItem.item(list.get(i), lastItem, nextItem, i);
            }
        }
    }


    /**
     * 移动元素
     *
     * @param list      列表
     * @param fromIndex 删除元素的位置
     * @param toIndex   删除后的list中加入的位置
     * @param <T>
     */
    public static <T> void moveItem(List<T> list, int fromIndex, int toIndex) {
        T t = list.remove(fromIndex);
        list.add(toIndex, t);
    }
}
