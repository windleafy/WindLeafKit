package me.windleafy.kity.java.utils;

import java.util.ArrayList;
import java.util.List;

/**
 * @description: 匹配过滤
 * @author: YangYong
 * @sence: 2020/12/26
 * @version: 2.0
 */
public class MatchKit {

    private MatchKit() {
        /**cannot be instantiated **/
        throw new UnsupportedOperationException("cannot be instantiated");
    }


    /**
     * 匹配条件
     *
     * @param <T>
     */
    public interface Matcher<T> {
        boolean match(T item);
    }

    /**
     * 模糊匹配条件
     *
     * @param <T>
     */
    public interface FurryMatcher<T> {
        boolean match(T item, String str);
    }

    /**
     * 查找匹配项
     *
     * @param list
     * @param matcher
     * @param <T>
     * @return
     */
    public static <T> List<T> getMatchList(List<T> list, Matcher<T> matcher) {
        List<T> newList = new ArrayList<>();
        for (T item : list) {
            if (matcher.match(item)) {
                newList.add(item);
            }
        }
        return newList;
    }

    /**
     * 查找字符串模糊匹配项，匹配一个则满足
     *
     * @param <T>
     * @param list
     * @param matcher
     * @return
     */
    public static <T> List<T> getOneMatchList(List<T> list, String[] fuzzyArray, FurryMatcher<T> matcher) {
        List<T> newList = new ArrayList<>();
        for (T item : list) {
            if (isOneMatch(item, fuzzyArray, matcher)) {
                newList.add(item);
            }
        }
        return newList;
    }

    /**
     * 查找字符串模糊匹配项,匹配所有则满足
     *
     * @param <T>
     * @param list
     * @param matcher
     * @return
     */
    public static <T> List<T> getAllMatchList(List<T> list, String[] fuzzyArray, FurryMatcher<T> matcher) {
        List<T> newList = new ArrayList<>();
        for (T item : list) {
            if (isAllMatch(item, fuzzyArray, matcher)) {
                newList.add(item);
            }
        }
        return newList;
    }

    /**
     * 模糊匹配对象，匹配一个则满足
     *
     * @param <T>
     * @param item
     * @param fuzzyArray
     * @param matcher
     * @return
     */
    public static <T> boolean isOneMatch(T item, String[] fuzzyArray, FurryMatcher<T> matcher) {
        for (String fuzzy : fuzzyArray) {
            if (matcher.match(item, fuzzy.trim())) {
                return true;
            }
        }
        return false;
    }

    /**
     * 模糊匹配对象,匹配所有则满足
     *
     * @param <T>
     * @param item
     * @param fuzzyArray
     * @param matcher
     * @return
     */
    public static <T> boolean isAllMatch(T item, String[] fuzzyArray, FurryMatcher<T> matcher) {
        for (String fuzzy : fuzzyArray) {
            if (!matcher.match(item, fuzzy.trim())) {
                return false;
            }
        }
        return true;
    }







}