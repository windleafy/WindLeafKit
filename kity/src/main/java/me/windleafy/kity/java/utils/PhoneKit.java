package me.windleafy.kity.java.utils;

import android.text.TextUtils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

public class PhoneKit {

    public static boolean isPhoneNumber(String str) throws PatternSyntaxException {
//        String regExp = "^1(3[0-9]|4[56789]|5[0-9]|6[6]|7[0-9]|8[0-9]|9[189])\\d{8}$";
        String regExp = "^1[345789]\\d{9}$";
        Pattern p = Pattern.compile(regExp);
        Matcher m = p.matcher(str);
        return m.matches();
    }

    /**
     * 获取加密手机号
     */
    public static String phoneSecret(String phoneNumber) {
        return phoneSecret(phoneNumber, true);
    }

    public static String phoneSecret(String phoneNumber, boolean isPhoneSecret) {
        if (!TextUtils.isEmpty(phoneNumber)) {
            if (isPhoneSecret && phoneNumber.length() == 11)
                return phoneNumber.substring(0, 3) + "****" + phoneNumber.substring(7, 11);
            else
                return phoneNumber;
        }
        return "";
    }

}
