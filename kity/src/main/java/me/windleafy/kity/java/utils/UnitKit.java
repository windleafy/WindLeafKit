package me.windleafy.kity.java.utils;

/**
 * 单位换算
 */
public class UnitKit {

    private UnitKit() {
        /**cannot be instantiated **/
        throw new UnsupportedOperationException("cannot be instantiated");
    }


    /**
     * 获取文件大小
     */
    public static String fileSize(long sizeB) {
        if (sizeB < 1024f) {
            return fileSizeB(sizeB);
        } else if (sizeB < 1048576f) {
            return fileSizeKB(sizeB);
        } else if (sizeB < 1073741824f) {
            return fileSizeMB(sizeB);
        } else if (sizeB < 1099511627776f) {
            return fileSizeGB(sizeB);
        } else {
            return fileSizeTB(sizeB);
        }
    }

    public static String fileSizeB(long sizeB) {
        return fileSizeB(sizeB, "B");
    }

    public static String fileSizeKB(long sizeB) {
        return fileSizeKB(sizeB, "KB");
    }

    public static String fileSizeMB(long sizeB) {
        return fileSizeMB(sizeB, "MB");
    }

    public static String fileSizeGB(long sizeB) {
        return fileSizeGB(sizeB, "GB");
    }

    public static String fileSizeTB(long sizeB) {
        return fileSizeTB(sizeB, "TB");
    }

    public static String fileSizeB(long sizeB, String unit) {
        return String.format("%s", sizeB) + unit;
    }

    public static String fileSizeKB(long sizeB, String unit) {
        return String.format("%.2f", sizeB / 1024f) + unit; //1024
    }

    public static String fileSizeMB(long sizeB, String unit) {
        return String.format("%.2f", sizeB / 1048576f) + unit; //(1024 * 1024)
    }

    public static String fileSizeGB(long sizeB, String unit) {
        return String.format("%.2f", sizeB / 1073741824f) + unit;//(1024 * 1024 * 1024)
    }

    public static String fileSizeTB(long sizeB, String unit) {
        return String.format("%.2f", sizeB / 1099511627776f) + unit; //1024 * 1024 * 1024 * 1024
    }


    /**
     * 获取数字大小，4进位
     */
    public static String numberEn4(long num) {
        if (num < 10000) {
            return num + "";
        } else if (num < 100000000) {
            return numberWanEn(num);
        } else {
            return numberYiEn(num);
        }
    }

    public static String numberCn4(long num) {
        if (num < 10000) {
            return num + "";
        } else if (num < 100000000) {
            return numberWanCn(num);
        } else {
            return numberYiCn(num);
        }
    }

    public static String numberWanEn(long num) {
        return numberWan(num, "w");
    }

    public static String numberWanCn(long num) {
        return numberWan(num, "万");
    }

    public static String numberWan(long num, String unit) {
        return String.format("%.2f", num / 10000f) + unit;
    }

    public static String numberYiEn(long num) {
        return numberYi(num, "w");
    }

    public static String numberYiCn(long num) {
        return numberYi(num, "万");
    }

    public static String numberYi(long num, String unit) {
        return String.format("%.2f", num / 100000000f) + unit;
    }


    /**
     * 获取数字大小，3进位
     */
    public static String numberEn3(long num) {
        if (num < 1000) {
            return num + "";
        } else if (num < 1000000) {
            return numberKiloEn(num);
        } else {
            return numberKiloEn(num);
        }
    }

    public static String numberCn3(long num) {
        if (num < 1000) {
            return num + "";
        } else if (num < 1000000) {
            return numberKiloCn(num);
        } else {
            return numberKiloCn(num);
        }
    }

    public static String numberKiloEn(long num) {
        return numberKilo(num, "k");
    }

    public static String numberKiloCn(long num) {
        return numberKilo(num, "千");
    }

    public static String numberKilo(long num, String unit) {
        return String.format("%.2f", num / 1000f) + unit;
    }

    public static String numberMillionEn(long num) {
        return numberMillion(num, "m");
    }

    public static String numberMillionCn(long num) {
        return numberMillion(num, "百万");
    }

    public static String numberMillion(long num, String unit) {
        return String.format("%.2f", num / 1000000f) + unit;
    }


}