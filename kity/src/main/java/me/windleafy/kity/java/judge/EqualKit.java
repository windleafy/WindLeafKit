package me.windleafy.kity.java.judge;

/**
 * @description: 判断是否相等工具类
 * @author: YangYong
 * @sence: 2021/1/4
 * @version: 2.0
 */
public class EqualKit {

    private EqualKit() {
        /**cannot be instantiated **/
        throw new UnsupportedOperationException("cannot be instantiated");
    }

    public static boolean equals(String param1, String param2) {
        if (EmptyKit.isEmpty(param1) || EmptyKit.isEmpty(param2)) return false;
        return param1.equals(param2);
    }

    public static boolean equals(String param1, int param2) {
        if (EmptyKit.isEmpty(param1)) return false;
        return param1.equals(param2 + "");
    }

    public static boolean equals(int param1, String param2) {
        return equals(param2, param1);
    }

    public static boolean equals(int param1, int param2) {
        return param1 == param2;
    }


    public static boolean notEquals(String param1, String param2) {
        return !equals(param2, param1);
    }

    public static boolean notEquals(String param1, int param2) {
        return !equals(param2, param1);
    }

    public static boolean notEquals(int param1, String param2) {
        return !equals(param2, param1);
    }

    public static boolean notEquals(int param1, int param2) {
        return !equals(param2, param1);
    }


    /**
     * 都为null或者都为""时仍然认为相同
     */
    public static boolean equalsEvenNull(String param1, String param2) {
        if (param1 == null && param2 == null) return true;
        if (param1 == null || param2 == null) return false;
        if ("".equals(param1) && "".equals(param2)) return true;
        return param1.equals(param2);
    }


    /**
     * 判断元素值是否相同或相等
     *
     * @param element1
     * @param element2
     * @param <E>
     * @return
     */
    public static <E> boolean isEqual(E element1, E element2) {
        if (element1 == null || element2 == null) {
            return false;
        }
        if (element1 instanceof CharSequence) {
            return element1.equals(element2);
        } else if (element1 instanceof Integer) {
            return ((Integer) element1).intValue() == ((Integer) element2).intValue();
        } else if (element1 instanceof Long) {
            return ((Long) element1).longValue() == ((Long) element2).longValue();
        } else if (element1 instanceof Double) {
            return ((Double) element1).doubleValue() == ((Double) element2).doubleValue();
        } else if (element1 instanceof Float) {
            return ((Float) element1).floatValue() == ((Float) element2).floatValue();
        } else if (element1 instanceof Short) {
            return ((Short) element1).shortValue() == ((Short) element2).shortValue();
        } else {
            return element1 == element2;
        }
    }

    /**
     * 判断元素值是否不相同或不相等
     *
     * @param element1
     * @param element2
     * @param <E>
     * @return
     */
    public static <E> boolean notEqual(E element1, E element2) {
        return !isEqual(element1, element2);
    }


}