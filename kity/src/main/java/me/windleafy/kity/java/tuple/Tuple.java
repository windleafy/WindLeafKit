package me.windleafy.kity.java.tuple;

import java.util.Arrays;

import me.windleafy.kity.java.collection.ListKit;

/**
 * @description: 元组
 * @author: YangYong
 * @sence: 2022/8/19
 */
public class Tuple {

  private final Object[] objects;

  public Tuple(Object... objects) {
    this.objects = objects;
  }

  public Object getValue(int index) {
    if (index < 0 || index >= objects.length) {
      return null;
    }
    return objects[index];
  }

  @Override
  public String toString() {
    if (objects == null) {
      return "";
    }
    StringBuilder builder = new StringBuilder();
    ListKit.forLoop(Arrays.asList(objects), (item, index) -> {
      if (index == 0) {
        builder.append("( ");
      }
      builder.append(objects[index]);
      if (index + 1 < objects.length) {
        builder.append(", ");
      } else {
        builder.append(" )");
      }
    });
    return builder.toString();
  }
}
