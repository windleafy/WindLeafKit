package me.windleafy.kity.java.judge;

/**
 * 元素判断
 */
public final class CompareKit {

    private CompareKit() {
        throw new UnsupportedOperationException("u can't instantiate me...");
    }


    /**
     * 比较条件
     *
     * @param <E>
     * @param <V>
     */
    public interface Condition<E, V> {
        boolean equal(E e, V v);
    }

    /**
     * 是否集合里是否包含element值
     *
     * @param elements
     * @param <E>
     * @return
     */
    @SafeVarargs
    public static <E> boolean contain(E element, E... elements) {
        for (E e : elements) {
            if (EqualKit.isEqual(element, e))
                return true;
        }
        return false;
    }

    /**
     * 是否集合里是否包含values值
     *
     * @param element
     * @param values
     * @param <E>
     * @param <V>
     * @return
     */
    @SafeVarargs
    public static <E, V> boolean contain(Condition<E, V> condition, E element, V... values) {
        for (V v : values) {
            if (condition.equal(element, v))
                return true;
        }
        return false;
    }

    /**
     * 是否所有元素都与element相同
     *
     * @param element
     * @param elements
     * @param <E>
     * @return
     */
    @SafeVarargs
    public static <E> boolean equals(E element, E... elements) {
        if (elements == null) return false;
        for (E e : elements) {
            if (EqualKit.notEqual(element, e))
                return false;
        }
        return true;
    }

    /**
     * 是否所有元素都与values相同
     *
     * @param element
     * @param values
     * @param <E>
     * @return
     */
    @SafeVarargs
    public static <E, V> boolean equals(Condition<E, V> condition, E element, V... values) {
        if (values == null) return false;
        for (V v : values) {
            if (!condition.equal(element, v))
                return false;
        }
        return true;
    }

    /**
     * 是否所有元素都相同
     *
     * @param elements
     * @param <E>
     * @return
     */
    @SafeVarargs
    public static <E> boolean allEquals(E... elements) {
        if (elements == null) return false;
        int length = elements.length;
        if (length < 2) {
            return false;
        }
        //length > 2
        return equals(elements[0], elements);
    }

}
