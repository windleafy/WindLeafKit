package me.windleafy.kity.java.bean;

import java.util.Arrays;
import java.util.List;

/**
 * @description: 多位参数
 * @author: YangYong
 * @sence: 2021/12/10
 * @version: 2.3
 */
public class MultiValue<T> {

    private T[] values;

    public MultiValue(T... values) {
        this.values = values;
    }

    public MultiValue(List<T> list, T[] type) {
        this.values = list.toArray(type);
    }

    /**
     * 获取列表
     */
    public List<T> asList() {
        return Arrays.asList(values);
    }

    /**
     * 获取数组
     */
    public T[] asArray() {
        return values;
    }

    /**
     * 获取值
     */
    public T value(int index) {
        if (index > 0 && values != null && values.length > index) {
            return values[index];
        } else {
            return null;
        }
    }

}
