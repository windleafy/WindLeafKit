package me.windleafy.kity.java.callback;

public interface CallBackValue<V> {
    void back(V v);
}