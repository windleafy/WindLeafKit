package me.windleafy.kity.java.utils;

import android.text.TextUtils;

import java.net.FileNameMap;
import java.net.URLConnection;

/**
 * 跟App相关的辅助类
 */
public class FileKit {

    private FileKit() {
        /**cannot be instantiated **/
        throw new UnsupportedOperationException("cannot be instantiated");
    }

    /**
     * 根据文件后缀名判断 文件是否是图片文件
     */
    public static boolean isImage(String fileName) {
        return !TextUtils.isEmpty(fileName) && getMimeType(fileName).contains("image/");
    }

    /**
     * 根据文件后缀名判断 文件是否是音频文件
     */
    public static boolean isAudio(String fileName) {
        return !TextUtils.isEmpty(fileName) && getMimeType(fileName).contains("audio/");
    }


    /**
     * 根据文件后缀名判断 文件是否是视频文件
     */
    public static boolean isVideo(String fileName) {
        return !TextUtils.isEmpty(fileName) && getMimeType(fileName).contains("video/");
    }

    /**
     * 获取文件类型
     *
     * @param fileName 文件名
     * @return 返回MIME类型
     * https://www.cnblogs.com/niwanglong385/articles/5632166.html
     */
    private static String getMimeType(String fileName) {
        FileNameMap fileNameMap = URLConnection.getFileNameMap();
        String type = fileNameMap.getContentTypeFor(fileName);
        return type;
    }



}