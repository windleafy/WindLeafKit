package me.windleafy.kity.java.model;

import android.os.Parcel;
import android.os.Parcelable;

public class PersonParcel implements Parcelable {

    private Integer id;

    private String name;

    public String age;

    //构造函数1
    public PersonParcel() {

    }

    //构造函数2
    public PersonParcel(Integer id) {
        this.id = id;
    }

    //构造函数3
    public PersonParcel(Integer id, String name) {
        this.id = id;
        this.name = name;
    }

    //构造函数3
    public PersonParcel(Integer id, String name, String age) {
        this.id = id;
        this.name = name;
        this.age = age;
    }


    protected PersonParcel(Parcel in) {
        if (in.readByte() == 0) {
            id = null;
        } else {
            id = in.readInt();
        }
        name = in.readString();
        age = in.readString();
    }

    public static final Creator<PersonParcel> CREATOR = new Creator<PersonParcel>() {
        @Override
        public PersonParcel createFromParcel(Parcel in) {
            return new PersonParcel(in);
        }

        @Override
        public PersonParcel[] newArray(int size) {
            return new PersonParcel[size];
        }
    };

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    /**
     * 静态方法
     */
    public static void update() {

    }


    @Override
    public String toString() {
        return "Person{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", age='" + age + '\'' +
                '}';
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        if (id == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(id);
        }
        dest.writeString(name);
        dest.writeString(age);
    }
}