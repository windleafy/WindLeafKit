package me.windleafy.kity.java.judge;

/**
 * @description: 判断是否包含工具类
 * @author: YangYong
 * @sence: 2021/1/4
 * @version: 2.0
 */
public class ContainKit {

    private ContainKit() {
        /**cannot be instantiated **/
        throw new UnsupportedOperationException("cannot be instantiated");
    }

    public static boolean contain(String param, String... params) {
        if (param == null || params == null) return false;
        return CompareKit.contain(param, params);
    }

    public static boolean contain(String param, Integer... params) {
        if (param == null) return false;
        return CompareKit.contain(EqualKit::equals, param, params);
    }

    public static boolean contain(Integer param, Integer... params) {
        return CompareKit.contain(param, params);
    }


}