package me.windleafy.kity.java.utils;

import me.windleafy.kity.java.judge.EmptyKit;

/**
 * @description: 字符串工具类
 * @author: YangYong
 * @sence: 2021/3/2
 * @version: 2.0
 */
public class ValidKit {

    private ValidKit() {
        throw new UnsupportedOperationException("u can't instantiate me...");
    }

    /**
     * param不为null，则返回param本身，否则返回paramIfNull
     */
    public static <T> T valueForNull(T param, T paramIfNull) {
        return param != null ? param : paramIfNull;
    }

    /**
     * param不为null，则返回param本身，否则返回paramIfNull
     */
    public static String valueForEmpty(String param, String paramIfEmpty) {
        return EmptyKit.notEmpty(param) ? param : paramIfEmpty;
    }

    /**
     * param不为null，则返回param本身，否则返回paramIfNull
     */
    public static <T> T valueForValidAndNull(T param, T valid, T paramIfNull) {
        return param != null ? valid : paramIfNull;
    }

}
