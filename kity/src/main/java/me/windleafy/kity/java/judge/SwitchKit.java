package me.windleafy.kity.java.judge;

import me.windleafy.kity.java.bean.MultiValue;
import me.windleafy.kity.java.callback.CallBackVoid;

/**
 * @description: 替代switch
 * @author: YangYong
 * @sence: 2021/12/10
 * @version: 2.3
 */
public class SwitchKit<T> {

    private T value;//待比对的值

    private boolean isCaseDone = false;//是否已执行

    public SwitchKit(T value) {
        this.value = value;
    }

    public SwitchKit<T> addCase(T caseValue, CallBackVoid callBackVoid) {
        if (!isCaseDone && EqualKit.isEqual(value, caseValue)) {
            isCaseDone = true;
            callBackVoid.back();
        }
        return this;
    }

    public SwitchKit<T> addMultiCase(MultiValue<T> caseValues, CallBackVoid callBackVoid) {
        if (!isCaseDone && CompareKit.contain(value, caseValues.asArray())) {
            isCaseDone = true;
            callBackVoid.back();
        }
        return this;
    }

    public SwitchKit<T> setDefault(CallBackVoid callBackVoid) {
        if (!isCaseDone) {
            callBackVoid.back();
        }
        return this;
    }

    /**
     * 返回值
     * 当执行了addCase和addMultiCase中的代码返回 true
     * 执行了addDefault中的代码返回 false
     */
    public boolean withReturn() {
        return isCaseDone;
    }

    /**
     * @param value
     * @param <T>
     * @return
     */
    public static <T> SwitchKit<T> value(T value) {
        return new SwitchKit<>(value);
    }

}
