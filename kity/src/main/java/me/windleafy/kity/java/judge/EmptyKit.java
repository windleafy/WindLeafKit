package me.windleafy.kity.java.judge;

import java.util.Collection;
import java.util.Map;

/**
 * @description: 空判断
 * @author: YangYong
 * @sence: 2021/3/2
 * @version: 2.0
 */
public class EmptyKit {

    private EmptyKit() {
        throw new UnsupportedOperationException("u can't instantiate me...");
    }

    /**
     * 所有参数为空
     */
    public static boolean isEmpty(String... strings) {
        for (String str : strings) {
            if (notEmpty(str)) return false;
        }
        return true;
    }

    /**
     * 有参数为空
     */
    public static boolean hasEmpty(String... strings) {
        for (String str : strings) {
            if (isEmpty(str)) return true;
        }
        return false;
    }

    /**
     * 字符为空，去除空格
     */
    public static boolean isEmpty(CharSequence str) {
        return isNull(str) || str.toString().trim().length() == 0; //str.length() == 0
    }

    public static boolean isEmpty(Object[] os) {
        return isNull(os) || os.length == 0;
    }

    public static boolean isEmpty(Collection<?> l) {
        return isNull(l) || l.isEmpty();
    }

    public static boolean isEmpty(Map<?, ?> m) {
        return isNull(m) || m.isEmpty();
    }

    /**
     * 所有参数不为空
     */
    public static boolean notEmpty(String... strings) {
        for (String str : strings) {
            if (isEmpty(str)) return false;
        }
        return true;
    }

    /**
     * 有参数不为空
     */
    public static boolean hasNotEmpty(String... strings) {
        for (String str : strings) {
            if (notEmpty(str)) return true;
        }
        return false;
    }

    public static boolean notEmpty(CharSequence str) {
        return !isEmpty(str);
    }

    public static boolean notEmpty(Object[] os) {
        return !isEmpty(os);
    }

    public static boolean notEmpty(Collection<?> l) {
        return !isEmpty(l);
    }

    public static boolean notEmpty(Map<?, ?> m) {
        return !isEmpty(m);
    }


    public static boolean isNull(Object o) {
        return o == null;
    }

    public static boolean notNull(Object o) {
        return o != null;
    }


    /**
     * 所有参数为空
     */
    public static boolean isNull(Object... os) {
        for (Object o : os) {
            if (!isNull(o)) return false;
        }
        return true;
    }

    /**
     * 有参数为空
     */
    public static boolean hasNull(Object... os) {
        for (Object o : os) {
            if (isNull(o)) return true;
        }
        return false;
    }


    /**
     * 所有参数不为空
     */
    public static boolean notNull(Object... os) {
        for (Object o : os) {
            if (!notNull(o)) return false;
        }
        return true;
    }

    /**
     * 有参数不为空
     */
    public static boolean hasNotNull(Object... os) {
        for (Object o : os) {
            if (notNull(o)) return true;
        }
        return false;
    }

    /**
     * 从左到右取第一个非空值
     */
    @SafeVarargs
    public static <T> T value(T... params) {
        for (T param : params) {
            if (notNull(param))
                return param;
        }
        return null;
    }

}
