package me.windleafy.kity.java.judge;

import me.windleafy.kity.java.callback.CallBackVoid;

/**
 * @description: 替代if
 * @author: YangYong
 * @sence: 2021/12/10
 * @version: 2.3
 */
public class IfKit {

    private boolean isDone = false;//是否已执行

    public IfKit() {
    }

    public IfKit addJudge(boolean caseValue, CallBackVoid callBackVoid) {
        if (!isDone && caseValue) {
            isDone = true;
            callBackVoid.back();
        }
        return this;
    }

    /**
     * @param condition
     * @return
     */
    public static IfKit judge(boolean condition, CallBackVoid callBackVoid) {
        return new IfKit().addJudge(condition, callBackVoid);
    }


//    /**
//     * @param condition
//     * @return
//     */
//    public static IfKit judge(boolean condition, CallBackValue<Boolean> callBackVoid) {
//        return condition? callBackVoid ?callBackVoid.back(true);
//    }

}
