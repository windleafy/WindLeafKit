package me.windleafy.kity.java.judge;

import java.util.Map;

/**
 * @description: 字符串工具类
 * @author: YangYong
 * @sence: 2021/3/2
 * @version: 2.0
 */
public class StringKit {

    private StringKit() {
        throw new UnsupportedOperationException("u can't instantiate me...");
    }

    /**
     * param不为空，则返回param本身，否则返回""
     */
    public static String valid(String param) {
        return EmptyKit.notEmpty(param) ? param : "";
    }

    /**
     * 从左到右取第一个非空值，都为空则返回""
     */
    public static String valid(String... params) {
        for (String param : params) {
            if (EmptyKit.notEmpty(param))
                return param;
        }
        return "";
    }


    /**
     * 获取匹配的数据
     */
    public static <T, V> V keyValue(T key, Map<T, V> map) {
        for (Map.Entry<T, V> entry : map.entrySet()) {
            if (entry.getKey() != null) {
                if (CompareKit.equals(entry.getKey(), key)) {
                    return entry.getValue();
                }
            }
        }
        return null;
    }

    /**
     * 获取匹配的数据
     */
    public static String keyValue(String key, Map<String, String> map, String def) {
        for (Map.Entry<String, String> entry : map.entrySet()) {
            if (EmptyKit.notEmpty(entry.getKey())) {
                if (EqualKit.equals(entry.getKey(), key)) {
                    return entry.getValue();
                }
            }
        }
        return def;
    }

    /**
     * param不为null，则返回param本身，否则返回paramIfNull
     */
    public static String valueNotNull(String param, String paramIfNull) {
        return EmptyKit.notNull(param) ? param : paramIfNull;
    }

    /**
     * param不为空，则返回param本身，否则返回paramIfEmpty
     */
    public static String valueNotEmpty(String param, String paramIfEmpty) {
        return EmptyKit.notEmpty(param) ? param : paramIfEmpty;
    }

    /**
     * 是否包含
     */
    public static boolean contain(String str, String s) {
        if (str == null) return false;
        return str.contains(s);
    }

    /**
     * 起始字符串匹配情况
     */
    public static boolean startsWith(String str, String start) {
        return str != null && str.startsWith(start);
    }

    /**
     * 是否为Integer
     */
    public static boolean isInteger(String str) {
        try {
            Integer.valueOf(str);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    /**
     * 是否为Double
     */
    public static boolean isDouble(String str) {
        try {
            Double.valueOf(str);
            return true;
        } catch (Exception e) {
            return false;
        }
    }


}
