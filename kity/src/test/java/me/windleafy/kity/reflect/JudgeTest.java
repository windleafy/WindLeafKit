package me.windleafy.kity.reflect;

import org.junit.Test;

import java.util.ArrayList;

import me.windleafy.kity.java.judge.JudgeKit;

public class JudgeTest {


    @Test
    public void test() {

        int a = 1;
        int a1 = 1;
        int a2 = 1;
        int a3 = 1;

        int b = 2;
        int b1 = 2;
        int b2 = 2;

        int c = 3;
        int c1 = 3;

        int d = 4;

//        int[] params = new int[]{a,a1,a2,b};
        String[] arrays = new String[]{"123", "1234", "12345"};
        ArrayList<String> lists = new ArrayList<>();
        lists.add("123");
        lists.add("1234");
        lists.add("1235");


        System.out.println(JudgeKit.contain("123", lists));
        System.out.println(JudgeKit.contain("123", lists.toArray()));
        System.out.println(JudgeKit.allEquals(arrays));
        System.out.println(JudgeKit.allNotEquals(arrays));


    }


}
