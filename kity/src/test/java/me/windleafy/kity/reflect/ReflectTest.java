package me.windleafy.kity.reflect;

import org.junit.Test;

import java.lang.reflect.InvocationTargetException;
import java.util.Arrays;
import java.util.List;

import me.windleafy.kity.java.model.Game;
import me.windleafy.kity.java.model.Person;
import me.windleafy.kity.java.random.RandomFactory;
import me.windleafy.kity.java.utils.ReflectKit;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class ReflectTest {

    /**
     * 随机类生成器
     */

    @Test
    public void testFactory() {
        Person person = RandomFactory.createObject(Person.class);
        System.out.println(person.toString()+"\n");

        Game game = RandomFactory.createObject(Game.class);
        System.out.println(game.toString()+"\n");
    }

    /**
     * 随机类List生成器
     */
    @Test
    public void testListFactory() {
        List list = RandomFactory.createList(Person.class, 5);
        System.out.println(list.toString()+"\n");

        List list2 = RandomFactory.createList(Game.class, 3);
        System.out.println(list2.toString()+"\n");
    }





    @Test
    public void testReflectKit() throws InstantiationException, IllegalAccessException {
        ReflectKit.info(Person.class);
    }

    @Test
    public void testReflectKitConstructors() throws InstantiationException, IllegalAccessException, InvocationTargetException, NoSuchMethodException {
        ReflectKit.constructor(Person.class, Integer.class, String.class);
    }

    @Test
    public void testReflectKitWrap() throws InstantiationException, IllegalAccessException {
        System.out.println(ReflectKit.isWrapClass(int.class));
        System.out.println(ReflectKit.isWrapClass(boolean.class));
        System.out.println(ReflectKit.isWrapClass(Integer.class));
        System.out.println(ReflectKit.isWrapClass(Double.class));
        System.out.println(ReflectKit.isWrapClass(CharSequence.class));
        System.out.println(ReflectKit.isWrapClass(String.class));
        System.out.println(ReflectKit.isWrapClass(Person.class));
    }

    @Test
    public void testReflectKitSimpleName() throws InstantiationException, IllegalAccessException {
        //基本类型
        ReflectKit.createBasicObject(boolean.class);
        ReflectKit.createBasicObject(byte.class);
        ReflectKit.createBasicObject(char.class);
        ReflectKit.createBasicObject(short.class);
        ReflectKit.createBasicObject(int.class);
        ReflectKit.createBasicObject(long.class);
        ReflectKit.createBasicObject(float.class);
        ReflectKit.createBasicObject(double.class);
//        ReflectKit.createBasicObject(void.class);
        //封装类型
        ReflectKit.createBasicObject(Boolean.class);
        ReflectKit.createBasicObject(Byte.class);
        ReflectKit.createBasicObject(Character.class);
        ReflectKit.createBasicObject(Short.class);
        ReflectKit.createBasicObject(Integer.class);
        ReflectKit.createBasicObject(Long.class);
        ReflectKit.createBasicObject(Float.class);
        ReflectKit.createBasicObject(Double.class);
//        ReflectKit.createBasicObject(Void.class);
    }

    @Test
    public void testReflectKitCreateBasicObject() throws InstantiationException, IllegalAccessException, InvocationTargetException, NoSuchMethodException {
        //基本类型
        System.out.println(ReflectKit.createBasicObject(boolean.class));
        System.out.println(ReflectKit.createBasicObject(byte.class));
        System.out.println(ReflectKit.createBasicObject(char.class));
        System.out.println(ReflectKit.createBasicObject(short.class));
        System.out.println(ReflectKit.createBasicObject(int.class));
        System.out.println(ReflectKit.createBasicObject(long.class));
        System.out.println(ReflectKit.createBasicObject(float.class));
        System.out.println(ReflectKit.createBasicObject(double.class));
//         System.out.println(ReflectKit.createBasicObject(void.class));
        //封装类型
        System.out.println(ReflectKit.createBasicObject(Boolean.class));
        System.out.println(ReflectKit.createBasicObject(Byte.class));
        System.out.println(ReflectKit.createBasicObject(Character.class));
        System.out.println(ReflectKit.createBasicObject(Short.class));
        System.out.println(ReflectKit.createBasicObject(Integer.class));
        System.out.println(ReflectKit.createBasicObject(Long.class));
        System.out.println(ReflectKit.createBasicObject(Float.class));
        System.out.println(ReflectKit.createBasicObject(Double.class));
        //String
        System.out.println(">" + ReflectKit.createBasicObject(String.class) + "<");

        Object[] objects = ReflectKit.createBasicObject(boolean.class, int.class, Integer.class, Double.class, String.class);
        System.out.println(Arrays.asList(objects));

        Person person = ReflectKit.instance(Person.class, Integer.class, String.class);
        System.out.println("person:" + person.toString());

        Person person1 = ReflectKit.constructor(Person.class, Integer.class, String.class, String.class);
        System.out.println("person1:" + person1.toString());

        Person person2 = ReflectKit.constructor(Person.class);
        System.out.println("person2:" + person2.toString());

    }

}