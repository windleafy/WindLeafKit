package me.windleafy.kity.java;

import org.junit.Test;

import me.windleafy.kity.java.model.Person;
import me.windleafy.kity.java.tuple.Tuple;

import static org.junit.Assert.assertEquals;

/**
 * @description: 描述
 * @author: YangYong
 * @sence: 2022/8/19
 */
public class TupleTest {

  @Test
  public void test() {
    Tuple tuple = new Tuple(1,"abc",3.0f,new Person(123,"杨勇"));

    System.out.println(tuple.getValue(0));
    System.out.println(tuple.getValue(1));
    System.out.println(tuple.getValue(2));
    System.out.println(tuple.getValue(3));
    System.out.println(tuple.getValue(4));
    System.out.println(tuple.getValue(5));
    System.out.println(tuple.getValue(6));

    System.out.println(tuple.toString());

  }
}
