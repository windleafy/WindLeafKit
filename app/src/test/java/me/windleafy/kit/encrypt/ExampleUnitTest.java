package me.windleafy.kit.encrypt;

import org.junit.Test;

import java.io.IOException;
import java.math.BigDecimal;
import java.nio.charset.StandardCharsets;
import java.security.SecureRandom;
import java.util.Arrays;
import java.util.Base64;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

import me.windleafy.kity.java.utils.EncryptKit;
import me.windleafy.kity.java.utils.HttpKit;
import me.windleafy.kity.java.utils.TimeKit;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class ExampleUnitTest {

    @Test
    public void test() {

        double val = 12.355;
        String str = "12.34545";
        String ret = String.format("%.2f", val);
        String ret2 = String.format("%.2f", Double.valueOf(str));
        BigDecimal bigDecimal = new BigDecimal(str);
        String ret3 = new BigDecimal(str).setScale(2, BigDecimal.ROUND_DOWN).toString();
        System.out.println(ret);
        System.out.println(ret2);
        System.out.println(ret3);

    }

    @Test
    public void test2() {

        List<String> audienceList = new LinkedList<>();
        audienceList.add("AA");
        audienceList.add("BB");
        audienceList.add("CC");

        System.out.println("audienceList = " + audienceList);
        System.out.println("audienceList.size() = " + audienceList.size());
        System.out.println("audienceList.toString() = " + audienceList.toString());
        System.out.println("Arrays.toString(audienceList.toArray()) = " + Arrays.toString(audienceList.toArray()));
    }

    @Test
    public void test3() {
        System.out.println(TimeKit.getTime(new Date(), true, true, true, true, true, true));
        System.out.println(TimeKit.getTime(new Date(), false, true, true, true, true, true));
        System.out.println(TimeKit.getTime(new Date(), false, true, true, true, true, false));
        System.out.println(TimeKit.getTime(new Date(), false, false, false, true, true, true));
        Date date = null;
        try {
            date = TimeKit.getDate("2010-3-13 16:01");
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println(TimeKit.getTime(date, true, true, true, true, true, true));
        System.out.println(TimeKit.getTime(date, TimeKit.FormatDateMonthDay, TimeKit.FormatTimeHourMinute));
    }

    @Test
    public void test4() {
        String time = null;
        System.out.println(TimeKit.getTime(time, TimeKit.FormatDefault, TimeKit.FormatDate + TimeKit.FormatTimeHourMinute));
        System.out.println(TimeKit.getTime("", TimeKit.FormatDefault, TimeKit.FormatDate + TimeKit.FormatTimeHourMinute));
        System.out.println(TimeKit.getTime("2018-10-10 1:20:22", TimeKit.FormatDefault, TimeKit.FormatDate + TimeKit.FormatTimeHourMinute));
    }


    @Test
    public void test5() {
        String hobdesc = "";
        int index = hobdesc.indexOf("—");
        System.out.println("index=" + index);

//        hobdesc.substring(0, hobdesc.indexOf("—"));
        String HOBTYPE = "Abc";
        System.out.println(HOBTYPE.substring(HOBTYPE.length() - 1, HOBTYPE.length()));
        System.out.println(HOBTYPE.charAt(HOBTYPE.length() - 1) == 'A');
    }

    @Test
    public void test6() {
        String url1 = "http://www.jhcee.cn/upload/jhkf.apk";
//        String url2 = "http://xiazai.xqishu.com/txt/%E6%96%97%E6%88%98%E7%8B%82%E6%BD%AE.txt";
        String url2 = "http://www.jhcee.cn/upload/jhkf.apk.txt";
        System.out.println(TimeKit.currentMillis());
        System.out.println(HttpKit.isExist(url1));
        System.out.println(HttpKit.readFileByUrl(url2));
//        for (int i = 0; i < 3; i++) {
//            System.out.println(HttpKit.isExist("http://www.jhcee.cn/upload/jhkf_v" + i + "apk"));
//        }

        System.out.println(TimeKit.currentMillis());
    }

    @Test
    public void test7() {
        String content = "url：http://www.xpk.jhcee.cn/jhgk/rest/contactus/getPhoneNumber/";
        String pwd = "abc";

        String encrypt = EncryptKit.encrypt(content, pwd);
        System.out.println(encrypt);


        String decrypt = EncryptKit.decrypt(encrypt, pwd);
        System.out.println(decrypt);
    }

    /**
     * 加密
     *
     * @param content  需要加密的内容
     * @param password 加密密码
     * @return
     */
    public static String encrypt(String content, String password) {
        try {
            KeyGenerator kgen = KeyGenerator.getInstance("AES");
            kgen.init(128, new SecureRandom(password.getBytes()));
            SecretKey secretKey = kgen.generateKey();
            byte[] enCodeFormat = secretKey.getEncoded();
            SecretKeySpec key = new SecretKeySpec(enCodeFormat, "AES");
            Cipher cipher = Cipher.getInstance("AES");// 创建密码器
            byte[] byteContent = content.getBytes(StandardCharsets.UTF_8);
            cipher.init(Cipher.ENCRYPT_MODE, key);// 初始化
            byte[] result = cipher.doFinal(byteContent);

            Base64.Encoder encoder = Base64.getEncoder();
            return new String(encoder.encode(result), StandardCharsets.UTF_8);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 解密AES加密过的字符串
     *
     * @param content  AES加密过过的内容
     * @param password 加密时的密码
     * @return 明文
     */
    public static String decrypt(String content, String password) {
        try {
            Base64.Decoder decoder = Base64.getDecoder();
            byte[] input= decoder.decode(content);

            KeyGenerator kgen = KeyGenerator.getInstance("AES");// 创建AES的Key生产者
            kgen.init(128, new SecureRandom(password.getBytes()));
            SecretKey secretKey = kgen.generateKey();// 根据用户密码，生成一个密钥
            byte[] enCodeFormat = secretKey.getEncoded();// 返回基本编码格式的密钥
            SecretKeySpec key = new SecretKeySpec(enCodeFormat, "AES");// 转换为AES专用密钥
            Cipher cipher = Cipher.getInstance("AES");// 创建密码器
            cipher.init(Cipher.DECRYPT_MODE, key);// 初始化为解密模式的密码器
            byte[] result = cipher.doFinal(input);

            return new String(result, StandardCharsets.UTF_8);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }


    @Test
    public void test8() {
        System.out.println(encrypt("abc", "jhgk"));
    }


    @Test
    public void test9() throws IOException {
        System.out.println(encrypt("abc"));
    }

    /**
     * 加密
     *
     * @param content 需要加密的内容
     * @return
     */
    public static String encrypt(String content) {
        try {
            Base64.Encoder encoder = Base64.getEncoder();
            return new String(encoder.encode(content.getBytes()), StandardCharsets.UTF_8);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }


}