package me.windleafy.kit.encrypt;

import org.junit.Test;

import me.windleafy.kity.java.utils.EncryptKit;

public class EncryptTest {

    @Test
    public void test() {
        String str = "/iU/stAmJKU0dse+7YJU29uevL6QfNqxcu++cvuWw42qk1Z0v7yfq5DgTHBAaxJmbXDhN3lpE3Pxtz9baf1mUcEugJwTE0F4A7T4d/zo/Pw=";

        System.out.println(EncryptKit.decrypt(str,"abc"));
    }

    @Test
    public void test2() {
        String content = "url：http://www.xpk.jhcee.cn/jhgk/rest/contactus/getPhoneNumber/";
        String pwd = "abc";

        String encrypt = EncryptKit.encrypt(content, pwd);
        System.out.println(encrypt);

        String decrypt = EncryptKit.decrypt(encrypt, pwd);
        System.out.println(decrypt);
    }

    @Test
    public void test3() {
        String content = "{\"code\":200,\"message\":\"成功\",\"data\":{}}";

        String encrypt = EncryptKit.encryptBase64(content);
        System.out.println(encrypt);

        String decrypt = EncryptKit.decryptBase64(encrypt);
        System.out.println(decrypt);
    }
}
