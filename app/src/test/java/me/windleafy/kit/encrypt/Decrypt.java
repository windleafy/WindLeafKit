package me.windleafy.kit.encrypt;

import org.junit.Test;

import me.windleafy.kity.java.utils.EncryptKit;

public class Decrypt {

    @Test
    public void test() {

        String encrypt = "eyJjb2RlIjoyMDAsIm1lc3NhZ2UiOiLmiJDlip8iLCJkYXRhIjp7fX0=";

        String decrypt = EncryptKit.decryptBase64(encrypt);
        System.out.println(decrypt);
    }
}
