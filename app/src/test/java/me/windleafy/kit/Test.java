package me.windleafy.kit;

import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;

import me.windleafy.kity.java.math.NumberKit;
import me.windleafy.kity.java.utils.TimeKit;

public class Test {

    @org.junit.Test
    public void test() {
        String str = NumberKit.convert(100099,new NumberKit.NumberFormat(10000,2,"万"));

        System.out.println(str);
    }

    @org.junit.Test
    public void test1() throws ParseException {
        Date curDate = new Date();
        Calendar curCal = TimeKit.getCalendar(curDate);

        Date compareDate = TimeKit.getDate(curCal.getWeekYear()+"-9-1 00:00:00");
        Calendar compareCal = TimeKit.getCalendar(curCal.get(Calendar.YEAR)+"-9-1 00:00:00");

//        System.out.println(compareDate.toString());
        System.out.println(TimeKit.getTime(curCal,TimeKit.FormatDefault));
        System.out.println(TimeKit.getTime(compareCal,TimeKit.FormatDefault));
        System.out.println(curCal.after(compareCal));
        System.out.println(curCal.before(compareCal));

        System.out.println(TimeKit.getTime(curDate,TimeKit.FormatDefault));
        System.out.println(TimeKit.getTime(compareDate,TimeKit.FormatDefault));
        System.out.println(curDate.after(compareDate));
        System.out.println(curDate.before(compareDate));
    }

}
