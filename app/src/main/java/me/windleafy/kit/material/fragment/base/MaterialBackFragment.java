package me.windleafy.kit.material.fragment.base;

import android.support.v7.widget.Toolbar;
import android.view.View;

import me.windleafy.kit.R;
import me.windleafy.kity.android.base.fragment.BaseSupportFragment;

/**
 * Created by YoKeyword on 16/2/7.
 */
public class MaterialBackFragment extends BaseSupportFragment {

    protected void initToolbarNav(Toolbar toolbar) {
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back_white_24dp);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pop();
            }
        });
    }
}
