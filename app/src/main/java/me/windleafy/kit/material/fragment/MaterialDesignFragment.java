package me.windleafy.kit.material.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.zhy.adapter.recyclerview.CommonAdapter;
import com.zhy.adapter.recyclerview.base.ViewHolder;

import java.util.ArrayList;
import java.util.List;

import me.windleafy.kit.R;
import me.windleafy.kit.material.fragment.base.MaterialMainFragment;
import me.windleafy.kit.test.TestActivity;
import me.windleafy.kit.test.activity.view.debug.DebugViewActivity;
import me.windleafy.kity.android.wiget.debug.DebugView;
import me.windleafy.kity.android.wiget.toast.Toaster;

/**
 * 测试模板Fragment
 */
public class MaterialDesignFragment extends MaterialMainFragment {

    public static final String TAG = MaterialDesignFragment.class.getSimpleName();

    private static final String ARG_TITLE = "arg_title";

    private Toolbar mToolbar;
    private String mTitle;
    private DebugView debugLayout;
    private RecyclerView mRecyclerView;
    private List<JumpTarget> mList;

    public static MaterialDesignFragment newInstance() {
        return newInstance("Main");
    }

    public static MaterialDesignFragment newInstance(String title) {
        MaterialDesignFragment fragment = new MaterialDesignFragment();
        Bundle bundle = new Bundle();
        bundle.putString(ARG_TITLE, title);
        fragment.setArguments(bundle);
        return fragment;
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle bundle = getArguments();
        if (bundle != null) {
            mTitle = bundle.getString(ARG_TITLE);
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_material_design, container, false);
        initView(view);

        return view;
    }



    private void initView(final View view) {
        mToolbar = (Toolbar) view.findViewById(R.id.toolbar);
        mToolbar.setTitle(mTitle);
        initToolbarNav(mToolbar);
        FloatingActionButton fab = view.findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Snackbar.make(v, "snackbar", Snackbar.LENGTH_SHORT)
                        .setAction("Undo", new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Toaster.show("undo program");
                            }
                        }).show();
            }
        });
        mRecyclerView = view.findViewById(R.id.recycle_view);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initRV();
//        initDebug();
    }

    private void initRV() {
        mList = new ArrayList<>();
        mList.add(new JumpTarget("DebugViewActivity", DebugViewActivity.class));
        mList.add(new JumpTarget("TestActivity", TestActivity.class));

        mRecyclerView.setLayoutManager(new LinearLayoutManager(_mActivity));
        mRecyclerView.setAdapter(new CommonAdapter<JumpTarget>(_mActivity, android.R.layout.simple_list_item_1, mList) {
            @Override
            protected void convert(ViewHolder holder, final JumpTarget jumpTarget, int position) {
                holder.setText(android.R.id.text1, jumpTarget.getTitle());
                holder.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        startActivity(new Intent(_mActivity, jumpTarget.gettClass()));
                    }
                });
            }
        });
    }

    protected void initDebug() {
        debugLayout = new DebugView(this, R.id.fragment_container);
        debugLayout.show(true);
        debugLayout.setVisibility(View.VISIBLE);
        debugLayout.setTitle("Main");
        //        debugLayout.showEditLayout();

        //Button
        //        debugLayout.setButtonText("TOP", "CENTER", "BOTTOM", "VIEW");
        debugLayout.setDebugClickListener(new DebugView.DebugClickListener() {
            @Override
            public void onClick(View v, int index) {
                Toaster.show("button index = " + index);
                switch (index) {
                    case 1:
                        break;
                    case 2:
                        break;
                    case 3:
                        break;
                    case 4:
                        break;
                }
            }
        });

        debugLayout.setDebugClickListener2(new DebugView.DebugClickListener() {
            @Override
            public void onClick(View v, int index) {
                Toaster.show("button2 index = " + index);
                switch (index) {
                    case 1:
                        debugLayout.setText("CardView click");
                        break;
                    case 2:
                        break;
                    case 3:
                        break;
                    case 4:
                        break;
                }
            }
        });
    }

    public static class JumpTarget {
        private String title;
        private Class<?> tClass;

        public JumpTarget(String title, Class<?> tClass) {
            this.title = title;
            this.tClass = tClass;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public Class<?> gettClass() {
            return tClass;
        }

        public void settClass(Class<?> tClass) {
            this.tClass = tClass;
        }
    }

}
