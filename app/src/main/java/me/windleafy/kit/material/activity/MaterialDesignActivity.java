package me.windleafy.kit.material.activity;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import me.yokeyword.fragmentation.SupportFragment;
import me.yokeyword.fragmentation.ISupportFragment;
import me.windleafy.kit.R;
import me.windleafy.kit.demo_flow.base.BaseMainFragment;
import me.windleafy.kit.material.fragment.CardViewFragment;
import me.windleafy.kit.material.fragment.MaterialDesignFragment;
import me.windleafy.kit.material.fragment.SwipeRefreshFragment;
import me.windleafy.kity.android.base.activity.BaseSupportActivity;
import me.windleafy.kity.android.base.fragment.BaseSupportFragment;
import me.windleafy.kity.android.wiget.debug.DebugView;
import me.windleafy.kity.android.wiget.toast.Toaster;

public class MaterialDesignActivity extends BaseSupportActivity
        implements NavigationView.OnNavigationItemSelectedListener,
        BaseMainFragment.OnFragmentOpenDrawerListener {

    private DebugView debugLayout;

    public static final String TAG = MaterialDesignActivity.class.getSimpleName();

    private DrawerLayout mDrawer;
    private NavigationView mNavigationView;
    private TextView mTvName;   // NavigationView上的名字
    private ImageView mImgNav;  // NavigationView上的头像

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_material_design);

        MaterialDesignFragment fragment = findFragment(MaterialDesignFragment.class);
        if (fragment == null) {
            loadRootFragment(R.id.material_fragment_container, MaterialDesignFragment.newInstance());
        }

        initView();

    }


    public void initView() {
        mDrawer = findViewById(R.id.material_drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, mDrawer, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        toggle.syncState();

        mNavigationView = findViewById(R.id.material_navigation);
        mNavigationView.setNavigationItemSelectedListener(this);
        mNavigationView.setCheckedItem(R.id.nav1);

        LinearLayout mNavHeader = (LinearLayout) mNavigationView.getHeaderView(0);
        mTvName = (TextView) mNavHeader.findViewById(R.id.tv_name);
        mImgNav = (ImageView) mNavHeader.findViewById(R.id.img_nav);
        mNavHeader.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDrawer.closeDrawer(GravityCompat.START);
                mDrawer.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        goLogin();
                    }
                }, 250);
            }
        });

            

    }

    private void goLogin() {

    }



    @Override
    public void onBackPressedSupport() {

//        super.onBackPressedSupport();
        if (mDrawer.isDrawerOpen(GravityCompat.START)) {
            mDrawer.closeDrawer(GravityCompat.START);
        } else {
            ISupportFragment topFragment = getTopFragment();

            // 主页的Fragment
            if (topFragment instanceof BaseMainFragment) {
                mNavigationView.setCheckedItem(R.id.nav1);
            }

            if (getSupportFragmentManager().getBackStackEntryCount() > 1) {
                pop();
            } else {
//                Toaster.initCenter("back else");

                //启动双击返回
                startDoubleBackClick(new DoubleBackClick() {
                    @Override
                    public void onToast() {
                        Toaster.show("再点一次退出");
                    }

                    @Override
                    public void onExited() {
                        finish();
                    }
                });


            }
        }
    }

    /**
     * 打开抽屉
     */
    @Override
    public void onOpenDrawer() {
        if (!mDrawer.isDrawerOpen(GravityCompat.START)) {
            mDrawer.openDrawer(GravityCompat.START);
        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull final MenuItem item) {
        mDrawer.closeDrawer(GravityCompat.START);
        mDrawer.postDelayed(new Runnable() {
            @Override
            public void run() {

                BaseSupportFragment topFragment = (BaseSupportFragment) getTopFragment();

                switch (item.getItemId()) {
                    case R.id.nav1:
                        MaterialDesignFragment fragment1 = findFragment(MaterialDesignFragment.class);
                        if (fragment1 == null) {
                            topFragment.startWithPopTo(MaterialDesignFragment.newInstance(""), MaterialDesignFragment.class, false);

                        } else {
                            topFragment.start(fragment1, SupportFragment.SINGLETASK);
                        }
                        break;
                    case R.id.nav2:
                        CardViewFragment fragment2 = findFragment(CardViewFragment.class);
                        if (fragment2 == null) {
                            topFragment.startWithPopTo(CardViewFragment.newInstance(), MaterialDesignFragment.class, false);
                        } else {
                            topFragment.start(fragment2, SupportFragment.SINGLETASK);
                        }
                        break;
                    case R.id.nav3:
                        SwipeRefreshFragment fragment3 = findFragment(SwipeRefreshFragment.class);
                        if (fragment3 == null) {
                            topFragment.startWithPopTo(SwipeRefreshFragment.newInstance(), MaterialDesignFragment.class, false);
                        } else {
                            topFragment.start(fragment3, SupportFragment.SINGLETASK);
                        }
                        break;
                    case R.id.nav4:
                        break;
                    case R.id.nav5:
                        break;
                }


            }
        }, 300);
        return false;
    }
}
