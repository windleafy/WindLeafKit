package me.windleafy.kit.material.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import me.windleafy.kit.R;
import me.windleafy.kit.material.fragment.base.MaterialBackFragment;
import me.windleafy.kity.android.wiget.debug.DebugView;
import me.windleafy.kity.android.wiget.toast.Toaster;

/**
 * 测试模板Fragment
 */
public class CardViewFragment extends MaterialBackFragment implements View.OnClickListener {

    public static final String TAG = CardViewFragment.class.getSimpleName();

    private static final String ARG_TITLE = "arg_title";

    private Toolbar mToolbar;
    private String mTitle;
    private DebugView debugLayout;


    public static CardViewFragment newInstance() {
        return newInstance("CardView");
    }

    public static CardViewFragment newInstance(String title) {
        CardViewFragment fragment = new CardViewFragment();
        Bundle bundle = new Bundle();
        bundle.putString(ARG_TITLE, title);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle bundle = getArguments();
        if (bundle != null) {
            mTitle = bundle.getString(ARG_TITLE);
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_card_view, container, false);
        initView(view);

        return view;
    }

    private void initView(View view) {
        mToolbar = (Toolbar) view.findViewById(R.id.toolbar);
        mToolbar.setTitle(mTitle);
        initToolbarNav(mToolbar);
        CardView cardView = view.findViewById(R.id.card_view);
        cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Toaster.show("CardView onClick");
            }
        });
        ImageView imageView = view.findViewById(R.id.card_image);
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Toaster.show("ImageView onClick");
            }
        });
//
//        CardView card1 = view.findViewById(R.id.card1);
//        card1.setOnClickListener(this);
//        CardView card2 = view.findViewById(R.id.card2);
//        card2.setOnClickListener(this);
//        CardView card3= view.findViewById(R.id.card3);
//        card3.setOnClickListener(this);
//        CardView card4 = view.findViewById(R.id.card4);
//        card4.setOnClickListener(this);
//        CardView card5 = view.findViewById(R.id.card5);
//        card5.setOnClickListener(this);
//        CardView card6 = view.findViewById(R.id.card6);
//        card6.setOnClickListener(this);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initDebug();
    }

    protected void initDebug() {
        debugLayout = new DebugView(this, R.id.fragment_container);
        debugLayout.show(true);
        debugLayout.setVisibility(View.VISIBLE);
        debugLayout.setTitle("CardView");
        //        debugLayout.showEditLayout();

        //Button
        debugLayout.setButtonText("TOP", "CENTER", "BOTTOM", "VIEW");
        debugLayout.setDebugClickListener(new DebugView.DebugClickListener() {
            @Override
            public void onClick(View v, int index) {
                Toaster.show("button index = " + index);
                switch (index) {
                    case 1:
                        break;
                    case 2:
                        break;
                    case 3:
                        break;
                    case 4:
                        break;
                }
            }
        });

        debugLayout.setDebugClickListener2(new DebugView.DebugClickListener() {
            @Override
            public void onClick(View v, int index) {
                Toaster.show("button2 index = " + index);
                switch (index) {
                    case 1:
                        debugLayout.setText("CardView click");
                        break;
                    case 2:
                        break;
                    case 3:
                        break;
                    case 4:
                        break;
                }
            }
        });
    }


    @Override
    public void onClick(View v) {

    }
}
