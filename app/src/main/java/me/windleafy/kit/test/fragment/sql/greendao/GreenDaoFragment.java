//package me.windleafy.kit.test.fragment.sql.greendao;
//
//import android.os.Bundle;
//import android.support.annotation.NonNull;
//import android.support.annotation.Nullable;
//import android.support.v7.widget.Toolbar;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//
//import java.util.List;
//
//import me.windleafy.kit.R;
//import me.windleafy.kit.demo_flow.base.BaseBackFragment;
//import me.windleafy.kit.test.fragment.sql.greendao.controller.DbController;
//import me.windleafy.kit.test.fragment.sql.greendao.model.PersonInfor;
//import me.windleafy.kity.android.wiget.debug.DebugView;
//
///**
// * 测试模板Fragment
// */
//public class GreenDaoFragment extends BaseBackFragment {
//
//    public static final String TAG = GreenDaoFragment.class.getSimpleName();
//
//    private static final String ARG_TITLE = "arg_title";
//
//    private Toolbar mToolbar;
//    private String mTitle;
//    private DebugView debugLayout;
//
//    private DbController mDbController;
//    private PersonInfor personInfor1, personInfor2, personInfor3, personInfor4;
//
//    public static GreenDaoFragment newInstance(String title) {
//        GreenDaoFragment fragment = new GreenDaoFragment();
//        Bundle bundle = new Bundle();
//        bundle.putString(ARG_TITLE, title);
//        fragment.setArguments(bundle);
//        return fragment;
//    }
//
//    @Override
//    public void onCreate(@Nullable Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//
//        Bundle bundle = getArguments();
//        if (bundle != null) {
//            mTitle = bundle.getString(ARG_TITLE);
//        }
//    }
//
//    @Nullable
//    @Override
//    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
//        View view = inflater.inflate(R.layout.fragment_debug_view, container, false);
//        initView(view);
//
//        return view;
//    }
//
//    private void initView(View view) {
//        mToolbar = (Toolbar) view.findViewById(R.id.toolbar);
//        mToolbar.setTitle(mTitle);
//        initToolbarNav(mToolbar);
//    }
//
//    @Override
//    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
//        super.onViewCreated(view, savedInstanceState);
//        initDebug();
//
//        mDbController = DbController.getInstance(_mActivity);
//        similateData();
//    }
//
//    protected void initDebug() {
//        debugLayout = new DebugView(this, R.id.debug_container);
//        debugLayout.show(true);
////        debugLayout.showEditLayout();
//
//        //Button
//        debugLayout.setButtonText("增", "删", "改", "查");
//        debugLayout.setDebugClickListener(new DebugView.DebugClickListener() {
//            @Override
//            public void onClick(View v, int index) {
//                switch (index) {
//                    case 1:
//                        //Add
//                        mDbController.insertOrReplace(personInfor1);
//                        mDbController.insertOrReplace(personInfor2);
//                        mDbController.insertOrReplace(personInfor3);
//                        mDbController.insertOrReplace(personInfor4);
//                        showDataList();
//                        break;
//                    case 2:
//                        //Delete
//                        mDbController.delete("王麻麻");
//                        showDataList();
//                        break;
//                    case 3:
//                        //Update
//                        mDbController.update(personInfor1);
//                        showDataList();
//                        break;
//                    case 4:
//                        //Search
//                        showDataList();
//                        break;
//                }
//            }
//        });
//
//    }
//
//    private void similateData() {
//        personInfor1 = new PersonInfor(null, "001", "王大宝", "男",50);
//        personInfor2 = new PersonInfor(null, "002", "李晓丽", "女",51);
//        personInfor3 = new PersonInfor(null, "003", "王麻麻", "男",52);
//        personInfor4 = new PersonInfor(null, "004", "王大锤", "女",53);
//    }
//
//
//
//    private void showDataList() {
//        StringBuilder sb = new StringBuilder();
//        List<PersonInfor> personInfors = mDbController.searchAll();
//        for (PersonInfor personInfor : personInfors) {
//            // dataArea.setText("id:"+p);
//            sb.append("id:").append(personInfor.getId())
//                    .append("perNo:").append(personInfor.getPerNo())
//                    .append("name:").append(personInfor.getName())
//                    .append("sex:").append(personInfor.getSex())
//                    .append("\n");
//        }
//        debugLayout.setScrollText(sb.toString());
//    }
//
//
//}
