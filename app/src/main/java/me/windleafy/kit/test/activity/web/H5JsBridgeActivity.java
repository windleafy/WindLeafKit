package me.windleafy.kit.test.activity.web;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.webkit.ValueCallback;
import android.widget.Button;
import android.widget.Toast;

import com.github.lzyzsd.jsbridge.BridgeHandler;
import com.github.lzyzsd.jsbridge.BridgeWebView;
import com.github.lzyzsd.jsbridge.CallBackFunction;
import com.google.gson.Gson;
import com.umeng.socialize.ShareAction;
import com.umeng.socialize.UMShareListener;
import com.umeng.socialize.bean.SHARE_MEDIA;
import com.umeng.socialize.media.UMImage;
import com.umeng.socialize.media.UMWeb;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import me.windleafy.kit.R;
import me.windleafy.kity.android.base.activity.BaseSupportActivity;
import me.windleafy.kity.android.utils.ScreenKit;
import me.windleafy.kity.android.wiget.debug.DebugView;
import me.windleafy.kity.android.wiget.toast.Toaster;

public class H5JsBridgeActivity extends BaseSupportActivity {

    private static final String TAG = H5JsBridgeActivity.class.getSimpleName();

    private DebugView debugLayout;

    private BridgeWebView mWebView;

    int RESULT_CODE = 0;

    ValueCallback<Uri> mUploadMessage;

    ValueCallback<Uri[]> mUploadMessageArray;

    static class Location {
        String address;
    }

    static class User {
        String name;
        Location location;
        String testStr;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_h5_js_bridge);

        init();
    }

    public void init() {
        initView();
    }

    public void initView() {
//        initDebug();
//        initScreen();
        initWeb();

    }

    private void initWeb() {
        mWebView = findViewById(R.id.web_view);

        //本地
//        mWebView.loadUrl("file:///android_asset/demo.html");

//        mWebView.loadUrl("http://192.168.0.111/jhgk/rest/qrCode/myQrList");
        mWebView.loadUrl("http://192.168.0.62/jhgk/rest/qrCode/myQrList");

        // Web端调用
        // 对应：window.WebViewJavascriptBridge.send()
//        mWebView.setDefaultHandler(new DefaultHandler());
        mWebView.setDefaultHandler(new BridgeHandler() {
            @Override
            public void handler(String data, CallBackFunction function) {
                Toaster.show("setDefaultHandler：" + data);
                function.onCallBack("[Android setDefaultHandler onCallBack]:" + data);
            }
        });

        // Web端调用
        // 对应：window.WebViewJavascriptBridge.callHandler(
        //                'submitFromWeb'
        mWebView.registerHandler("submitFromWeb", new BridgeHandler() {

            @Override
            public void handler(String data, CallBackFunction function) {
                Toaster.show("registerHandler：" + data);
                function.onCallBack("[Android registerHandler onCallBack]：" + data);
            }

        });

        /*
            window.WebViewJavascriptBridge.callHandler(
                'exit'
                , ''
                , function(responseData) {

                }
            );
        *
        * */
        mWebView.registerHandler("exit", new BridgeHandler() {
            @Override
            public void handler(String data, CallBackFunction function) {
                Toaster.show("退出");
                finish();
            }
        });

        mWebView.registerHandler("json", new BridgeHandler() {
            @Override
            public void handler(String data, CallBackFunction function) {
                Toaster.show("JSON");
//                JSON json = JSON.parseArray()
            }

        });

        mWebView.registerHandler("sharePic", new BridgeHandler() {

            @Override
            public void handler(String data, CallBackFunction function) {

                Toaster.show("sharePic");
//                function.onCallBack("[Android registerHandler onCallBack]：" + data);

                Log.i(TAG, "handler = shareLink, data from web = " + data);

                Map<String, Object> map = new HashMap<>();
                Gson gson = new Gson();
                map = gson.fromJson(data, map.getClass());
                System.out.println("map" + map);
                /*shareWay:shareWay,
                        sharePic:sharePic*/
//                String isLink = (String) map.get("isLink");

                String type = (String) map.get("type");
                String pic = (String) map.get("pic");

                SHARE_MEDIA platform = null;

                //TEST
//                shareWay = "save";

                switch (type) {
                    case "qq":
                        platform = SHARE_MEDIA.QQ;
                        break;
                    case "qqZone":
                        platform = SHARE_MEDIA.QZONE;
                        break;
                    case "weiXin":
                        platform = SHARE_MEDIA.WEIXIN;
                        break;
                    case "pyq":
                        platform = SHARE_MEDIA.WEIXIN_CIRCLE;
                        break;
                    case "weibo":
                        platform = SHARE_MEDIA.SINA;
                        break;
                }

                try {
                    byte[] imageBytes = Base64.decode(pic, Base64.DEFAULT);
                    if (platform != null) {
                        UMImage image = new UMImage(H5JsBridgeActivity.this, imageBytes);//资源文件
//                        UMImage thumb =  new UMImage(ShareActivity.this, R.mipmap.pic_no_collection);
//                        image.setThumb(thumb);
                        new ShareAction(H5JsBridgeActivity.this)
                                .setPlatform(platform)//传入平台
//                                .withText("abc")
                                .withMedia(image)
                                .setCallback(shareListener)//回调监听器
                                .share();

                    } else {
                        saveBitmap("jh" + new Date().getTime() + ".png", imageBytes);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

        });

        mWebView.registerHandler("shareLink", new BridgeHandler() {

            @Override
            public void handler(String data, CallBackFunction function) {

                Toaster.show("shareLink");
//                function.onCallBack("[Android registerHandler onCallBack]：" + data);

                Log.i(TAG, "handler = shareLink, data from web = " + data);

                Map<String, Object> map = new HashMap<>();
                Gson gson = new Gson();
                map = gson.fromJson(data, map.getClass());
                System.out.println("map" + map);
                /*shareWay:shareWay,
                        sharePic:sharePic*/
//                String isLink = (String) map.get("isLink");

                String type = (String) map.get("type");
                String url = (String) map.get("url");
                String title = (String) map.get("title");
                String content = (String) map.get("content");

                SHARE_MEDIA platform = null;

                //TEST
//                shareWay = "save";

                switch (type) {
                    case "qq":
                        platform = SHARE_MEDIA.QQ;
                        break;
                    case "qqZone":
                        platform = SHARE_MEDIA.QZONE;
                        break;
                    case "weiXin":
                        platform = SHARE_MEDIA.WEIXIN;
                        break;
                    case "pyq":
                        platform = SHARE_MEDIA.WEIXIN_CIRCLE;
                        break;
                    case "weibo":
                        platform = SHARE_MEDIA.SINA;
                        break;
                }

                if (platform != null) {
                    UMImage thumb = new UMImage(H5JsBridgeActivity.this, R.mipmap.launcher);
                    thumb.compressStyle = UMImage.CompressStyle.SCALE;//大小压缩，默认为大小压缩，适合普通很大的图
                    thumb.compressFormat = Bitmap.CompressFormat.PNG;//用户分享透明背景的图片可以设置这种方式，但是qq好友，微信朋友圈，不支持透明背景图片，会变成黑色

                    UMWeb web = new UMWeb(url);
                    web.setTitle(title);//标题
                    web.setDescription(content);//描述
                    web.setThumb(thumb);  //缩略图

                    new ShareAction(H5JsBridgeActivity.this)
                            .setPlatform(platform)//传入平台
                            .withMedia(web)
                            .setCallback(shareListener)//回调监听器
                            .share();

                } else {
                }

            }

        });


        Button button = (Button) findViewById(R.id.button);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Android端调用
                // 对应：
                // connectWebViewJavascriptBridge(function(bridge) {
                //      ......
                //      bridge.registerHandler("functionInJs", function(data, responseCallback)
                // }
                mWebView.callHandler("functionInJs", "Android onClick", new CallBackFunction() {
                    @Override
                    public void onCallBack(String data) {
                        Toaster.show("callHandler：" + data);
                        Log.i(TAG, "[button onClick]：" + data);
                    }

                });
            }
        });

        Button button2 = (Button) findViewById(R.id.button2);
        button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final User user = new User();
                Location location = new Location();
                location.address = "地址";
                user.location = location;
                user.name = "大头鬼";
                // Android端调用
                // 对应：
                // connectWebViewJavascriptBridge(function(bridge) {
                //      ......
                //      bridge.registerHandler("functionInJs", function(data, responseCallback)
                // }
                mWebView.callHandler("functionInJs", new Gson().toJson(user), new CallBackFunction() {
                    @Override
                    public void onCallBack(String data) {
                        Toaster.show("callHandler：" + data);
                        Log.i(TAG, "[button onClick]：" + data);
                    }
                });
            }
        });

        //???
        mWebView.send("[mWebView.send]");

    }

    private void sharePic(SHARE_MEDIA platform, String pic) {
        //解密
        try {
            byte[] imageBytes = Base64.decode(pic, Base64.DEFAULT);
            if (platform != null) {
                UMImage image = new UMImage(H5JsBridgeActivity.this, imageBytes);//资源文件
//                        UMImage thumb =  new UMImage(ShareActivity.this, R.mipmap.pic_no_collection);
//                        image.setThumb(thumb);
                new ShareAction(H5JsBridgeActivity.this)
                        .setPlatform(platform)//传入平台
//                                .withText("abc")
                        .withMedia(image)
                        .setCallback(shareListener)//回调监听器
                        .share();

            } else {
                saveBitmap("jh" + new Date().getTime() + ".jpg", imageBytes);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void shareLink(SHARE_MEDIA platform, String shareContent) {

    }

    private void saveImage(byte[] imageBytes) {
    }

    public void saveBitmap(String imgName, byte[] bytes) {
        if (Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)) {
            String filePath = null;
            FileOutputStream fos = null;
            try {
                filePath = Environment.getExternalStorageDirectory().getCanonicalPath() + "/Jinhong";
                File imgDir = new File(filePath);
                if (!imgDir.exists()) {
                    imgDir.mkdirs();
                }
                imgName = filePath + "/" + imgName;
                fos = new FileOutputStream(imgName);
                fos.write(bytes);
                Toast.makeText(this, "图片已保存到" + filePath, Toast.LENGTH_SHORT).show();
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                try {
                    if (fos != null) {
                        fos.close();
                    }
                    //这个广播的目的就是更新图库，发了这个广播进入相册就可以找到你保存的图片了！，记得要传你更新的file哦
                    Intent intent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
                    Uri uri = Uri.fromFile(new File(imgName));
                    intent.setData(uri);
                    sendBroadcast(intent);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        } else {
            Toast.makeText(this, "请检查SD卡是否可用", Toast.LENGTH_SHORT).show();
        }

    }


    private UMShareListener shareListener = new UMShareListener() {
        /**
         * @param platform 平台类型
         * @descrption 分享开始的回调
         */
        @Override
        public void onStart(SHARE_MEDIA platform) {
        }

        /**
         * @descrption 分享成功的回调
         * @param platform 平台类型
         */
        @Override
        public void onResult(SHARE_MEDIA platform) {
            Toast.makeText(H5JsBridgeActivity.this, "成功", Toast.LENGTH_LONG).show();
        }

        /**
         * @descrption 分享失败的回调
         * @param platform 平台类型
         * @param t 错误原因
         */
        @Override
        public void onError(SHARE_MEDIA platform, Throwable t) {
            Toast.makeText(H5JsBridgeActivity.this, "失败" + t.getMessage(), Toast.LENGTH_LONG).show();
        }

        /**
         * @descrption 分享取消的回调
         * @param platform 平台类型
         */
        @Override
        public void onCancel(SHARE_MEDIA platform) {
            Toast.makeText(H5JsBridgeActivity.this, "取消", Toast.LENGTH_LONG).show();
        }
    };


    public void pickFile() {
        Intent chooserIntent = new Intent(Intent.ACTION_GET_CONTENT);
        chooserIntent.setType("image/*");
        startActivityForResult(chooserIntent, RESULT_CODE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
        if (requestCode == RESULT_CODE) {
            if (null == mUploadMessage && null == mUploadMessageArray) {
                return;
            }
            if (null != mUploadMessage && null == mUploadMessageArray) {
                Uri result = intent == null || resultCode != RESULT_OK ? null : intent.getData();
                mUploadMessage.onReceiveValue(result);
                mUploadMessage = null;
            }

            if (null == mUploadMessage && null != mUploadMessageArray) {
                Uri result = intent == null || resultCode != RESULT_OK ? null : intent.getData();
                mUploadMessageArray.onReceiveValue(new Uri[]{result});
                mUploadMessageArray = null;
            }

        }
    }


    private void initScreen() {

        DisplayMetrics metrics = ScreenKit.getDisplayMetrics(H5JsBridgeActivity.this);

        debugLayout.setScrollText("width:" + metrics.widthPixels);
        debugLayout.setScrollText("height:" + metrics.heightPixels);
        debugLayout.setScrollText("widthDp:" + metrics.widthPixels / metrics.density);
        debugLayout.setScrollText("heightDp:" + metrics.heightPixels / metrics.density);
        debugLayout.setScrollText("density:" + metrics.density);
        debugLayout.setScrollText("scaledDensity" + metrics.scaledDensity);
        debugLayout.setScrollText("densityDpi:" + metrics.densityDpi);
    }


    //放在initView中，这样屏蔽initDebug后，其它地方使用debugView的显示功能才不会报NULL
    protected void initDebug() {
        debugLayout = new DebugView(this);
        debugLayout.show(true);
        debugLayout.setVisibility(View.VISIBLE);
//        debugLayout.showEditLayout();

        //Button
        debugLayout.setButtonText("One", "Tow", "Three", "Four");
        debugLayout.setDebugClickListener(new DebugView.DebugClickListener() {
            @Override
            public void onClick(View v, int index) {
                switch (index) {
                    case 1:
                        mWebView.callHandler("functionInJs", "data from Java", new CallBackFunction() {
                            @Override
                            public void onCallBack(String data) {
                                Log.i(TAG, "reponse data from js " + data);
                                Toaster.show(data);
                            }
                        });
                        break;
                    case 2:
                        break;
                    case 3:
                        break;
                    case 4:
                        break;
                }
            }
        });

        debugLayout.setDebugSubClickListener(new DebugView.DebugSubClickListener() {
            @Override
            public void onClick(View v, int index) {
                Toaster.show("index = " + index);
                switch (index) {
                    case 1:
                        break;
                    case 2:
                        break;
                    case 3:
                        break;
                    case 4:
                        break;
                }
            }
        });


    }


}
