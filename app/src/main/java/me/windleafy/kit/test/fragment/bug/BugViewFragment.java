package me.windleafy.kit.test.fragment.bug;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import me.windleafy.kit.R;
import me.windleafy.kit.demo_flow.base.BaseBackFragment;
import me.windleafy.kit.demo_flow.listener.OnItemClickListener;
import me.windleafy.kity.android.bug.FixBugLinearLayoutManager;
import me.windleafy.kity.android.wiget.toast.Toaster;


public class BugViewFragment extends BaseBackFragment {

    public static final String TAG = BugViewFragment.class.getSimpleName();

    private static final String ARG_TITLE = "arg_title";

    private Toolbar mToolbar;
    private String mTitle;
    private ArrayList<String> mList;

    public static BugViewFragment newInstance(String title) {
        BugViewFragment fragment = new BugViewFragment();
        Bundle bundle = new Bundle();
        bundle.putString(ARG_TITLE, title);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle bundle = getArguments();
        if (bundle != null) {
            mTitle = bundle.getString(ARG_TITLE);
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_bug_view, container, false);
        initView(view);

        return view;
    }

    private void initView(View view) {
        mToolbar = (Toolbar) view.findViewById(R.id.toolbar);
        mToolbar.setTitle(mTitle);
        initToolbarNav(mToolbar);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initDebug(view);
    }

    private RecyclerView mRecy;
    private BugViewAdapter mAdapter;

    protected void initDebug(View view) {
        view.findViewById(R.id.click).setOnClickListener(new View.OnClickListener() {
            private static final int sum = 10;

            @Override
            public void onClick(View v) {
                //崩溃1
//                mAdapter.add("" + RandomKit.getRandom(0, 99));
//                mAdapter.notifyItemInserted(0);
//                mAdapter.notifyItemInserted(0);

                //崩溃2
                mAdapter.remove(0);
                mAdapter.remove(0);
                mAdapter.notifyItemRemoved(0);


                //始终显示新加入item为第一位
//                mAdapter.add(0, "" + RandomKit.getRandom(0, 99));
//                //超出时删除末尾项
//                if(mAdapter.size() > sum) {
//                    mAdapter.remove(sum);
//                    mAdapter.notifyItemRemoved(sum);
//                }
//                mAdapter.notifyItemInserted(0);

//                mAdapter.remove(0);
//                mAdapter.notifyDataSetChanged();
            }

        });

        mRecy = (RecyclerView) view.findViewById(R.id.recy);

        mAdapter = new BugViewAdapter(_mActivity);
        FixBugLinearLayoutManager manager = new FixBugLinearLayoutManager(_mActivity);
        mRecy.setLayoutManager(manager);
        mRecy.setAdapter(mAdapter);

        mAdapter.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(int position, View view) {
                Toaster.show("pos = " + position);
            }
        });

        mRecy.post(new Runnable() {
            @Override
            public void run() {
                // Init Datas
                mList = new ArrayList<>();
                for (int i = 0; i < 5; i++) {
                    mList.add(getString(R.string.recommend) + " " + i);
                }
                mAdapter.setItems(mList);
                mAdapter.notifyDataSetChanged();
            }
        });


    }


}
