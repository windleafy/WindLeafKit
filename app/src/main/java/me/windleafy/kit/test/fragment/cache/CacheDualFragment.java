package me.windleafy.kit.test.fragment.cache;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.blankj.utilcode.util.CacheDoubleUtils;

import me.windleafy.kit.R;
import me.windleafy.kit.demo_flow.base.BaseBackFragment;
import me.windleafy.kity.android.wiget.debug.DebugView;

/**
 *
 */
public class CacheDualFragment extends BaseBackFragment {

    public static final String TAG = CacheDualFragment.class.getSimpleName();

    private static final String ARG_TITLE = "arg_title";

    private Toolbar mToolbar;
    private String mTitle;
    private DebugView debugLayout;

    public static CacheDualFragment newInstance(String title) {
        CacheDualFragment fragment = new CacheDualFragment();
        Bundle bundle = new Bundle();
        bundle.putString(ARG_TITLE, title);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle bundle = getArguments();
        if (bundle != null) {
            mTitle = bundle.getString(ARG_TITLE);
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_debug_view, container, false);
        initView(view);

        return view;
    }

    private void initView(View view) {
        mToolbar = (Toolbar) view.findViewById(R.id.toolbar);
        mToolbar.setTitle(mTitle);
        initToolbarNav(mToolbar);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initDebug();
    }

    protected void initDebug() {
        debugLayout = new DebugView(this, R.id.debug_container);
        debugLayout.show(true);
//        debugLayout.showEditLayout();

        //Button
        debugLayout.setButtonText("put string", "put currentTime", "put student", "remove");
        debugLayout.setDebugClickListener(new DebugView.DebugClickListener() {
            @Override
            public void onClick(View v, int index) {
                switch (index) {
                    case 1:
                        CacheDoubleUtils.getInstance().put("string", "abc");
                        debugLayout.setScrollText("put(\"string\", \"abc\")");
                        break;
                    case 2:
                        CacheDoubleUtils.getInstance().put("currentTime", "time3s", 3);
                        debugLayout.setScrollText("put(\"currentTime\", \"time3s\", 3)");
                        break;
                    case 3:
                        CacheDoubleUtils.getInstance().put("student", new Student("yangyong", 23));
                        debugLayout.setScrollText("put(\"student\", new Student(\"yangyong\", 23))");
                        break;
                    case 4:
                        CacheDoubleUtils.getInstance().remove("string");
                        debugLayout.setScrollText("remove(\"string\")");
                        String get1 = CacheDoubleUtils.getInstance().getString("string", "xxx");
                        debugLayout.setScrollText("getString(\"string\", \"xxx\")");
                        debugLayout.setScrollText("result = " + get1);
                        break;
                }
            }
        });

        //Button2
        debugLayout.setButtonText2("get string", "get currentTime", "get student", "clear");
        debugLayout.setDebugClickListener2(new DebugView.DebugClickListener() {
            @Override
            public void onClick(View v, int index) {
                switch (index) {
                    case 1:
                        String get1 = CacheDoubleUtils.getInstance().getString("string", "xxx");
                        debugLayout.setScrollText("getString(\"string\", \"xxx\")");
                        debugLayout.setScrollText("result = " + get1);
                        break;
                    case 2:
                        String get2 = CacheDoubleUtils.getInstance().getString("currentTime", "xxx");
                        debugLayout.setScrollText("getString(\"currentTime\", \"xxx\")");
                        debugLayout.setScrollText("result = " + get2);
                        break;
                    case 3:
                        Student get3 = (Student) CacheDoubleUtils.getInstance().getSerializable("student", new Student("xxx", 0));
                        debugLayout.setScrollText("getSerializable(\"student\", new Student(\"xxx\", 0))");
                        debugLayout.setScrollText("result = " + get3.toString());
                        break;
                    case 4:

                        CacheDoubleUtils.getInstance().clear();
                        debugLayout.setScrollText("clear()");
                        debugLayout.setScrollText("getString(\"string\", \"xxx\")");
                        debugLayout.setScrollText("result = " + CacheDoubleUtils.getInstance().getString("string", "xxx"));
                        debugLayout.setScrollText("getString(\"currentTime\", \"xxx\")");
                        debugLayout.setScrollText("result = " + CacheDoubleUtils.getInstance().getString("currentTime", "xxx"));
                        debugLayout.setScrollText("getSerializable(\"student\", new Student(\"xxx\", 0))");
                        debugLayout.setScrollText("result = " + CacheDoubleUtils.getInstance().getSerializable("student", new Student("xxx", 0)).toString());
                        break;
                }
            }
        });

        debugLayout.setDebugSubClickListener(new DebugView.DebugSubClickListener() {
            @Override
            public void onClick(View v, int index) {
                debugLayout.setScrollText("index = " + index);
                switch (index) {
                    case 1:
                        break;
                    case 2:
                        break;
                    case 3:
                        break;
                    case 4:
                        break;
                    //........
                }
            }
        });


    }


}
