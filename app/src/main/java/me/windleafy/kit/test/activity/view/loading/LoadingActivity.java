package me.windleafy.kit.test.activity.view.loading;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import me.windleafy.kit.R;
import me.windleafy.kity.android.utils.InflaterKit;
import me.windleafy.kity.android.utils.ToastKit;
import me.windleafy.kity.android.view.matrix.ViewArray;
import me.windleafy.kity.android.view.matrix.dynamic.ButtonArray;
import me.windleafy.kity.android.wiget.layout.LayoutUtil;
import me.windleafy.kity.android.wiget.layout.LoadingUtil;
import me.windleafy.kity.java.utils.DateKit;

public class LoadingActivity extends AppCompatActivity {

    private View view1;
    private View view2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_loading);

        view1 = InflaterKit.inflate(this, R.layout.inflate_loading_view1);
        view2 = InflaterKit.inflate(this, R.layout.inflate_loading_view2);

        //设置true消耗触摸事件，不可点击
        view1.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return true;
            }
        });


        Button button = view2.findViewById(R.id.button);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ToastKit.show("view2 click button");
            }
        });

        initTestMaMatrix();
    }

    /**
     * 测试ViewMatrix
     */
    private void initTestMaMatrix() {

        String[] text = new String[]{"add view1", "add view2", "hide view1", "hide view2", "hideAll","setCenter view1 currentTime"};
        ButtonArray buttonArray = new ButtonArray(this, R.id.array_container, text.length)
                .init(text);
        buttonArray.setItemOnClickListener(new ViewArray.ItemOnClickListener() {
            @Override
            public void onItemClick(int i) {
                switch (i) {
                    case 0:
                        LayoutUtil.show(LoadingActivity.this, view1);
                        break;
                    case 1:
                        LoadingUtil.show(LoadingActivity.this, view2);
                        break;
                    case 2:
                        LayoutUtil.hide(view1);
                        break;
                    case 3:
                        LoadingUtil.hide(view2);
                        break;
                    case 4:
                        LoadingUtil.hideAll();
                        break;
                    case 5:
                        TextView category_txt = view1.findViewById(R.id.category_txt);
                        category_txt.setText( DateKit.time());
                        break;
                }
            }
        });

    }


}
