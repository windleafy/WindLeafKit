package me.windleafy.kit.test.fragment.adapter;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.zhy.adapter.recyclerview.CommonAdapter;
import com.zhy.adapter.recyclerview.MultiItemTypeAdapter;
import com.zhy.adapter.recyclerview.base.ItemViewDelegate;
import com.zhy.adapter.recyclerview.base.ViewHolder;

import java.util.ArrayList;
import java.util.List;

import me.windleafy.kit.R;
import me.windleafy.kit.demo_flow.base.BaseBackFragment;

/**
 * https://github.com/hongyangAndroid/baseAdapter
 */
public class MultAdapterFragment extends BaseBackFragment {

    public static final String TAG = MultAdapterFragment.class.getSimpleName();
    private static final int REQ_MODIFY_FRAGMENT = 100;

    private static final String ARG_TITLE = "arg_title";
    static final String KEY_RESULT_TITLE = "title";

    private Toolbar mToolbar;
    private TextView mTvContent;
    private FloatingActionButton mFab;
    private String mTitle;
    private MultiItemTypeAdapter<String> mMultAdapter;
    private List<String> mDatas = new ArrayList<>();
    private RecyclerView mRecyclerView;

    public static MultAdapterFragment newInstance(String title) {
        MultAdapterFragment fragment = new MultAdapterFragment();
        Bundle bundle = new Bundle();
        bundle.putString(ARG_TITLE, title);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle bundle = getArguments();
        if (bundle != null) {
            mTitle = bundle.getString(ARG_TITLE);
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_mult_adapter, container, false);
        initView(view);

        return view;
    }

    private void initView(View view) {
        mToolbar = (Toolbar) view.findViewById(R.id.toolbar);

        mToolbar.setTitle(mTitle);

        initToolbarNav(mToolbar);

        mRecyclerView = view.findViewById(R.id.recycler_view);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(_mActivity));
        for (int i = 'A'; i <= 'z'; i++) {
            mDatas.add((char) i + "");
        }

        mMultAdapter = new MultiItemTypeAdapter<>(_mActivity, mDatas);
        mMultAdapter.addItemViewDelegate(new ItemViewDelegate<String>() {
            @Override
            public int getItemViewLayoutId() {
                return R.layout.lv_item_mult1;
            }

            @Override
            public boolean isForViewType(String item, int position) {
                return position % 3 == 0 || position % 5 == 0;
            }

            @Override
            public void convert(ViewHolder holder, String s, int position) {
                holder.setText(R.id.title, s + " position:" + position);
            }
        });
        mMultAdapter.addItemViewDelegate(new ItemViewDelegate<String>() {
            @Override
            public int getItemViewLayoutId() {
                return R.layout.lv_item_mult2;
            }

            @Override
            public boolean isForViewType(String item, int position) {
                return !(position % 3 == 0 || position % 5 == 0);
            }

            @Override
            public void convert(ViewHolder holder, String s, int position) {
                holder.setText(R.id.title, s + " position:" + position);
            }
        });
        mMultAdapter.setOnItemClickListener(new MultiItemTypeAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, RecyclerView.ViewHolder holder, int position) {
                Toast.makeText(_mActivity, "pos = " + position, Toast.LENGTH_SHORT).show();
            }

            @Override
            public boolean onItemLongClick(View view, RecyclerView.ViewHolder holder, int position) {
                return false;
            }
        });

        mRecyclerView.setAdapter(mMultAdapter);


    }

    /**
     * 这里演示:
     * 比较复杂的Fragment页面会在第一次start时,导致动画卡顿
     * Fragmentation提供了onEnterAnimationEnd()方法,该方法会在 入栈动画 结束时回调
     * 所以在onCreateView进行一些简单的View初始化(比如 toolbar设置标题,返回按钮; 显示加载数据的进度条等),
     * 然后在onEnterAnimationEnd()方法里进行 复杂的耗时的初始化 (比如FragmentPagerAdapter的初始化 加载数据等)
     */
    @Override
    public void onEnterAnimationEnd(Bundle savedInstanceState) {
        initDelayView();
    }

    private void initDelayView() {


    }

    @Override
    public void onFragmentResult(int requestCode, int resultCode, Bundle data) {
        super.onFragmentResult(requestCode, resultCode, data);

    }

}
