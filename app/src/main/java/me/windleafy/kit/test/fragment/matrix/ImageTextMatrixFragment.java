package me.windleafy.kit.test.fragment.matrix;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import me.windleafy.kit.R;
import me.windleafy.kit.demo_flow.base.BaseBackFragment;
import me.windleafy.kity.android.wiget.debug.DebugView;
import me.windleafy.kity.android.view.matrix.ViewMatrix;
import me.windleafy.kity.android.view.matrix.dynamic.ImageTextMatrix;
import me.windleafy.kity.android.wiget.toast.Toaster;

public class ImageTextMatrixFragment extends BaseBackFragment {

    public static final String TAG = ImageTextMatrixFragment.class.getSimpleName();

    private static final String ARG_TITLE = "arg_title";

    private Toolbar mToolbar;
    private String mTitle;
    private DebugView debugLayout;

    public static ImageTextMatrixFragment newInstance(String title) {
        ImageTextMatrixFragment fragment = new ImageTextMatrixFragment();
        Bundle bundle = new Bundle();
        bundle.putString(ARG_TITLE, title);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle bundle = getArguments();
        if (bundle != null) {
            mTitle = bundle.getString(ARG_TITLE);
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_matrix_image_text, container, false);
        initView(view);

        return view;
    }

    private void initView(View view) {
        mToolbar = (Toolbar) view.findViewById(R.id.toolbar);
        mToolbar.setTitle(mTitle);
        initToolbarNav(mToolbar);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initImageView();
    }


    private void initImageView() {


        Integer[] image = new Integer[]{R.drawable.gity_fragmentation_help
                , R.drawable.gity_fragmentation_help
                , R.drawable.gity_fragmentation_help
                , R.drawable.gity_fragmentation_help
                , R.drawable.gity_fragmentation_help};
        String[] text = new String[]{"text1", "text2", "text3", "text4", "text5"};

        ImageTextMatrix imageTextMatrix = (ImageTextMatrix) new ImageTextMatrix(_mActivity, R.id.matrix_container, text.length)
                .init(image, text);
        imageTextMatrix.setOnItemClickListener(new ViewMatrix.OnClickListener() {
            @Override
            public void OnRowColClick(View view, int row, int column) {
//                Toaster.initCenter("row = " + row + ", column" + column);
            }

            @Override
            public void OnItemViewClick(View view, int item, int index) {
                Toaster.show("item = " + item + ", index" + index);
            }
        });

    }


}
