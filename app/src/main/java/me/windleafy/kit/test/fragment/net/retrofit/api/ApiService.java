package me.windleafy.kit.test.fragment.net.retrofit.api;


import io.reactivex.Observable;
import me.windleafy.kit.test.fragment.net.retrofit.model.Movie;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface  ApiService {
    @GET("top250")
    Observable<Movie> getTopMovie(@Query("start") int start, @Query("count") int count);
}
