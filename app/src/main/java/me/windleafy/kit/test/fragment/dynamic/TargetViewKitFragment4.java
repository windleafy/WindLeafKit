package me.windleafy.kit.test.fragment.dynamic;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import me.windleafy.kit.R;
import me.windleafy.kit.demo_flow.base.BaseBackFragment;
import me.windleafy.kity.android.utils.view.TargetView;
import me.windleafy.kity.android.wiget.debug.DebugView;

public class TargetViewKitFragment4 extends BaseBackFragment {

    public static final String TAG = TargetViewKitFragment4.class.getSimpleName();

    private static final String ARG_TITLE = "arg_title";

    private Toolbar mToolbar;
    private String mTitle;
    private DebugView debugLayout;
    private Fragment fragment;


    public static TargetViewKitFragment4 newInstance(String title) {
        TargetViewKitFragment4 fragment = new TargetViewKitFragment4();
        Bundle bundle = new Bundle();
        bundle.putString(ARG_TITLE, title);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        fragment = this;
        Bundle bundle = getArguments();
        if (bundle != null) {
            mTitle = bundle.getString(ARG_TITLE);
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_targetviewkit4, container, false);
        initView(view);

        return view;
    }

    private void initView(View view) {

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initDebug();
    }

    protected void initDebug() {


        debugLayout = new DebugView(this, R.id.debug_container);
        debugLayout.show(true);
//        debugLayout.showEditLayout();


        final TargetView targetView = new TargetView(this, R.id.target_layout);

        //Button
        debugLayout.setButtonText("addFrameContainer", "addLinearContainer", "", "removeParent");
        debugLayout.setDebugClickListener(new DebugView.DebugClickListener() {
            @Override
            public void onClick(View v, int index) {

                switch (index) {
                    case 1:
                        targetView.addParent(TargetView.LayoutType.FRAME);
                        break;
                    case 2:
                        targetView.addParent(TargetView.LayoutType.LINEAR);
                        break;
                    case 3:
                        break;
                    case 4:
                        targetView.removeParent();
                        break;
                }
            }
        });

        debugLayout.setButtonText2("addView", "removeView");
        debugLayout.setDebugClickListener2(new DebugView.DebugClickListener() {
            @Override
            public void onClick(View v, int index) {
//                final int targetView = R.id.dynamic_layout_root;
                //每次点击resView变化
                switch (index) {
                    case 1:
                        targetView.addView(R.layout.view_res, TargetView.AddType.REPLACE);
                        break;
                    case 2:
                        targetView.removeView(R.layout.view_res, TargetView.AddType.REPLACE);
                        break;
                    case 3:
                        targetView.addView(R.layout.view_res2, TargetView.AddType.AFTER);
                        break;
                    case 4:
                        targetView.removeView(R.layout.view_res2, TargetView.AddType.AFTER);
                        break;
                }
            }
        });


        debugLayout.setDebugSubClickListener(new DebugView.DebugSubClickListener() {
            @Override
            public void onClick(View v, int index) {
                //每次点击resView无变化
                switch (index) {
                    case 1:
                        break;
                    case 2:
                        break;
                    case 3:
                        break;
                    case 4:
                        break;
                    case 5:
                        break;
                    case 6:
                        break;
                    case 7:
                        break;
                    case 8:
                        break;
                }
            }
        });


    }

    @Override
    public void onDestroy() {
        super.onDestroy();
//        TargetViewKit.clear(this);
    }
}
