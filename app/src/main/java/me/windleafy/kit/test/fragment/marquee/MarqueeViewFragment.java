package me.windleafy.kit.test.fragment.marquee;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import me.windleafy.gity.android.view.textview.MarqueeView;
import me.windleafy.kit.R;
import me.windleafy.kit.demo_flow.base.BaseBackFragment;
import me.windleafy.kity.android.wiget.debug.DebugView;

/**
 * 测试模板Fragment
 */
public class MarqueeViewFragment extends BaseBackFragment {

    public static final String TAG = MarqueeViewFragment.class.getSimpleName();

    private static final String ARG_TITLE = "arg_title";

    private Toolbar mToolbar;
    private String mTitle;
    private DebugView debugLayout;

    private MarqueeView marquee_view;
    private MarqueeView marquee_view2;

    public static MarqueeViewFragment newInstance(String title) {
        MarqueeViewFragment fragment = new MarqueeViewFragment();
        Bundle bundle = new Bundle();
        bundle.putString(ARG_TITLE, title);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle bundle = getArguments();
        if (bundle != null) {
            mTitle = bundle.getString(ARG_TITLE);
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_marquee_view, container, false);
        initView(view);

        return view;
    }

    private void initView(View view) {
        mToolbar = (Toolbar) view.findViewById(R.id.toolbar);
        mToolbar.setTitle(mTitle);
        initToolbarNav(mToolbar);


        marquee_view = view.findViewById(R.id.marquee_view);
        String singleInfo = "1、手续费会从提现金额中扣除，实际到账金额为扣除手续费之后的金额。\n" +
                "2、支付宝第三方支付平台会收取一定比例的收单费，比例详见余额提现规则，本平台不收取任何服务费。\n" +
                "3、到账时间为1-7个工作日（不包括节假日）。\n";
        marquee_view.startWithText(singleInfo);


        marquee_view2 = view.findViewById(R.id.marquee_view2);
        List<String> info = new ArrayList<>();
        info.add("1、手续费会从提现金额中扣除，实际到账金额为扣除手续费之后的金额。");
        info.add("2、支付宝第三方支付平台会收取一定比例的收单费，比例详见余额提现规则，本平台不收取任何服务费。");
        info.add("3、到账时间为1-7个工作日（不包括节假日）。");
//        marquee_view2.startWithList(info);
        marquee_view2.startWithList(info, R.anim.gity_marquee_view_bottom_in, R.anim.gity_marquee_view_top_out);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

    }


}
