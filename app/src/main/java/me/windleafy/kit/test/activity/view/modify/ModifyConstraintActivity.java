package me.windleafy.kit.test.activity.view.modify;

import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import me.windleafy.kit.R;
import me.windleafy.kity.android.utils.ScreenKit;
import me.windleafy.kity.android.utils.ViewKit;
import me.windleafy.kity.android.wiget.debug.DebugView;

/**
 * 动态布局，
 */
public class ModifyConstraintActivity extends AppCompatActivity {


    private DebugView debugLayout;
    private TextView mTextView1;
    private TextView mTextView2;
    private TextView mTextView3;
    private TextView mTextView4;

    private LinearLayout mTopLayout;
    private RelativeLayout mBottomLayout;
    private ConstraintLayout mCenterLayout;
    private ConstraintLayout mRootLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_constraint_view_include);
        mTextView1 = findViewById(R.id.text1);
        mTextView2 = findViewById(R.id.text2);
        mTextView3 = findViewById(R.id.text3);
        mTextView4 = findViewById(R.id.text4);

        initDebug();

//        initScreen();

        initLayout();

    }

    private void initLayout() {
        mTopLayout = findViewById(R.id.top_layout);
        mBottomLayout = findViewById(R.id.bottom_layout);
        mCenterLayout = findViewById(R.id.center_layout);
        mRootLayout = findViewById(R.id.layout_root);


    }

    /**
     * 参数：xxxxToXxxx
     * null：-1，parent：0，有数据：R.id.xxxxxxx
     */
    private void setLayoutParam1() {
        ConstraintLayout.LayoutParams topLayoutParams = (ConstraintLayout.LayoutParams) mTopLayout.getLayoutParams();
        ConstraintLayout.LayoutParams bottomLayoutParams = (ConstraintLayout.LayoutParams) mBottomLayout.getLayoutParams();
        int topLayoutParams_topToTop = topLayoutParams.topToTop;
        int bottomLayoutParams_bottomToBottom = bottomLayoutParams.bottomToBottom;

        ConstraintLayout.LayoutParams layoutParams = (ConstraintLayout.LayoutParams) mCenterLayout.getLayoutParams();

//        int top = R.id.top_layout;

//        int topToTop = layoutParams.topToTop;
//        int bottomToBottom = layoutParams.bottomToBottom;
//        int leftToLeft = layoutParams.leftToLeft;
//        int rightToRight = layoutParams.rightToRight;
//
//        int bottomToTop = layoutParams.bottomToTop;
//        int topToBottom = layoutParams.topToBottom;
//        int leftToRight = layoutParams.leftToRight;
//        int rightToLeft = layoutParams.rightToLeft;

        layoutParams.topToTop = 0;
        layoutParams.topToBottom = -1;
        layoutParams.bottomToBottom = 0;
        layoutParams.bottomToTop = -1;

        layoutParams.dimensionRatio = "";

        bottomLayoutParams.topToBottom = R.id.top_layout;

        mCenterLayout.setLayoutParams(layoutParams);
    }

    /**
     * null：-1，parent：0
     */
    private void setLayoutParam2() {
        ConstraintLayout.LayoutParams topLayoutParams = (ConstraintLayout.LayoutParams) mTopLayout.getLayoutParams();
        ConstraintLayout.LayoutParams bottomLayoutParams = (ConstraintLayout.LayoutParams) mBottomLayout.getLayoutParams();
        int topLayoutParams_topToTop = topLayoutParams.topToTop;
        int bottomLayoutParams_bottomToBottom = bottomLayoutParams.bottomToBottom;
        String topLayoutParams_dimensionRatio = topLayoutParams.dimensionRatio;

        ConstraintLayout.LayoutParams layoutParams = (ConstraintLayout.LayoutParams) mCenterLayout.getLayoutParams();

//        int top = R.id.top_layout;

//        int topToTop = layoutParams.topToTop;
//        int bottomToBottom = layoutParams.bottomToBottom;
//        int leftToLeft = layoutParams.leftToLeft;
//        int rightToRight = layoutParams.rightToRight;
//
//        int bottomToTop = layoutParams.bottomToTop;
//        int topToBottom = layoutParams.topToBottom;
//        int leftToRight = layoutParams.leftToRight;
//        int rightToLeft = layoutParams.rightToLeft;

        String dimensionRatio = layoutParams.dimensionRatio;

//        layoutParams.dimensionRatio = -1;

        layoutParams.topToTop = -1;
        layoutParams.topToBottom = R.id.top_layout;

        layoutParams.bottomToBottom = -1;
        layoutParams.bottomToTop = R.id.bottom_layout;

        layoutParams.dimensionRatio = "16:9";


        bottomLayoutParams.topToBottom = R.id.center_layout;

        mCenterLayout.setLayoutParams(layoutParams);
    }


    private void initScreen() {

        DisplayMetrics metrics = ScreenKit.getDisplayMetrics(ModifyConstraintActivity.this);

        debugLayout.setScrollText("width:" + metrics.widthPixels);
        debugLayout.setScrollText("height:" + metrics.heightPixels);
        debugLayout.setScrollText("widthDp:" + metrics.widthPixels / metrics.density);
        debugLayout.setScrollText("heightDp:" + metrics.heightPixels / metrics.density);
        debugLayout.setScrollText("density:" + metrics.density);
        debugLayout.setScrollText("scaledDensity" + metrics.scaledDensity);
        debugLayout.setScrollText("densityDpi:" + metrics.densityDpi);
    }


    //放在initView中，这样屏蔽initDebug后，其它地方使用debugView的显示功能才不会报NULL
    protected void initDebug() {
        debugLayout = new DebugView(this);
        debugLayout.show(true);
        debugLayout.setVisibility(View.VISIBLE);
//        debugLayout.showEditLayout();

        //Button
        debugLayout.setButtonText("setLayoutParam1", "setLayoutParam2");
        debugLayout.setDebugClickListener(new DebugView.DebugClickListener() {
            @Override
            public void onClick(View v, int index) {
                switch (index) {
                    case 1:
                        setLayoutParam1();
                        break;
                    case 2:
                        setLayoutParam2();
                        break;
                    case 3:
                        break;
                    case 4:
                        break;
                }
            }
        });

//        debugLayout.setButtonText2("removeView");
        debugLayout.setDebugClickListener2(new DebugView.DebugClickListener() {
            @Override
            public void onClick(View v, int index) {
                switch (index) {
                    case 1:
//                        setTextWidthHeight(mTextView1, Kit.dp2px(200), Kit.dp2px(50));
                        break;
                    case 2:
//                        setTextWidthHeight(mTextView2, Kit.dp2px(200), Kit.dp2px(50));
                        break;
                    case 3:
//                        setTextWidthHeight(mTextView3, Kit.dp2px(200), Kit.dp2px(50));
                        break;
                    case 4:
//                        setTextWidthHeight(mTextView4, Kit.dp2px(200), Kit.dp2px(50));
                        break;
                }
            }
        });

        debugLayout.setDebugSubClickListener(new DebugView.DebugSubClickListener() {
            @Override
            public void onClick(View v, int index) {
                switch (index) {
                    case 1:
                        break;
                    case 2:
                        break;
                    case 3:
                        break;
                    case 4:
                        break;
                }
            }
        });


    }

    private void showTextInfo(TextView textView) {
        debugLayout.setScrollText("");
        debugLayout.setScrollText("getWidth:" + textView.getWidth());
        debugLayout.setScrollText("getHeight:" + textView.getHeight());
        debugLayout.setScrollText("getMeasuredWidth:" + textView.getMeasuredWidth());
        debugLayout.setScrollText("getMeasuredHeight:" + textView.getMeasuredHeight());
    }

    private void showTextInfoAtViewKit(TextView textView) {
        debugLayout.setScrollText("");
        debugLayout.setScrollText("getWidth:" + ViewKit.getWrapWidthOnCreate(textView));
        debugLayout.setScrollText("getHeight:" + ViewKit.getWrapHeightOnCreate(textView));
    }

    private void setTextWidthHeight(TextView textView, int widthPx, int heightPx) {
//        CoordinatorLayout.LayoutParams
        ViewKit.setWidthHeight(textView, widthPx, heightPx);
        showTextInfo(textView);
    }

}


