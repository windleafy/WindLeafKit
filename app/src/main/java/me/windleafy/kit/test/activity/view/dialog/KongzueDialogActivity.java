package me.windleafy.kit.test.activity.view.dialog;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Toast;

import com.kongzue.dialog.interfaces.OnDialogButtonClickListener;
import com.kongzue.dialog.util.BaseDialog;
import com.kongzue.dialog.util.DialogSettings;
import com.kongzue.dialog.v3.MessageDialog;

import me.windleafy.kit.R;
import me.windleafy.kity.android.wiget.debug.DebugView;
import me.windleafy.kity.android.wiget.toast.Toaster;

/**
 * 使用：https://github.com/orhanobut/dialogplus
 */
public class KongzueDialogActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dialog_plus);

        initDebug();
    }


    //放在initView中，这样屏蔽initDebug后，其它地方使用debugView的显示功能才不会报NULL
    protected void initDebug() {
        DebugView debugLayout = new DebugView(this);
        debugLayout.show(true);
        debugLayout.setVisibility(View.VISIBLE);
//        debugLayout.showSubText();
//        debugLayout.showEditLayout();

        String title = "title";
        String content = "content";
        String ok = "ok";
        String cancel = "cancel";
        //Button
        debugLayout.setDebugClickListener(new DebugView.DebugClickListener() {
            @Override
            public void onClick(View v, int index) {

                switch (index) {
                    case 1: //TOP
                        MessageDialog.show(KongzueDialogActivity.this, "title", "content");
                        break;

                    case 2: //CENTER
                        MessageDialog.build(KongzueDialogActivity.this)
                                .setStyle(DialogSettings.STYLE.STYLE_IOS)
                                .setTheme(DialogSettings.THEME.LIGHT)
                                .setTitle("定制化对话框")
                                .setMessage("我是内容")
                                .setOkButton("OK", new OnDialogButtonClickListener() {
                                    @Override
                                    public boolean onClick(BaseDialog baseDialog, View v) {
                                        Toast.makeText(KongzueDialogActivity.this, "点击了OK！", Toast.LENGTH_SHORT).show();
                                        return false;
                                    }
                                })
                                .show();
                        break;

                    case 3: //BOTTOM

                        break;

                    case 4: //VIEW
                        break;
                }
            }
        });

        debugLayout.setDebugSubClickListener(new DebugView.DebugSubClickListener() {
            @Override
            public void onClick(View v, int index) {
                Toaster.show("index = " + index);
                switch (index) {
                    case 1: //HEADER
                        break;
                    case 2:
                        break;
                    case 3:
                        break;
                    case 8:
                        break;
                }
            }
        });


    }

}
