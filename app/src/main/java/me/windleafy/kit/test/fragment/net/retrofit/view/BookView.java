package me.windleafy.kit.test.fragment.net.retrofit.view;

import me.windleafy.kit.test.fragment.net.retrofit.model.Book;

public interface BookView extends View {
    void onSuccess(Book mBook);
    void onError(String result);
}
