package me.windleafy.kit.test.activity.view.dialog;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.blankj.utilcode.util.ConvertUtils;
import com.orhanobut.dialogplus.DialogPlus;
import com.orhanobut.dialogplus.GridHolder;
import com.orhanobut.dialogplus.ListHolder;
import com.orhanobut.dialogplus.OnItemClickListener;
import com.zhy.adapter.abslistview.CommonAdapter;
import com.zhy.adapter.abslistview.ViewHolder;

import java.util.LinkedList;

import me.windleafy.kit.R;
import me.windleafy.kity.android.utils.InflaterKit;
import me.windleafy.kity.android.wiget.debug.DebugView;
import me.windleafy.kity.android.wiget.toast.Toaster;

/**
 * 使用：https://github.com/orhanobut/dialogplus
 */
public class DialogPlusActivity extends AppCompatActivity {


    private DialogPlus dialog;
    private LinkedList<String> list;
    private LinkedList<String> longList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dialog_plus);

        initDebug();
    }


    //放在initView中，这样屏蔽initDebug后，其它地方使用debugView的显示功能才不会报NULL
    protected void initDebug() {
        DebugView debugLayout = new DebugView(this);
        debugLayout.show(true);
        debugLayout.setVisibility(View.VISIBLE);
//        debugLayout.showSubText();
//        debugLayout.showEditLayout();
        debugLayout.setTitle("DialogPlus");

        list = new LinkedList<>();
        list.add("AAA");
        list.add("BBB");
        list.add("CCC");
        list.add("DDD");

        longList = new LinkedList<>();
        longList.add("AAA");
        longList.add("BBB");
        longList.add("CCC");
        longList.add("DDD");
        longList.add("EEEE");
        longList.add("FFFF");
        longList.add("GGGG");
        longList.add("HHHH");
        longList.add("IIIIII");
        longList.add("JJJJJJ");
        longList.add("KKKKKK");
        longList.add("LLLLLL");

        //Button
        debugLayout.setButtonText("TOP", "CENTER", "BOTTOM", "VIEW");
        debugLayout.setDebugClickListener(new DebugView.DebugClickListener() {
            @Override
            public void onClick(View v, int index) {

                switch (index) {
                    case 1: //TOP
                        dialog = DialogPlus.newDialog(DialogPlusActivity.this)
                                .setAdapter(new CommonAdapter<String>(DialogPlusActivity.this, R.layout.lv_item_linear_hor, list) {
                                    @Override
                                    protected void convert(ViewHolder viewHolder, final String item, int position) {
                                        viewHolder.setText(R.id.text, item);
                                    }
                                })
                                .setOnItemClickListener(new OnItemClickListener() {
                                    @Override
                                    public void onItemClick(DialogPlus dialog, Object item, View view, int position) {
                                        Toaster.show((String) item);
                                        dialog.dismiss();
                                    }
                                })
                                .setGravity(Gravity.TOP)
                                .setContentHolder(new ListHolder())
                                .setExpanded(true, ConvertUtils.dp2px(200))
                                .create();
                        dialog.show();
                        break;

                    case 2: //CENTER
                        dialog = DialogPlus.newDialog(DialogPlusActivity.this)
                                .setAdapter(new CommonAdapter<String>(DialogPlusActivity.this, R.layout.lv_item_linear_hor, list) {
                                    @Override
                                    protected void convert(ViewHolder viewHolder, final String item, int position) {
                                        viewHolder.setText(R.id.text, item);
                                    }
                                })
                                .setOnItemClickListener(new OnItemClickListener() {
                                    @Override
                                    public void onItemClick(DialogPlus dialog, Object item, View view, int position) {
                                        Toaster.show((String) item);
                                        dialog.dismiss();
                                    }
                                })
                                .setGravity(Gravity.CENTER)
                                .setContentHolder(new ListHolder())
                                .setExpanded(true, ConvertUtils.dp2px(200))
                                .create();
                        dialog.show();
                        break;

                    case 3: //BOTTOM
                        dialog = DialogPlus.newDialog(DialogPlusActivity.this)
                                .setAdapter(new CommonAdapter<String>(DialogPlusActivity.this, R.layout.lv_item_linear_ver, list) {
                                    @Override
                                    protected void convert(ViewHolder viewHolder, final String item, int position) {
                                        viewHolder.setText(R.id.text, item);
                                    }
                                })
                                .setOnItemClickListener(new OnItemClickListener() {
                                    @Override
                                    public void onItemClick(DialogPlus dialog, Object item, View view, int position) {
                                        Toaster.show((CharSequence) item);
                                        dialog.dismiss();
                                    }
                                })
                                .setContentHolder(new GridHolder(3))
                                .setExpanded(true, ConvertUtils.dp2px(200))
                                .create();
                        dialog.show();
                        break;

                    case 4: //VIEW
                        LinearLayout new_layout = new LinearLayout(DialogPlusActivity.this);
                        new_layout.setGravity(Gravity.CENTER);
                        new_layout.setOrientation(LinearLayout.HORIZONTAL);
                        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
                        new_layout.setLayoutParams(layoutParams);

                        ImageView imageView = new ImageView(DialogPlusActivity.this);
                        imageView.setBackgroundResource(R.drawable.bg_fifth);
                        LinearLayout.LayoutParams lp1 = new LinearLayout.LayoutParams(ConvertUtils.dp2px(30), ConvertUtils.dp2px(30));
                        lp1.setMargins(ConvertUtils.dp2px(15), ConvertUtils.dp2px(15), ConvertUtils.dp2px(15), ConvertUtils.dp2px(15));
                        imageView.setLayoutParams(lp1);
                        new_layout.addView(imageView);

                        TextView tv = new TextView(DialogPlusActivity.this); // 普通聊天对话
                        tv.setText("我和猫猫是新添加的");
                        tv.setBackgroundColor(Color.GRAY);
                        tv.setGravity(Gravity.CENTER);
                        LinearLayout.LayoutParams lp2 = new LinearLayout.LayoutParams(
                                ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                        tv.setLayoutParams(lp2);
                        new_layout.addView(tv);

                        dialog = DialogPlus.newDialog(DialogPlusActivity.this)
                                .setContentHolder(new com.orhanobut.dialogplus.ViewHolder(new_layout))
                                .setExpanded(false, ConvertUtils.dp2px(200))
                                .create();
                        dialog.show();
                        break;
                }
            }
        });

        debugLayout.setDebugSubClickListener(new DebugView.DebugSubClickListener() {
            @Override
            public void onClick(View v, int index) {
                Toaster.show("index = " + index);
                switch (index) {
                    case 1: //HEADER
                        dialog = DialogPlus.newDialog(DialogPlusActivity.this)
                                .setAdapter(new CommonAdapter<String>(DialogPlusActivity.this, R.layout.lv_item_linear_hor, list) {
                                    @Override
                                    protected void convert(ViewHolder viewHolder, final String item, int position) {
                                        viewHolder.setText(R.id.text, item);
                                    }
                                })
                                .setOnItemClickListener(new OnItemClickListener() {
                                    @Override
                                    public void onItemClick(DialogPlus dialog, Object item, View view, int position) {
                                        Toaster.show((String) item);
                                        dialog.dismiss();
                                    }
                                })
                                .setHeader(R.layout.lv_item_header)
                                .setFooter(InflaterKit.inflate(DialogPlusActivity.this,R.layout.lv_item_footer))
                                .setGravity(Gravity.TOP)
                                .setContentHolder(new ListHolder())
                                .setExpanded(true)
                                .create();
                        dialog.show();
                        break;
                    case 2:
                        dialog = DialogPlus.newDialog(DialogPlusActivity.this)
                                .setAdapter(new CommonAdapter<String>(DialogPlusActivity.this, R.layout.lv_item_linear_hor, list) {
                                    @Override
                                    protected void convert(ViewHolder viewHolder, final String item, int position) {
                                        viewHolder.setText(R.id.text, item);
                                    }
                                })
                                .setOnItemClickListener(new OnItemClickListener() {
                                    @Override
                                    public void onItemClick(DialogPlus dialog, Object item, View view, int position) {
                                        Toaster.show((String) item);
                                        dialog.dismiss();
                                    }
                                })
                                .setFooter(R.layout.lv_item_footer)
                                .setGravity(Gravity.BOTTOM)
                                .setContentHolder(new ListHolder())
                                .setExpanded(false)
                                .create();
                        dialog.show();
                        break;
                    case 3:
                        break;
                    case 8:
                        break;
                }
            }
        });


    }

}
