package me.windleafy.kit.test;

import android.app.Activity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.blankj.utilcode.util.ActivityUtils;

import java.util.ArrayList;

import me.windleafy.kit.R;
import me.windleafy.kit.demo_flow.adapter.ObjectAdapter;
import me.windleafy.kit.demo_flow.base.MySupportFragment;
import me.windleafy.kit.demo_flow.listener.OnItemClickListener;
import me.windleafy.kit.test.activity.arouter.ARouterActivity;
import me.windleafy.kit.test.activity.layout.dynamic.DynamicActivity;
import me.windleafy.kit.test.activity.material.MaterialActivity1;
import me.windleafy.kit.test.activity.mvvm.MVVMActivity;
import me.windleafy.kit.test.activity.orient.OrientationActivity;
import me.windleafy.kit.test.activity.orient.OrientationActivity2;
import me.windleafy.kit.test.activity.plugin.RePluginActivity;
import me.windleafy.kit.test.activity.sdk.umeng.PushActivity;
import me.windleafy.kit.test.activity.sdk.umeng.ShareActivity;
import me.windleafy.kit.test.activity.swipe.SwipeBackMainActivity;
import me.windleafy.kit.test.activity.view.debug.DebugViewActivity;
import me.windleafy.kit.test.activity.view.dialog.DialogPlusActivity;
import me.windleafy.kit.test.activity.view.dialog.KongzueDialogActivity;
import me.windleafy.kit.test.activity.view.loading.LoadingActivity;
import me.windleafy.kit.test.activity.view.loading.LoadingActivity2;
import me.windleafy.kit.test.activity.view.modify.ModifyConstraintActivity;
import me.windleafy.kit.test.activity.view.modify.ModifyViewActivity;
import me.windleafy.kit.test.activity.web.H5JsBridgeActivity;
import me.windleafy.kit.test.activity.web.RichTextActivity;
import me.windleafy.kit.test.activity.wiget.NotificationActivity;


public class TestActivity extends MySupportFragment {
    private static final String ARG_FROM = "arg_from";

    private int mFrom;

    private RecyclerView mRecy;
    private ObjectAdapter mAdapter;
    private ArrayList<Class<? extends Activity> > items;

    public static TestActivity newInstance(int from) {
        Bundle args = new Bundle();
        args.putInt(ARG_FROM, from);

        TestActivity fragment = new TestActivity();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle args = getArguments();
        if (args != null) {
            mFrom = args.getInt(ARG_FROM);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_pager, container, false);

        initView(view);

        return view;
    }

    private void initView(View view) {
        mRecy = (RecyclerView) view.findViewById(R.id.recy);

        mAdapter = new ObjectAdapter(_mActivity);
        LinearLayoutManager manager = new LinearLayoutManager(_mActivity);
        mRecy.setLayoutManager(manager);
        mRecy.setAdapter(mAdapter);

        mAdapter.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(int position, View view) {
                ActivityUtils.startActivity(getActivity(), items.get(position));
            }
        });

        mRecy.post(new Runnable() {
            @Override
            public void run() {
                // Init Datas
                items = new ArrayList<>();
                items.add(MVVMActivity.class);
                items.add(RePluginActivity.class);
                items.add( ARouterActivity.class);
                items.add(NotificationActivity.class);
                items.add( SwipeBackMainActivity.class);
                items.add(MaterialActivity1.class);
                items.add(RichTextActivity.class);
                items.add(PushActivity.class);
                items.add(ShareActivity.class);
                items.add( H5JsBridgeActivity .class);
                items.add(ModifyConstraintActivity .class);
                items.add(ModifyViewActivity .class);
                items.add(OrientationActivity2 .class);
                items.add(OrientationActivity .class);
                items.add( DialogPlusActivity .class);
                items.add( KongzueDialogActivity .class);
                items.add( DynamicActivity .class);
                items.add( LoadingActivity2.class);
                items.add(LoadingActivity.class);
//                items.add(new Object[]{"MaterialDesignActivity", MaterialDesignActivity.class});
                items.add( DebugViewActivity.class);

                mAdapter.setDatas(items);

            }
        });
    }
}
