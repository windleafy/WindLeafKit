package me.windleafy.kit.test.fragment.java.timer;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import me.windleafy.kit.R;
import me.windleafy.kit.demo_flow.base.BaseBackFragment;
import me.windleafy.kity.android.wiget.debug.DebugView;
import me.windleafy.kity.java.timer.TimerUnit;
import me.windleafy.kity.java.utils.TimeKit;

/**
 * 定时器测试
 */
public class TimeryFragment extends BaseBackFragment {

    public static final String TAG = TimeryFragment.class.getSimpleName();

    private static final String ARG_TITLE = "arg_title";

    private Toolbar mToolbar;
    private String mTitle;
    private DebugView debugLayout;
    private TimerUnit timerUnit;

    public static TimeryFragment newInstance(String title) {
        TimeryFragment fragment = new TimeryFragment();
        Bundle bundle = new Bundle();
        bundle.putString(ARG_TITLE, title);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle bundle = getArguments();
        if (bundle != null) {
            mTitle = bundle.getString(ARG_TITLE);
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_debug_view, container, false);
        initView(view);

        return view;
    }

    private void initView(View view) {
        mToolbar = (Toolbar) view.findViewById(R.id.toolbar);
        mToolbar.setTitle(mTitle);
        initToolbarNav(mToolbar);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initDebug();
    }

    protected void initDebug() {
        debugLayout = new DebugView(this, R.id.debug_container);
        debugLayout.show(true);
//        debugLayout.showEditLayout();

        //Button
        debugLayout.setDebugClickListener(new DebugView.DebugClickListener() {
            @Override
            public void onClick(View v, int index) {
                switch (index) {
                    case 1:
                        timerUnit = new TimerUnit(new TimerUnit.Task() {
                            @Override
                            public void prepare() {
                                debugLayout.setScrollText("prepare :  " + TimeKit.currentMillis());
                            }

                            @Override
                            public void period(long time, int num) {
                                debugLayout.setScrollText("period   :  " + TimeKit.currentMillis() + ",  currentTime=" + time + ",  num=" + num);
                            }

                            @Override
                            public void end(long time) {
                                debugLayout.setScrollText("end        :  " + TimeKit.currentMillis() + ",  currentTime=" + time);
                            }
                        }, 3000);
                        debugLayout.setScrollText("==== delay:3000 ====");
                        debugLayout.setScrollText("++++ start");
                        timerUnit.start();
                        break;

                    case 2:
                        timerUnit = new TimerUnit(new TimerUnit.Task() {
                            @Override
                            public void prepare() {
                                debugLayout.setScrollText("prepare" + TimeKit.currentMillis());
                            }

                            @Override
                            public void period(long time, int num) {
                                debugLayout.setScrollText("period" + TimeKit.currentMillis() + ", currentTime=" + time + ", num=" + num);
                            }

                            @Override
                            public void end(long time) {
                                debugLayout.setScrollText("end" + TimeKit.currentMillis() + ", currentTime=" + time);
                            }
                        }, 3000,2000);
                        debugLayout.setScrollText("==== delay:3000, period:2000 ====");
                        debugLayout.setScrollText("++++ start");
                        timerUnit.start();
                        break;

                    case 3:
                        timerUnit = new TimerUnit(new TimerUnit.Task() {
                            @Override
                            public void prepare() {
                                debugLayout.setScrollText("prepare" + TimeKit.currentMillis());
                            }

                            @Override
                            public void period(long time, int num) {
                                debugLayout.setScrollText("period" + TimeKit.currentMillis() + ", currentTime=" + time + ", num=" + num);
                            }

                            @Override
                            public void end(long time) {
                                debugLayout.setScrollText("end" + TimeKit.currentMillis() + ", currentTime=" + time);
                            }
                        }, 3000,2000, 5);
                        debugLayout.setScrollText("==== delay:3000, period:2000,  num:5 ====");
                        debugLayout.setScrollText("++++ start");
                        timerUnit.start();
                        break;
                    case 4:
                        debugLayout.setScrollText("++++ destroy");
                        timerUnit.destroy();
                        break;
                }
            }
        });

        //Button2
        debugLayout.setButtonText2("start","pause","stop","restart");
        debugLayout.setDebugClickListener2(new DebugView.DebugClickListener() {
            @Override
            public void onClick(View v, int index) {
                switch (index) {
                    case 1:
                        debugLayout.setScrollText("++++ start");
                        timerUnit.start();
                        break;
                    case 2:
                        debugLayout.setScrollText("++++ pause");
                        timerUnit.pause();
                        break;
                    case 3:
                        debugLayout.setScrollText("++++ stop");
                        timerUnit.stop();
                        break;
                    case 4:
                        debugLayout.setScrollText("++++ restart");
                        timerUnit.restart();
                        break;
                }
            }
        });

    }


}
