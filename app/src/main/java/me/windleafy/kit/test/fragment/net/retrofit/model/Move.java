package me.windleafy.kit.test.fragment.net.retrofit.model;

import java.util.List;

public class Move {

    private String title;
    private List<Subjects> subjects;


    public String getTitle() {
        return title;
    }

    public List<Subjects> getSubjects() {
        return subjects;
    }
}
