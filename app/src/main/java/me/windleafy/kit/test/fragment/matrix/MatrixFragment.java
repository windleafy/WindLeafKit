package me.windleafy.kit.test.fragment.matrix;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import me.windleafy.kit.R;
import me.windleafy.kit.demo_flow.base.BaseBackFragment;
import me.windleafy.kity.android.wiget.debug.DebugView;
import me.windleafy.kity.android.view.matrix.ViewArray;
import me.windleafy.kity.android.view.matrix.dynamic.ButtonArray;

public class MatrixFragment extends BaseBackFragment {

    public static final String TAG = MatrixFragment.class.getSimpleName();

    private static final String ARG_TITLE = "arg_title";

    private Toolbar mToolbar;
    private String mTitle;
    private DebugView debugLayout;

    public static MatrixFragment newInstance(String title) {
        MatrixFragment fragment = new MatrixFragment();
        Bundle bundle = new Bundle();
        bundle.putString(ARG_TITLE, title);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle bundle = getArguments();
        if (bundle != null) {
            mTitle = bundle.getString(ARG_TITLE);
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_matrix, container, false);
        initView(view);

        return view;
    }

    private void initView(View view) {
        mToolbar = (Toolbar) view.findViewById(R.id.toolbar);
        mToolbar.setTitle(mTitle);
        initToolbarNav(mToolbar);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initButtonArrayHor();
        initButtonArrayVer();
        initButtonArrayRel();
    }


    private void initButtonArrayHor(){
        String[] text = new String[]{"h1", "h2", "h3", "h4", "h5"};
        ButtonArray buttonArray = new ButtonArray(_mActivity, R.id.matrix_hor, text.length)
                .init(text);
        buttonArray.setItemOnClickListener(new ViewArray.ItemOnClickListener() {
            @Override
            public void onItemClick(int i) {
                switch (i) {
                    case 0:
                        break;
                    case 1:
                        break;
                    case 2:
                        break;
                    case 3:
                        break;

                }
            }
        });
    }

    private void initButtonArrayVer(){
        String[] text = new String[]{"v1", "v2", "v3", "v4", "v5"};
        ButtonArray buttonArray = new ButtonArray(_mActivity, R.id.matrix_ver, text.length)
                .init(text);
        buttonArray.setItemOnClickListener(new ViewArray.ItemOnClickListener() {
            @Override
            public void onItemClick(int i) {
                switch (i) {
                    case 0:
                        break;
                    case 1:
                        break;
                    case 2:
                        break;
                    case 3:
                        break;

                }
            }
        });
    }

    private void initButtonArrayRel() {

    }




}
