package me.windleafy.kit.test.activity.layout.dynamic;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Gravity;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.blankj.utilcode.util.ConvertUtils;

import me.windleafy.kit.R;

/**
 * 动态布局
 */
public class DynamicActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dynamic);

        testLinearLayout();

//        textRelativeLayout();

    }

    /**
     * 参考：https://blog.csdn.net/harvic880925/article/details/18042183
     */
    private void testLinearLayout() {


        LinearLayout layout_root = findViewById(R.id.layout_root);

        TextView textView1 = new TextView(this);
        TextView textView2 = new TextView(this);
        textView1.setText("TEXT1");
        textView1.setTextSize(30f);
        textView1.setTextColor(Color.RED);
        textView2.setText("TEXT2");
        textView2.setTextSize(50f);
        textView2.setTextColor(Color.GREEN);
        layout_root.addView(textView1);
        layout_root.addView(textView2);

        //xml添加新控件
        LinearLayout layout_container = findViewById(R.id.debug_container);
        TextView textView3 = new TextView(this);
        textView3.setText("TEXT3");
        layout_container.addView(textView3);

        //添加自定义Layout
        LinearLayout new_layout = new LinearLayout(this);
        new_layout.setGravity(Gravity.CENTER);
        new_layout.setOrientation(LinearLayout.HORIZONTAL);
        new_layout.setBackgroundResource(R.color.red_btn_bg_color);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        new_layout.setLayoutParams(layoutParams);

        ImageView imageView = new ImageView(this);
        imageView.setBackgroundResource(R.drawable.bg_fifth);
        LinearLayout.LayoutParams lp1 = new LinearLayout.LayoutParams(ConvertUtils.dp2px(30), ConvertUtils.dp2px(30));
        lp1.setMargins(ConvertUtils.dp2px(15), ConvertUtils.dp2px(15), ConvertUtils.dp2px(15), ConvertUtils.dp2px(15));
        imageView.setLayoutParams(lp1);
        new_layout.addView(imageView);

        TextView tv = new TextView(this); // 普通聊天对话
        tv.setText("我和猫猫是新添加的");
        tv.setBackgroundColor(Color.GRAY);
        tv.setGravity(Gravity.CENTER);
        LinearLayout.LayoutParams lp2 = new LinearLayout.LayoutParams(
                ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        tv.setLayoutParams(lp2);
        new_layout.addView(tv);


        layout_container.addView(new_layout);

    }

    /**
     * 参考：https://blog.csdn.net/harvic880925/article/details/24464537
     */
    private void textRelativeLayout() {

        final LinearLayout lin = findViewById(R.id.debug_container);
        LinearLayout.LayoutParams LP_FW = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.FILL_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        RelativeLayout newSingleRL = new RelativeLayout(this);

        for (int i = 0; i < 10; ) {
            newSingleRL = generateSingleLayout(i, "第" + (++i) + "个动态列表");
            lin.addView(newSingleRL, LP_FW);//全部用父结点的布局参数
        }
    }

    /**
     * 新建一个列表item
     *
     * @param imageID 新建imageView的ID值
     * @param str     TextView要显示的文字
     * @return 新建的单项布局变量
     */
    private RelativeLayout generateSingleLayout(int imageID, String str) {
        RelativeLayout layout_root_relative = new RelativeLayout(this);

        LinearLayout layout_sub_Lin = new LinearLayout(this);
        layout_sub_Lin.setBackgroundColor(Color.argb(0xff, 0x00, 0xff, 0x00));
        layout_sub_Lin.setOrientation(LinearLayout.VERTICAL);
        layout_sub_Lin.setPadding(5, 5, 5, 5);

        TextView tv = new TextView(this);
        LinearLayout.LayoutParams LP_WW = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        tv.setText(str);
        tv.setTextColor(Color.argb(0xff, 0x00, 0x00, 0x00));
        tv.setTextSize(20);
        tv.setLayoutParams(LP_WW);
        layout_sub_Lin.addView(tv);

        RelativeLayout.LayoutParams RL_MW = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT,
                RelativeLayout.LayoutParams.WRAP_CONTENT);//尤其注意这个位置，用的是父容器的布局参数
        RL_MW.setMargins(5, 5, 10, 5);
        RL_MW.addRule(RelativeLayout.LEFT_OF, imageID);
        layout_root_relative.addView(layout_sub_Lin, RL_MW);


        ImageView imageView = new ImageView(this);
        RelativeLayout.LayoutParams RL_WW = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT,
                RelativeLayout.LayoutParams.WRAP_CONTENT);
        imageView.setPadding(5, 5, 5, 5);
        RL_WW.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
        imageView.setLayoutParams(RL_WW);
        imageView.setClickable(true);
        imageView.setId(imageID);
        imageView.setImageResource(R.drawable.bg_fifth);
        layout_root_relative.addView(imageView);

        return layout_root_relative;

    }

}


