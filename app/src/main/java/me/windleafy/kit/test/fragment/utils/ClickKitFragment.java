package me.windleafy.kit.test.fragment.utils;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import me.windleafy.kit.R;
import me.windleafy.kit.demo_flow.base.BaseBackFragment;
import me.windleafy.kity.android.utils.ClickKit;
import me.windleafy.kity.android.utils.log.LogKit;
import me.windleafy.kity.android.wiget.debug.DebugView;
import me.windleafy.kity.android.wiget.toast.Toaster;

public class ClickKitFragment extends BaseBackFragment {

    public static final String TAG = ClickKitFragment.class.getSimpleName();

    private static final String ARG_TITLE = "arg_title";

    private Toolbar mToolbar;
    private String mTitle;
    private DebugView debugLayout;

    public static ClickKitFragment newInstance(String title) {
        ClickKitFragment fragment = new ClickKitFragment();
        Bundle bundle = new Bundle();
        bundle.putString(ARG_TITLE, title);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle bundle = getArguments();
        if (bundle != null) {
            mTitle = bundle.getString(ARG_TITLE);
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_debug_view, container, false);
        initView(view);

        return view;
    }

    private void initView(View view) {
        mToolbar = (Toolbar) view.findViewById(R.id.toolbar);
        mToolbar.setTitle(mTitle);
        initToolbarNav(mToolbar);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initDebug();
    }

    protected void initDebug() {
        debugLayout = new DebugView(this, R.id.debug_container);
        debugLayout.show(true);
//        debugLayout.showEditLayout();

        //Button
        debugLayout.setDebugClickListener(new DebugView.DebugClickListener() {
            @Override
            public void onClick(View v, int index) {
                LogKit.d("-----------------------------------------------------");
                switch (index) {
                    case 1:
                        boolean first = ClickKit.isFirstClick(v);
                        boolean ret = ClickKit.isLastTimeOut(v, 1000);
                        LogKit.d("first=" + first + ", ret=" + ret);
                        break;
                    case 2:
                        boolean ret2 = ClickKit.isLastTimeOut(v, 1000);
                        boolean first2 = ClickKit.isFirstClick(v);
                        LogKit.d("first=" + first2 + ", ret=" + ret2);
                        break;
                    case 3:
                        final boolean first3 = ClickKit.isFirstClick(v);
                        boolean ret3 = ClickKit.isLastTimeOut(v, 1000, new ClickKit.LastTimeOutCallback() {
                            @Override
                            public void lastTimeOut(boolean isTimeOut) {
                                LogKit.d("lastTimeOut isTimeOut=" + isTimeOut);
                            }

                            @Override
                            public void onClickLastTimeOut(boolean isTimeOut) {
                                LogKit.d("onClickLastTimeOut isTimeOut=" + isTimeOut);
                            }
                        });
                        LogKit.d("first=" + first3 + ", ret=" + ret3);
                        break;
                    case 4:
                        break;
                }
            }
        });

        //Button
        debugLayout.setDebugClickListener2(new DebugView.DebugClickListener() {
            @Override
            public void onClick(View v, int index) {
                switch (index) {
                    case 1:
                        break;
                    case 2:
                        break;
                    case 3:
                        break;
                    case 4:
                        break;
                }
            }
        });

        debugLayout.setDebugSubClickListener(new DebugView.DebugSubClickListener() {
            @Override
            public void onClick(View v, int index) {
                Toaster.show("index = " + index);
                switch (index) {
                    case 1:
                        break;
                    case 2:
                        break;
                    case 3:
                        break;
                    case 4:
                        break;
                    //........
                }
            }
        });


    }


}
