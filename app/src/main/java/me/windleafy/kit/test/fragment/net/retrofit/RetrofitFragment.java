package me.windleafy.kit.test.fragment.net.retrofit;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.gson.GsonBuilder;

import java.util.List;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import me.windleafy.kit.R;
import me.windleafy.kit.demo_flow.base.BaseBackFragment;
import me.windleafy.kit.test.fragment.net.retrofit.api.ApiMethods;
import me.windleafy.kit.test.fragment.net.retrofit.api.ApiService;
import me.windleafy.kit.test.fragment.net.retrofit.api.RetrofitService;
import me.windleafy.kit.test.fragment.net.retrofit.model.Book;
import me.windleafy.kit.test.fragment.net.retrofit.model.Movie;
import me.windleafy.kit.test.fragment.net.retrofit.model.Subjects;
import me.windleafy.kity.android.wiget.debug.DebugView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

/**
 * Created by YoKeyword on 16/2/3.
 */
public class RetrofitFragment extends BaseBackFragment {
    public static final String TAG = RetrofitFragment.class.getSimpleName();

    private static final String ARG_TITLE = "arg_title";

    private Toolbar mToolbar;
    private TextView mTvContent;
    private FloatingActionButton mFab;
    private String mTitle;
    private DebugView debugLayout;

    public static RetrofitFragment newInstance(String title) {
        RetrofitFragment fragment = new RetrofitFragment();
        Bundle bundle = new Bundle();
        bundle.putString(ARG_TITLE, title);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle bundle = getArguments();
        if (bundle != null) {
            mTitle = bundle.getString(ARG_TITLE);
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_retrofit, container, false);
        initView(view);

        return view;
    }

    private void initView(View view) {
        mToolbar = (Toolbar) view.findViewById(R.id.toolbar);

        mToolbar.setTitle(mTitle);

        initToolbarNav(mToolbar);

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initDebug();
    }

    //放在initView中，这样屏蔽initDebug后，其它地方使用debugView的显示功能才不会报NULL
    protected void initDebug() {
        debugLayout = new DebugView(this, R.id.debug_container);
        debugLayout.show(true);
//        debugLayout.setTitle("Learn Retrofit");
//        debugLayout.showEditLayout();

        //Button
        debugLayout.setButtonText("Call.enqueue", "ApiService", "ApiMethods", "");
        debugLayout.setDebugClickListener(new DebugView.DebugClickListener() {
            @Override
            public void onClick(View v, int index) {
                switch (index) {
                    case 1:
                        Retrofit retrofit = new Retrofit.Builder()
                                .baseUrl("https://api.douban.com/v2/movie/")
                                .addConverterFactory(GsonConverterFactory.create(new GsonBuilder().create()))
                                .build();
                        RetrofitService service = retrofit.create(RetrofitService.class);
                        Call<Book> call = service.getSearchBook("123", null, 0, 1);
                        call.enqueue(new Callback<Book>() {
                            @Override
                            public void onResponse(@NonNull Call<Book> call, @NonNull Response<Book> response) {
                                debugLayout.setScrollText(response.body() != null ? response.body().toString() : "null");
                            }

                            @Override
                            public void onFailure(@NonNull Call<Book> call, @NonNull Throwable t) {
                            }
                        });
                        break;

                    case 2:
                        String baseUrl = "https://api.douban.com/v2/movie/";
                        Retrofit retrofit2 = new Retrofit.Builder()
                                .baseUrl(baseUrl)
                                .addConverterFactory(ScalarsConverterFactory.create())//请求结果转换为基本类型
                                .addConverterFactory(GsonConverterFactory.create())//请求的结果转为实体类
                                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())  //适配RxJava2.0, RxJava1.x则为RxJavaCallAdapterFactory.create()
                                .build();
                        ApiService apiService = retrofit2.create(ApiService.class);
                        apiService.getTopMovie(0, 10)
                                .subscribeOn(Schedulers.io())
                                .observeOn(AndroidSchedulers.mainThread())
                                .subscribe(new Observer<Movie>() {
                                    @Override
                                    public void onSubscribe(Disposable d) {
                                        debugLayout.setScrollText("onSubscribe: ");
                                    }

                                    @Override
                                    public void onNext(Movie movie) {
                                        debugLayout.setScrollText("onNext: " + movie.getTitle());
                                        List<Subjects> list = movie.getSubjects();
                                        for (Subjects sub : list) {
                                            debugLayout.setScrollText("onNext: " + sub.getId() + "," + sub.getYear() + "," + sub.getTitle());
                                        }
                                    }

                                    @Override
                                    public void onError(Throwable e) {
                                        debugLayout.setScrollText("onError: " + e.getMessage());
                                    }

                                    @Override
                                    public void onComplete() {
                                        debugLayout.setScrollText("onComplete: Over!");
                                    }
                                });
                        break;

                    case 3:
                        Observer<Movie> observer = new Observer<Movie>() {
                            @Override
                            public void onSubscribe(Disposable d) {
                                debugLayout.setScrollText("onSubscribe: ");
                            }

                            @Override
                            public void onNext(Movie movie) {
                                debugLayout.setScrollText("onNext: " + movie.getTitle());
                                List<Subjects> list = movie.getSubjects();
                                for (Subjects sub : list) {
                                    debugLayout.setScrollText("list: " + sub.getId() + "," + sub.getYear() + "," + sub.getTitle());
                                }
                            }

                            @Override
                            public void onError(Throwable e) {
                                debugLayout.setScrollText("onError: " + e.getMessage());
                            }

                            @Override
                            public void onComplete() {
                                debugLayout.setScrollText("onComplete: Over!");
                            }
                        };
                        ApiMethods.getTopMovie(observer, 0, 10);
                        break;

                    case 4:

                        break;
                }
            }
        });

        debugLayout.setDebugClickListener2(new DebugView.DebugClickListener() {
            @Override
            public void onClick(View v, int index) {
                switch (index) {
                    case 1:
                        break;
                    case 2:
                        break;
                    case 3:
                        break;
                    case 4:
                        break;
                }
            }
        });


    }

}
