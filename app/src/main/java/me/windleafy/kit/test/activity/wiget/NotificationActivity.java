package me.windleafy.kit.test.activity.wiget;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.media.AudioManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v4.app.NotificationCompat;
import android.view.KeyEvent;
import android.view.View;
import android.widget.RemoteViews;

import me.windleafy.kit.R;
import me.windleafy.kit.demo_flow.MainActivity;
import me.windleafy.kity.android.base.activity.BaseSupportActivity;
import me.windleafy.kity.android.utils.BackKit;
import me.windleafy.kity.android.utils.log.LogKit;
import me.windleafy.kity.android.wiget.debug.DebugView;
import me.windleafy.kity.android.wiget.toast.Toaster;

public class NotificationActivity extends BaseSupportActivity {


    private static final String ACTION_ADD = "ACTION_ADD";
    private static final String ACTION_SUB = "ACTION_SUB";
    public static String ACTION = "to_service";
    public static String KEY_USR_ACTION = "key_usr_action";
    public static final int ACTION_PRE = 0, ACTION_PLAY_PAUSE = 1, ACTION_NEXT = 2;

    private DebugView debugLayout;
    private AudioManager mAudioManager;
    private int currentVolume;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_debug_view);

        init();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(receiver);
    }

    public void init() {
        initView();
    }

    public void initView() {
        initDebug();

        initVoice();

        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(ACTION_ADD);
        intentFilter.addAction(ACTION_SUB);
        registerReceiver(receiver, intentFilter);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            showRemoteView3();
        }
    }

    private void initVoice() {
        //获取系统的Audio管理者
        mAudioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
        //最大音量
        int maxVolume = mAudioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC);
        debugLayout.setScrollText("maxVolume:" + maxVolume);
        //当前音量
        currentVolume = mAudioManager.getStreamVolume(AudioManager.STREAM_MUSIC);
        debugLayout.setScrollText("currentVolume:" + currentVolume);
    }

    private BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            final String action = intent.getAction();
            LogKit.d("action = " + action);
            if (ACTION.equals(action)) {
                int widget_action = intent.getIntExtra(KEY_USR_ACTION, -1);
                Toaster.show("" + widget_action);
                switch (widget_action) {
                    case ACTION_PRE:
                        LogKit.d("action_prev");
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toaster.show("action = " + action);
                            }
                        });
                        break;
                    case ACTION_PLAY_PAUSE:
                        LogKit.d("action_play");
                        break;
                    case ACTION_NEXT:
                        LogKit.d("action_next");
                        break;
                    default:
                        break;
                }
            }
        }
    };


    //放在initView中，这样屏蔽initDebug后，其它地方使用debugView的显示功能才不会报NULL
    protected void initDebug() {
        debugLayout = new DebugView(this);
        debugLayout.show(true);
        debugLayout.setVisibility(View.VISIBLE);
//        debugLayout.showEditLayout();

        //Button
        debugLayout.setButtonText("One", "Tow", "Three", "Four");
        debugLayout.setDebugClickListener(new DebugView.DebugClickListener() {
            @Override
            public void onClick(View v, int index) {
                switch (index) {
                    case 1:
                        show(NotificationActivity.this);
                        break;
                    case 2:
                        showRemoteView2();
                        break;
                    case 3:
                        break;
                    case 4:
                        break;
                }
            }
        });

        debugLayout.setDebugSubClickListener(new DebugView.DebugSubClickListener() {
            @Override
            public void onClick(View v, int index) {
                Toaster.show("index = " + index);
                switch (index) {
                    case 1:
                        mAudioManager.setStreamVolume(AudioManager.STREAM_MUSIC, ++currentVolume, 0);
                        currentVolume = mAudioManager.getStreamVolume(AudioManager.STREAM_MUSIC);
                        debugLayout.setScrollText("currentVolume:" + currentVolume);
                        break;
                    case 2:
                        mAudioManager.setStreamVolume(AudioManager.STREAM_MUSIC, --currentVolume, 0);
                        currentVolume = mAudioManager.getStreamVolume(AudioManager.STREAM_MUSIC);
                        debugLayout.setScrollText("currentVolume:" + currentVolume);
                        break;
                    case 3:
//                        mAudioManager.setStreamVolume(AudioManager.STREAM_SYSTEM, ++currentVolume, 0);
//                        currentVolume = mAudioManager.getStreamVolume(AudioManager.STREAM_MUSIC);
//                        debugLayout.setScrollText("currentVolume:" + currentVolume);
                        break;
                    case 4:
                        break;
                }
            }
        });


    }


    private static final String CHANNEL_ID = "channel_id";   //通道渠道id
    public static final String CHANEL_NAME = "chanel_name"; //通道渠道名称


    public void show(Context context) {
        NotificationChannel channel = null;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            //创建 通知通道  channelid和channelname是必须的（自己命名就好）
            channel = new NotificationChannel(CHANNEL_ID, CHANEL_NAME, NotificationManager.IMPORTANCE_DEFAULT);
            channel.enableLights(true);//是否在桌面icon右上角展示小红点
            channel.setLightColor(Color.GREEN);//小红点颜色
            channel.setShowBadge(false); //是否在久按桌面图标时显示此渠道的通知
        }
        Notification notification;
        //获取Notification实例   获取Notification实例有很多方法处理
        // 在此我只展示通用的方法（虽然这种方式是属于api16以上，但是已经可以了，毕竟16以下的Android机很少了，如果非要全面兼容可以用）
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            //向上兼容 用Notification.Builder构造notification对象
            notification = new Notification.Builder(context, CHANNEL_ID)
                    .setContentTitle("通知栏标题")
                    .setContentText("这是消息通过通知栏的内容")
                    .setWhen(System.currentTimeMillis())
                    .setColor(Color.parseColor("#FEDA26"))
                    .setSmallIcon(R.drawable.ic_avatar)
                    .setLargeIcon(BitmapFactory.decodeResource(context.getResources(), R.mipmap.img_finish))
                    .setTicker("巴士门")
                    .build();
        } else {
            //向下兼容 用NotificationCompat.Builder构造notification对象
            notification = new NotificationCompat.Builder(context)
//                    .setStyle(new Notification.BigPictureStyle().bigPicture())
                    .setContentTitle("通知栏标题")
                    .setContentText("这是消息通过通知栏的内容")
                    .setWhen(System.currentTimeMillis())
                    .setSmallIcon(R.drawable.ic_avatar)
                    .setColor(Color.parseColor("#FEDA26"))
                    .setLargeIcon(BitmapFactory.decodeResource(context.getResources(), R.mipmap.img_finish))
                    .setTicker("巴士门")
                    .build();
        }


        //发送通知
        int notifiId = 1;
        //创建一个通知管理器
        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            notificationManager.createNotificationChannel(channel);
        }
        notificationManager.notify(notifiId, notification);

    }


    private void showRemoteView2() {
        Intent intent = new Intent(this, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        PendingIntent contentIntent = PendingIntent.getActivity(this, 0, intent,
                PendingIntent.FLAG_UPDATE_CURRENT);

        RemoteViews remoteViews = new RemoteViews(getPackageName(),
                R.layout.layout_notification);
        remoteViews.setImageViewResource(R.id.image, R.mipmap.icon_close_tiny);
        remoteViews.setTextViewText(R.id.title, "我是一个有内涵的程序猿");
        remoteViews.setTextViewText(R.id.content, "你懂我的，作为新时代的程序员，要什么都会，上刀山下火海，背砖搬砖都要会，不然哈哈哈哈哈哈哈哈哈你就惨了");
        NotificationCompat.Builder builder = new NotificationCompat.Builder(this);
        builder.setSmallIcon(R.mipmap.icon_close_tiny);
        builder.setContent(remoteViews);
        builder.setContentIntent(contentIntent);
        builder.setCustomBigContentView(remoteViews);

        NotificationManager notificationManager = (NotificationManager) getSystemService(
                Context.NOTIFICATION_SERVICE);
        notificationManager.notify(111, builder.build());
    }


    @RequiresApi(api = Build.VERSION_CODES.O)
    private void showRemoteView3() {
        Intent intent = new Intent(this, MainActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);

        String id = "my_channel_01";
        CharSequence name = "channel";
        String description = "description";
        int importance = NotificationManager.IMPORTANCE_DEFAULT;
        NotificationChannel mChannel = new NotificationChannel(id, name, importance);

        mChannel.setDescription(description);
        mChannel.enableLights(true);
        mChannel.setLightColor(Color.RED);
        mChannel.enableVibration(true);
        mChannel.setVibrationPattern(new long[]{100, 200, 300, 400, 500, 400, 300, 200, 400});

        NotificationManager manager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        manager.createNotificationChannel(mChannel);

        RemoteViews remoteView = new RemoteViews(getPackageName(), R.layout.layout_notification);
        remoteView.setTextColor(R.id.text, Color.RED);
        remoteView.setTextViewText(R.id.text, "remote view demo");
        remoteView.setImageViewResource(R.id.image, R.drawable.ic_launcher);

        remoteView.setOnClickPendingIntent(R.id.notification, pendingIntent);

        remoteView.setOnClickPendingIntent(R.id.num1, getPendingIntent(ACTION_ADD));
        remoteView.setOnClickPendingIntent(R.id.num2, getPendingIntent(ACTION_SUB));

        Notification notification = new Notification.Builder(this, id)
                .setAutoCancel(false)
                .setContentTitle("title")
                .setContentText("describe")
                .setContentIntent(pendingIntent)
                .setSmallIcon(R.drawable.ic_launcher)
                .setOngoing(true)
                .setCustomContentView(remoteView)
                .setWhen(System.currentTimeMillis())
                .build();
        manager.notify(1, notification);
    }

    private PendingIntent getPendingIntent(String action) {
        return PendingIntent.getBroadcast(getApplicationContext(), 0, new Intent(action), PendingIntent.FLAG_UPDATE_CURRENT);
    }

    private PendingIntent getPendingIntent(Context context, int buttonId) {
        Intent intent = new Intent();
        intent.setClass(context, MainActivity.class);
        intent.addCategory(Intent.CATEGORY_ALTERNATIVE);
        intent.setData(Uri.parse("" + buttonId));
        return PendingIntent.getBroadcast(context, 0, intent, PendingIntent.FLAG_CANCEL_CURRENT);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        return BackKit.setDoubleBackPressedFinish(this, "", 1000, keyCode);
    }
}
