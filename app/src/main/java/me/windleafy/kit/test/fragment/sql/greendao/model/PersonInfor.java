//package me.windleafy.kit.test.fragment.sql.greendao.model;
//
//import org.greenrobot.greendao.annotation.Entity;
//import org.greenrobot.greendao.annotation.Id;
//import org.greenrobot.greendao.annotation.Index;
//import org.greenrobot.greendao.annotation.Generated;
//
//@Entity
//public class PersonInfor {
//    @Id(autoincrement = true)//设置自增长
//    private Long id;
//
//    @Index(unique = true)//设置唯一性
//    private String perNo;//人员编号
//
//    private String name;//人员姓名
//
//    private String sex;//人员姓名
//
//    private Integer age;//人员姓名
//
//    @Generated(hash = 1819252330)
//    public PersonInfor(Long id, String perNo, String name, String sex,
//            Integer age) {
//        this.id = id;
//        this.perNo = perNo;
//        this.name = name;
//        this.sex = sex;
//        this.age = age;
//    }
//
//    @Generated(hash = 1362534400)
//    public PersonInfor() {
//    }
//
//    public Long getId() {
//        return this.id;
//    }
//
//    public void setId(Long id) {
//        this.id = id;
//    }
//
//    public String getPerNo() {
//        return this.perNo;
//    }
//
//    public void setPerNo(String perNo) {
//        this.perNo = perNo;
//    }
//
//    public String getName() {
//        return this.name;
//    }
//
//    public void setName(String name) {
//        this.name = name;
//    }
//
//    public String getSex() {
//        return this.sex;
//    }
//
//    public void setSex(String sex) {
//        this.sex = sex;
//    }
//
//    public Integer getAge() {
//        return this.age;
//    }
//
//    public void setAge(Integer age) {
//        this.age = age;
//    }
//}
