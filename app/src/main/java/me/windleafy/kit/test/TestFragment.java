package me.windleafy.kit.test;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.lang.reflect.Method;
import java.util.LinkedList;

import me.windleafy.kit.R;
import me.windleafy.kit.demo_flow.adapter.TestAdapter;
import me.windleafy.kit.demo_flow.base.MySupportFragment;


public class TestFragment extends MySupportFragment {
    private static final String ARG_FROM = "arg_from";

    private int mFrom;

    private RecyclerView mRecy;
    private TestAdapter mAdapter;
    private LinkedList<TestAdapter.TestObj> items;

    public static TestFragment newInstance(int from) {
        Bundle args = new Bundle();
        args.putInt(ARG_FROM, from);

        TestFragment fragment = new TestFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle args = getArguments();
        if (args != null) {
            mFrom = args.getInt(ARG_FROM);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_pager, container, false);

        initView(view);

        return view;
    }

    private void initView(View view) {
        mRecy = (RecyclerView) view.findViewById(R.id.recy);

        mAdapter = new TestAdapter(_mActivity);
        LinearLayoutManager manager = new LinearLayoutManager(_mActivity);
        mRecy.setLayoutManager(manager);
        mRecy.setAdapter(mAdapter);

        mAdapter.setOnItemClickListener(new TestAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(int position, TestAdapter.TestObj obj) {
                try {
                    Method method = obj.gettClass().getDeclaredMethod("newInstance", String.class);
                    MySupportFragment fragment = (MySupportFragment) method.invoke(null, obj.getTitle());
                    startAtViewPager(fragment);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        mRecy.post(new Runnable() {
            @Override
            public void run() {
                // Init Datas
                items = new LinkedList<>();

//                items.add(new TestAdapter.TestObj("GreenDaoFragment", GreenDaoFragment.class));
//                items.add(new TestAdapter.TestObj("SQLiteFragment", SQLiteFragment.class));
//                items.add(new TestAdapter.TestObj("BugViewFragment", BugViewFragment.class));
//                items.add(new TestAdapter.TestObj("ClickKitFragment", ClickKitFragment.class));
//                items.add(new TestAdapter.TestObj("LoadingFragment", LoadingFragment.class));
//                items.add(new TestAdapter.TestObj("CustomViewFragment", CustomViewFragment.class));
//                items.add(new TestAdapter.TestObj("Download", DownloadFragment.class));
//                items.add(new TestAdapter.TestObj("ButtonFragment", ButtonFragment.class));
//                items.add(new TestAdapter.TestObj("SDCardFragment", SDCardFragment.class));
//                items.add(new TestAdapter.TestObj("GlideFragment", GlideFragment.class));
//                items.add(new TestAdapter.TestObj("TargetViewKitFragment4", TargetViewKitFragment4.class));
//                items.add(new TestAdapter.TestObj("TargetViewKitFragment3", TargetViewKitFragment3.class));
//                items.add(new TestAdapter.TestObj("TargetViewKitFragment2", TargetViewKitFragment2.class));
//                items.add(new TestAdapter.TestObj("TargetViewKitFragment", TargetViewKitFragment.class));
//                items.add(new TestAdapter.TestObj("ViewKitFragment", ViewKitFragment.class));
//                items.add(new TestAdapter.TestObj("DynamicFragment", DynamicFragment.class));
//                items.add(new TestAdapter.TestObj("StyledDialog", StyledDialogFragment.class));
//                items.add(new TestAdapter.TestObj("MarqueeView", MarqueeViewFragment.class));
//                items.add(new TestAdapter.TestObj("SmartRefreshLayout", SmartRefreshFragment.class));
//                items.add(new TestAdapter.TestObj("DialogKit", DialogKitFragment.class));
//                items.add(new TestAdapter.TestObj("AlertDialog", AlertDialogFragment.class));
//                items.add(new TestAdapter.TestObj("ImageTextMatrix", ImageTextMatrixFragment.class));
//                items.add(new TestAdapter.TestObj("Matrix", MatrixFragment.class));
//                items.add(new TestAdapter.TestObj("CacheDual", CacheDualFragment.class));
//                items.add(new TestAdapter.TestObj("CacheMemory", CacheMemoryFragment.class));
//                items.add(new TestAdapter.TestObj("CacheDisk", CacheDiskFragment.class));
//                items.add(new TestAdapter.TestObj("Timery", TimeryFragment.class));
//                items.add(new TestAdapter.TestObj("RxJava", RxJavaFragment.class));
//                items.add(new TestAdapter.TestObj("Retrofit", RetrofitFragment.class));
//                items.add(new TestAdapter.TestObj("DebugView", DebugViewFragment.class));
//                items.add(new TestAdapter.TestObj("ConstraintLayout", ConstraintLayoutFragment.class));
//                items.add(new TestAdapter.TestObj("MultAdapter", MultAdapterFragment.class));
//                items.add(new TestAdapter.TestObj("CommonAdapter", CommonAdapterFragment.class));
//                items.add(new TestAdapter.TestObj("SweetDialog", SweetDialogFragment.class));
//                items.add(new TestAdapter.TestObj("CycleFragment", CycleFragment.class));

//                Collections.reverse(items);
                mAdapter.setDatas(items);

            }
        });
    }


}
