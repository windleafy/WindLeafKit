package me.windleafy.kit.test.fragment.download;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.blankj.utilcode.util.PathUtils;
import com.liulishuo.filedownloader.BaseDownloadTask;
import com.liulishuo.filedownloader.FileDownloadListener;
import com.liulishuo.filedownloader.FileDownloader;

import java.io.File;

import me.windleafy.kit.R;
import me.windleafy.kit.demo_flow.base.BaseBackFragment;
import me.windleafy.kity.android.utils.log.LogKit;
import me.windleafy.kity.android.wiget.debug.DebugView;
import me.windleafy.kity.android.wiget.toast.Toaster;

/**
 * 测试模板Fragment
 */
public class DownloadFragment extends BaseBackFragment {

    public static final String TAG = DownloadFragment.class.getSimpleName();

    private static final String ARG_TITLE = "arg_title";

    private Toolbar mToolbar;
    private String mTitle;
    private DebugView debugLayout;

    public static DownloadFragment newInstance(String title) {
        DownloadFragment fragment = new DownloadFragment();
        Bundle bundle = new Bundle();
        bundle.putString(ARG_TITLE, title);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle bundle = getArguments();
        if (bundle != null) {
            mTitle = bundle.getString(ARG_TITLE);
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_debug_view, container, false);
        initView(view);

        return view;
    }

    private void initView(View view) {
        mToolbar = (Toolbar) view.findViewById(R.id.toolbar);
        mToolbar.setTitle(mTitle);
        initToolbarNav(mToolbar);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initDebug();
    }

    protected void initDebug() {
        debugLayout = new DebugView(this, R.id.debug_container);
        debugLayout.show(true);
//        debugLayout.showEditLayout();

        FileDownloader.setup(_mActivity);

        //Button
        debugLayout.setButtonText("按钮1", "按钮2");
        debugLayout.setDebugClickListener(new DebugView.DebugClickListener() {
            @Override
            public void onClick(View v, int index) {
                switch (index) {
                    case 1:
//                        String url = "http://www.xpk.jhcee.cn/jhgk-cpbg/cpbg_out/15982368174.docx";
                        String url = "http://www.jhcee.cn/jhgk-cpbg/cpbg_out/15982368174.docx";
//                        String url  ="http://cdn.llsapp.com/android/LLS-v4.0-595-20160908-143200.apk";

                        String path = PathUtils.getExternalDocumentsPath() + File.separator + "15982368174.docx";
//                        String path = "/storage/emulated/0/Android/data/me.windleafy.kit/cache/tmpdir1/15982368174.docx";

//                        String path = "/storage/emulated/0/Android/data/me.windleafy.kit/cache/tmpdir1/LLS-v4.0-595-20160908-143200.apk";
//                        String path = "/storage/emulated/0/Android/data/com.liulishuo.filedownloader.demo/cache/tmpdir1/LLS-v4.0-595-20160908-143200.apk";


                        LogKit.d("url:" + url);
                        LogKit.d("path:" + path);
                        LogKit.e("startDownload");
                        Toaster.show("downloading");
                        FileDownloader.getImpl()
                                .create(url)
                                .setPath(path)
                                .setListener(new FileDownloadListener() {
                                    @Override
                                    protected void pending(BaseDownloadTask task, int soFarBytes, int totalBytes) {

                                    }

                                    @Override
                                    protected void progress(BaseDownloadTask task, int soFarBytes, int totalBytes) {

                                    }

                                    @Override
                                    protected void completed(BaseDownloadTask task) {

                                    }

                                    @Override
                                    protected void paused(BaseDownloadTask task, int soFarBytes, int totalBytes) {

                                    }

                                    @Override
                                    protected void error(BaseDownloadTask task, Throwable e) {

                                    }

                                    @Override
                                    protected void warn(BaseDownloadTask task) {

                                    }
                                }).start();

                        break;
                    case 2:
                        break;
                    case 3:
                        break;
                    case 4:
                        break;
                }
            }
        });

        //Button
        debugLayout.setDebugClickListener2(new DebugView.DebugClickListener() {
            @Override
            public void onClick(View v, int index) {
                switch (index) {
                    case 1:
                        break;
                    case 2:
                        break;
                    case 3:
                        break;
                    case 4:
                        break;
                }
            }
        });

        debugLayout.setDebugSubClickListener(new DebugView.DebugSubClickListener() {
            @Override
            public void onClick(View v, int index) {
                Toaster.show("index = " + index);
                switch (index) {
                    case 1:
                        break;
                    case 2:
                        break;
                    case 3:
                        break;
                    case 4:
                        break;
                    //........
                }
            }
        });


    }


}
