package me.windleafy.kit.test.fragment.sql;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import me.windleafy.kit.R;
import me.windleafy.kit.demo_flow.base.BaseBackFragment;
import me.windleafy.kity.android.wiget.debug.DebugView;
import me.windleafy.kity.android.wiget.toast.Toaster;
import me.windleafy.kity.java.utils.RandomKit;

/**
 * 测试模板Fragment
 */
public class SQLiteFragment extends BaseBackFragment {

    public static final String TAG = SQLiteFragment.class.getSimpleName();

    private static final String ARG_TITLE = "arg_title";

    private Toolbar mToolbar;
    private String mTitle;
    private DebugView debugLayout;
    private SQLiteDatabase mDB;

    public static SQLiteFragment newInstance(String title) {
        SQLiteFragment fragment = new SQLiteFragment();
        Bundle bundle = new Bundle();
        bundle.putString(ARG_TITLE, title);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle bundle = getArguments();
        if (bundle != null) {
            mTitle = bundle.getString(ARG_TITLE);
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_debug_view, container, false);
        initView(view);

        return view;
    }

    private void initView(View view) {
        mToolbar = (Toolbar) view.findViewById(R.id.toolbar);
        mToolbar.setTitle(mTitle);
        initToolbarNav(mToolbar);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initDebug();
    }

    protected void initDebug() {
        debugLayout = new DebugView(this, R.id.debug_container);
        debugLayout.show(true);
//        debugLayout.showEditLayout();

        //Button
        debugLayout.setButtonText("创建");
        debugLayout.setDebugClickListener(new DebugView.DebugClickListener() {
            @Override
            public void onClick(View v, int index) {
                switch (index) {
                    case 1:
                        //依靠DatabaseHelper带全部参数的构造函数创建数据库
                        DatabaseHelper dbHelper = new DatabaseHelper(_mActivity, "windleaf_db", null, 1);
                        mDB = dbHelper.getWritableDatabase();
                        break;
                    case 2:

                        break;
                    case 3:

                        break;
                    case 4:

                        break;
                }
            }
        });

        //Button
        debugLayout.setButtonText2("增", "删", "查", "改");
        debugLayout.setDebugClickListener2(new DebugView.DebugClickListener() {
            @Override
            public void onClick(View v, int index) {
                switch (index) {
                    case 1:
                        //创建存放数据的ContentValues对象
                        ContentValues values = new ContentValues();
                        values.put("name", RandomKit.getRandom(99) + "");
                        //数据库执行插入命令
                        mDB.insert("user", null, values);
                        break;
                    case 2:
                        mDB.delete("user", "name=?", new String[]{"1"});
                        break;
                    case 3:
                        //创建游标对象
                        Cursor cursor = mDB.query("user", new String[]{"name"}, null, null, null, null, null);
                        //利用游标遍历所有数据对象
                        //为了显示全部，把所有对象连接起来，放到TextView中
                        String textview_data = "";
                        while (cursor.moveToNext()) {
                            String name = cursor.getString(cursor.getColumnIndex("name"));
                            textview_data = textview_data + "\n" + name;
                        }
                        debugLayout.setScrollText(textview_data);
                        // 关闭游标，释放资源
                        cursor.close();

                        break;
                    case 4:
                        ContentValues values2 = new ContentValues();
                        values2.put("name", String.valueOf(RandomKit.getRandom(99) + 1));
                        mDB.update("user", values2, "name = ?", new String[]{String.valueOf(RandomKit.getRandom(99) + 1)});

                        break;
                }
            }
        });

        debugLayout.setDebugSubClickListener(new DebugView.DebugSubClickListener() {
            @Override
            public void onClick(View v, int index) {
                Toaster.show("index = " + index);
                switch (index) {
                    case 1:
                        break;
                    case 2:
                        break;
                    case 3:
                        break;
                    case 4:
                        break;
                    //........
                }
            }
        });


    }


}
