package me.windleafy.kit.test.activity.view.modify;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.TextView;

import me.windleafy.gity.Git;
import me.windleafy.kit.R;
import me.windleafy.kity.android.utils.ScreenKit;
import me.windleafy.kity.android.utils.ViewKit;
import me.windleafy.kity.android.wiget.debug.DebugView;

/**
 * 动态布局，
 */
public class ModifyViewActivity extends AppCompatActivity {

    private DebugView debugLayout;
    private TextView mTextView1;
    private TextView mTextView2;
    private TextView mTextView3;
    private TextView mTextView4;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_modify_view);
        mTextView1 = findViewById(R.id.text1);
        mTextView2 = findViewById(R.id.text2);
        mTextView3 = findViewById(R.id.text3);
        mTextView4 = findViewById(R.id.text4);

        initDebug();
        initScreen();

//        debugLayout.setScrollText("onCreate showTextInfo");
//        showTextInfo(mTextView1);
//        showTextInfo(mTextView2);
//        showTextInfo(mTextView3);
//        showTextInfo(mTextView4);

        debugLayout.setScrollText("onCreate showTextInfoAtViewKit");
        showTextInfoAtViewKit(mTextView1);
        showTextInfoAtViewKit(mTextView2);
        showTextInfoAtViewKit(mTextView3);
        showTextInfoAtViewKit(mTextView4);
    }


    private void initScreen() {

        DisplayMetrics metrics = ScreenKit.getDisplayMetrics(ModifyViewActivity.this);

        debugLayout.setScrollText("width:" + metrics.widthPixels);
        debugLayout.setScrollText("height:" + metrics.heightPixels);
        debugLayout.setScrollText("widthDp:" + metrics.widthPixels / metrics.density);
        debugLayout.setScrollText("heightDp:" + metrics.heightPixels / metrics.density);
        debugLayout.setScrollText("density:" + metrics.density);
        debugLayout.setScrollText("scaledDensity" + metrics.scaledDensity);
        debugLayout.setScrollText("densityDpi:" + metrics.densityDpi);
    }


    //放在initView中，这样屏蔽initDebug后，其它地方使用debugView的显示功能才不会报NULL
    protected void initDebug() {
        debugLayout = new DebugView(this);
        debugLayout.show(true);
        debugLayout.setVisibility(View.VISIBLE);
//        debugLayout.showEditLayout();

        //Button
        debugLayout.setButtonText("mTextView1");
        debugLayout.setDebugClickListener(new DebugView.DebugClickListener() {
            @Override
            public void onClick(View v, int index) {
                switch (index) {
                    case 1:
                        showTextInfo(mTextView1);
                        break;
                    case 2:
                        showTextInfo(mTextView2);
                        break;
                    case 3:
                        showTextInfo(mTextView3);
                        break;
                    case 4:
                        showTextInfo(mTextView4);
                        break;
                }
            }
        });

//        debugLayout.setButtonText2("removeView");
        debugLayout.setDebugClickListener2(new DebugView.DebugClickListener() {
            @Override
            public void onClick(View v, int index) {
                switch (index) {
                    case 1:
                        setTextWidthHeight(mTextView1, Git.dp2px(200), Git.dp2px(50));
                        break;
                    case 2:
                        setTextWidthHeight(mTextView2, Git.dp2px(200), Git.dp2px(50));
                        break;
                    case 3:
                        setTextWidthHeight(mTextView3, Git.dp2px(200), Git.dp2px(50));
                        break;
                    case 4:
                        setTextWidthHeight(mTextView4, Git.dp2px(200), Git.dp2px(50));
                        break;
                }
            }
        });

        debugLayout.setDebugSubClickListener(new DebugView.DebugSubClickListener() {
            @Override
            public void onClick(View v, int index) {
                switch (index) {
                    case 1:
                        ViewKit.setLayoutInParent(mTextView1, 200, 10, 600, 700);
                        break;
                    case 2:
                        break;
                    case 3:
                        break;
                    case 4:
                        break;
                }
            }
        });


    }

    private void showTextInfo(TextView textView) {
        debugLayout.setScrollText("");
        debugLayout.setScrollText("getWidth:" + textView.getWidth());
        debugLayout.setScrollText("getHeight:" + textView.getHeight());
        debugLayout.setScrollText("getMeasuredWidth:" + textView.getMeasuredWidth());
        debugLayout.setScrollText("getMeasuredHeight:" + textView.getMeasuredHeight());
    }

    private void showTextInfoAtViewKit(TextView textView) {
        debugLayout.setScrollText("");
        debugLayout.setScrollText("getWidth:" + ViewKit.getWrapWidthOnCreate(textView));
        debugLayout.setScrollText("getHeight:" + ViewKit.getWrapHeightOnCreate(textView));
    }

    private void setTextWidthHeight(TextView textView, int widthPx, int heightPx) {
//        CoordinatorLayout.LayoutParams
        ViewKit.setWidthHeight(textView, widthPx, heightPx);
        showTextInfo(textView);
    }

}


