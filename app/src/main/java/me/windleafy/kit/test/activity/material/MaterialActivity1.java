package me.windleafy.kit.test.activity.material;

import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.widget.ImageView;
import android.widget.TextView;

import me.windleafy.kit.R;
import me.windleafy.kit.demo_flow.base.BaseMainFragment;
import me.windleafy.kity.android.base.activity.BaseSupportActivity;
import me.windleafy.kity.android.wiget.debug.DebugView;
import me.windleafy.kity.android.wiget.toast.Toaster;
import me.yokeyword.fragmentation.ISupportFragment;

public class MaterialActivity1 extends BaseSupportActivity {

    private DebugView debugLayout;

    public static final String TAG = MaterialActivity1.class.getSimpleName();

    private DrawerLayout mDrawer;
    private NavigationView mNavigationView;
    private TextView mTvName;   // NavigationView上的名字
    private ImageView mImgNav;  // NavigationView上的头像

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_material_design);

        initView();

    }


    public void initView() {

    }

    private void goLogin() {

    }



    @Override
    public void onBackPressedSupport() {

//        super.onBackPressedSupport();
        if (mDrawer.isDrawerOpen(GravityCompat.START)) {
            mDrawer.closeDrawer(GravityCompat.START);
        } else {
            ISupportFragment topFragment = getTopFragment();

            // 主页的Fragment
            if (topFragment instanceof BaseMainFragment) {
                mNavigationView.setCheckedItem(R.id.nav1);
            }

            if (getSupportFragmentManager().getBackStackEntryCount() > 1) {
                pop();
            } else {
//                Toaster.initCenter("back else");

                //启动双击返回
                startDoubleBackClick(new DoubleBackClick() {
                    @Override
                    public void onToast() {
                        Toaster.show("再点一次退出");
                    }

                    @Override
                    public void onExited() {
                        finish();
                    }
                });


            }
        }
    }

}
