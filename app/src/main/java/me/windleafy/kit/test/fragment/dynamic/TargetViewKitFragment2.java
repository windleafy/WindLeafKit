package me.windleafy.kit.test.fragment.dynamic;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import me.windleafy.kit.R;
import me.windleafy.kit.demo_flow.base.BaseBackFragment;
import me.windleafy.kity.android.utils.view.TargetView;
import me.windleafy.kity.android.utils.view.TargetViewKit;
import me.windleafy.kity.android.wiget.debug.DebugView;

public class TargetViewKitFragment2 extends BaseBackFragment {

    public static final String TAG = TargetViewKitFragment2.class.getSimpleName();

    private static final String ARG_TITLE = "arg_title";

    private Toolbar mToolbar;
    private String mTitle;
    private DebugView debugLayout;
    private Fragment fragment;



    public static TargetViewKitFragment2 newInstance(String title) {
        TargetViewKitFragment2 fragment = new TargetViewKitFragment2();
        Bundle bundle = new Bundle();
        bundle.putString(ARG_TITLE, title);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        fragment = this;
        Bundle bundle = getArguments();
        if (bundle != null) {
            mTitle = bundle.getString(ARG_TITLE);
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_targetviewkit2, container, false);
        initView(view);

        return view;
    }

    private void initView(View view) {

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initDebug();
    }

    protected void initDebug() {

        final View targetView = LayoutInflater.from(getActivity()).inflate(R.layout.view_target, null);
        ViewGroup container = getView().findViewById(R.id.container);
        container.addView(targetView);


        debugLayout = new DebugView(this, R.id.debug_container);
        debugLayout.show(true);
//        debugLayout.showEditLayout();


        //Button
        debugLayout.setButtonText("dynamic_layout_root", "layout_container", "layout_container2", "layout_container3");
        debugLayout.setDebugClickListener(new DebugView.DebugClickListener() {
            @Override
            public void onClick(View v, int index) {


                TargetView.AddType type = null;
                int targetId = 0;
                int resId = 0;

                switch (index) {
                    case 1:
                        type = TargetView.AddType.COVER;
                        targetId = R.id.dynamic_layout_root;
                        resId = R.layout.view_nodata_message;
                        break;
                    case 2:
                        type = TargetView.AddType.COVER;
                        targetId = R.id.layout_container;
                        resId = R.layout.view_nodata_message;
                        break;
                    case 3:
                        type = TargetView.AddType.COVER;
                        targetId = R.id.layout_container2;
                        resId = R.layout.view_nodata_message;
                        break;
                    case 4:
                        type = TargetView.AddType.COVER;
                        targetId = R.id.layout_container3;
                        resId = R.layout.view_nodata_message2;
                        break;
                }
                final int finalTargetId = targetId;
                final int finalResId = resId;
                TargetViewKit.addView(TargetViewKitFragment2.this, targetId, resId, type,null, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        TargetViewKit.removeView(TargetViewKitFragment2.this, finalTargetId, finalResId, TargetView.AddType.COVER
                        );
                    }
                });
            }
        });

        final TargetViewKitFragment3 targetViewKitFragment3 = TargetViewKitFragment3.newInstance("3");

        debugLayout.setButtonText2("addView11", "addView12", "addView21", "addView22");
        debugLayout.setDebugClickListener2(new DebugView.DebugClickListener() {
            final View resView = LayoutInflater.from(getActivity()).inflate(R.layout.view_res, null);
            @Override
            public void onClick(View v, int index) {
                switch (index) {
                    case 1:
                        TargetViewKit.addView(TargetViewKitFragment2.this, targetView, resView, TargetView.AddType.HEAD,null, new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                TargetViewKit.removeView(TargetViewKitFragment2.this, targetView, resView, TargetView.AddType.HEAD);
                            }
                        });
                        break;
                    case 2:
                        TargetViewKit.addView(TargetViewKitFragment2.this, targetView, resView, TargetView.AddType.TAIL, null,new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                TargetViewKit.removeView(TargetViewKitFragment2.this, targetView, resView, TargetView.AddType.TAIL);
                            }
                        });
                        break;
                    case 3:
//                        TargetViewKit.addView(TargetViewKitFragment2.this, targetView, R.layout.view_nodata_message, TargetView.AddType.COVER, new View.OnClickListener() {
//                            @Override
//                            public void onClick(View v) {
//                                TargetViewKit.removeView(TargetViewKitFragment2.this, targetView, R.layout.view_nodata_message, TargetView.AddType.COVER);
//                            }
//                        });
                        break;
                    case 4:
                        start(targetViewKitFragment3);
                        break;
                }
            }
        });


        debugLayout.setDebugSubClickListener(new DebugView.DebugSubClickListener() {
            @Override
            public void onClick(View v, int index) {
                switch (index) {
                    case 1:
                        TargetViewKit.addView(TargetViewKitFragment2.this, targetView, R.layout.view_nodata_message, TargetView.AddType.HEAD, null,new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                TargetViewKit.removeView(TargetViewKitFragment2.this, targetView, R.layout.view_nodata_message, TargetView.AddType.HEAD);
                            }
                        });
                        break;
                    case 2:
                        TargetViewKit.addView(TargetViewKitFragment2.this, targetView, R.layout.view_nodata_message, TargetView.AddType.TAIL, null,new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                TargetViewKit.removeView(TargetViewKitFragment2.this, targetView, R.layout.view_nodata_message, TargetView.AddType.TAIL);
                            }
                        });
                        break;
                    case 3:
                        TargetViewKit.addView(TargetViewKitFragment2.this, targetView, R.layout.view_nodata_message, TargetView.AddType.BEFORE, null,new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                TargetViewKit.removeView(TargetViewKitFragment2.this, targetView, R.layout.view_nodata_message, TargetView.AddType.BEFORE);
                            }
                        });
                        break;
                    case 4:
                        TargetViewKit.addView(TargetViewKitFragment2.this, targetView, R.layout.view_nodata_message, TargetView.AddType.AFTER,null, new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                TargetViewKit.removeView(TargetViewKitFragment2.this, targetView, R.layout.view_nodata_message, TargetView.AddType.AFTER);
                            }
                        });
                        break;
                    case 5:
                        TargetViewKit.addView(TargetViewKitFragment2.this, targetView, R.layout.view_nodata_message, TargetView.AddType.REPLACE, null,new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                TargetViewKit.removeView(TargetViewKitFragment2.this, targetView, R.layout.view_nodata_message, TargetView.AddType.REPLACE);
                            }
                        });
                        break;
                    case 6:
                        TargetViewKit.addView(TargetViewKitFragment2.this, targetView, R.layout.view_nodata_message2, TargetView.AddType.REPLACE,null, new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                TargetViewKit.removeView(TargetViewKitFragment2.this, targetView, R.layout.view_nodata_message2, TargetView.AddType.REPLACE);
                            }
                        });
                        break;
                    case 7:
                        TargetViewKit.addView(TargetViewKitFragment2.this, targetView, R.layout.view_nodata_message, TargetView.AddType.OVERLAP,null, new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                TargetViewKit.removeView(TargetViewKitFragment2.this, targetView, R.layout.view_nodata_message, TargetView.AddType.OVERLAP);
                            }
                        });
                        break;
                    case 8:
                        TargetViewKit.addView(TargetViewKitFragment2.this, targetView, R.layout.view_nodata_message2, TargetView.AddType.OVERLAP, null,new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                TargetViewKit.removeView(TargetViewKitFragment2.this, targetView, R.layout.view_nodata_message2, TargetView.AddType.OVERLAP);
                            }
                        });
                        break;
                }
            }
        });


    }

    @Override
    public void onDestroy() {
        super.onDestroy();
//        TargetViewKit.clear(this);
    }
}
