package me.windleafy.kit.test.activity.sdk.umeng;

import android.Manifest;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.Toast;

import com.umeng.socialize.ShareAction;
import com.umeng.socialize.UMShareAPI;
import com.umeng.socialize.UMShareListener;
import com.umeng.socialize.bean.SHARE_MEDIA;
import com.umeng.socialize.media.UMImage;

import me.windleafy.kit.R;
import me.windleafy.kity.android.base.activity.BaseSupportActivity;
import me.windleafy.kity.android.utils.ScreenKit;
import me.windleafy.kity.android.wiget.debug.DebugView;
import me.windleafy.kity.android.wiget.toast.Toaster;

/**
 * 友盟分享
 * https://developer.umeng.com/docs/66632/detail/66639
 */
public class ShareActivity extends BaseSupportActivity {

    private static final String TAG = ShareActivity.class.getSimpleName();

    private DebugView debugLayout;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_debug_view);

        init();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        UMShareAPI.get(this).onActivityResult(requestCode, resultCode, data);
    }

    public void init() {
        initView();
    }

    public void initView() {
        initPermission();
        initDebug();
        initScreen();
//        initWeb();


    }

    private void initPermission() {
        if (Build.VERSION.SDK_INT >= 23) {
            String[] mPermissionList = new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE,
                    Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.CALL_PHONE,
                    Manifest.permission.READ_LOGS, Manifest.permission.READ_PHONE_STATE,
                    Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.SET_DEBUG_APP,
                    Manifest.permission.SYSTEM_ALERT_WINDOW, Manifest.permission.GET_ACCOUNTS,
                    Manifest.permission.WRITE_APN_SETTINGS};
            ActivityCompat.requestPermissions(this, mPermissionList, 123);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
    }

    private void initScreen() {

        DisplayMetrics metrics = ScreenKit.getDisplayMetrics(ShareActivity.this);

        debugLayout.setScrollText("width:" + metrics.widthPixels);
        debugLayout.setScrollText("height:" + metrics.heightPixels);
        debugLayout.setScrollText("widthDp:" + metrics.widthPixels / metrics.density);
        debugLayout.setScrollText("heightDp:" + metrics.heightPixels / metrics.density);
        debugLayout.setScrollText("density:" + metrics.density);
        debugLayout.setScrollText("scaledDensity" + metrics.scaledDensity);
        debugLayout.setScrollText("densityDpi:" + metrics.densityDpi);
    }

    private UMShareListener shareListener = new UMShareListener() {
        /**
         * @param platform 平台类型
         * @descrption 分享开始的回调
         */
        @Override
        public void onStart(SHARE_MEDIA platform) {
        }

        /**
         * @descrption 分享成功的回调
         * @param platform 平台类型
         */
        @Override
        public void onResult(SHARE_MEDIA platform) {
            Toast.makeText(ShareActivity.this, "成功", Toast.LENGTH_LONG).show();
        }

        /**
         * @descrption 分享失败的回调
         * @param platform 平台类型
         * @param t 错误原因
         */
        @Override
        public void onError(SHARE_MEDIA platform, Throwable t) {
            Toast.makeText(ShareActivity.this, "失败" + t.getMessage(), Toast.LENGTH_LONG).show();
        }

        /**
         * @descrption 分享取消的回调
         * @param platform 平台类型
         */
        @Override
        public void onCancel(SHARE_MEDIA platform) {
            Toast.makeText(ShareActivity.this, "取消", Toast.LENGTH_LONG).show();
        }
    };


    //放在initView中，这样屏蔽initDebug后，其它地方使用debugView的显示功能才不会报NULL
    protected void initDebug() {
        debugLayout = new DebugView(this);
        debugLayout.show(true);
        debugLayout.setVisibility(View.VISIBLE);
//        debugLayout.showEditLayout();

        //Button
        debugLayout.setButtonText("One", "Tow", "Three", "Four");
        debugLayout.setDebugClickListener(new DebugView.DebugClickListener() {
            @Override
            public void onClick(View v, int index) {
                switch (index) {
                    case 1:
                        UMImage image = new UMImage(ShareActivity.this, R.mipmap.pic_no_collection);//资源文件
//                        UMImage thumb =  new UMImage(ShareActivity.this, R.mipmap.pic_no_collection);
//                        image.setThumb(thumb);
                        new ShareAction(ShareActivity.this)
                                .setPlatform(SHARE_MEDIA.QQ)//传入平台
//                                .withText("abc")
                                .withMedia(image)
                                .setCallback(shareListener)//回调监听器
                                .share();
                        break;
                    case 2:
                        UMImage image2 = new UMImage(ShareActivity.this, R.mipmap.pic_no_collection);//资源文件
//                        UMImage thumb =  new UMImage(ShareActivity.this, R.mipmap.launcher);
//                        image.setThumb(thumb);
                        new ShareAction(ShareActivity.this)
                                .setPlatform(SHARE_MEDIA.WEIXIN)//传入平台
                                .withMedia(image2)
                                .setCallback(shareListener)//回调监听器
                                .share();
                        break;
                    case 3:
                        break;
                    case 4:
                        break;
                }
            }
        });

        debugLayout.setDebugSubClickListener(new DebugView.DebugSubClickListener() {
            @Override
            public void onClick(View v, int index) {
                Toaster.show("index = " + index);
                switch (index) {
                    case 1:
                        break;
                    case 2:
                        break;
                    case 3:
                        break;
                    case 8:
                        break;
                }
            }
        });


    }


}
