package me.windleafy.kit.test.fragment.dialog;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import me.windleafy.kit.R;
import me.windleafy.kit.demo_flow.base.BaseBackFragment;
import me.windleafy.kity.android.wiget.debug.DebugView;
import me.windleafy.kity.android.wiget.dialog.DialogView;
import me.windleafy.kity.android.wiget.toast.Toaster;

/**
 * 测试模板Fragment
 */
public class DialogKitFragment extends BaseBackFragment {

    public static final String TAG = DialogKitFragment.class.getSimpleName();

    private static final String ARG_TITLE = "arg_title";

    private Toolbar mToolbar;
    private String mTitle;
    private DebugView debugLayout;

    public static DialogKitFragment newInstance(String title) {
        DialogKitFragment fragment = new DialogKitFragment();
        Bundle bundle = new Bundle();
        bundle.putString(ARG_TITLE, title);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle bundle = getArguments();
        if (bundle != null) {
            mTitle = bundle.getString(ARG_TITLE);
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_debug_view, container, false);
        initView(view);

        return view;
    }

    private void initView(View view) {
        mToolbar = (Toolbar) view.findViewById(R.id.toolbar);
        mToolbar.setTitle(mTitle);
        initToolbarNav(mToolbar);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initDebug();
    }

    protected void initDebug() {
        debugLayout = new DebugView(this, R.id.debug_container);
        debugLayout.show(true);
//        debugLayout.showEditLayout();

        //Button
        debugLayout.setButtonText("Center", "Bottom", "Top","Default");
        debugLayout.setDebugClickListener(new DebugView.DebugClickListener() {
            @Override
            public void onClick(View v, int index) {
                View view = getLayoutInflater().inflate(R.layout.dialog_kit_test, null);
                switch (index) {
                    case 1:
                        DialogView.initCenter(_mActivity, view).setCenter().show();
                        break;
                    case 2:
                        DialogView.initBottom(_mActivity, view).setBottom().show();
                        break;
                    case 3:
                        DialogView.initTop(_mActivity, view).setTop().show();
                        break;
                    case 4:
                        break;
                }
            }
        });

        //Button
        debugLayout.setButtonText2("setWidthHeight", "setPercentRate", "setPercentRate", "setPercentRate");
        debugLayout.setDebugClickListener2(new DebugView.DebugClickListener() {
            @Override
            public void onClick(View v, int index) {
                View view = getLayoutInflater().inflate(R.layout.dialog_kit_test, null);
                switch (index) {
                    case 1:
                        DialogView.initCenter(_mActivity, view).setDismiss(DialogView.DISMISS_ALWAYS).setWidthHeight(300, 200).show();
                        break;
                    case 2:
                        DialogView.initCenter(_mActivity, view).setDismiss(DialogView.DISMISS_TOUCH_DIALOG).setPercentRate(1, 0.6).show();
                        break;
                    case 3:
                        DialogView.initCenter(_mActivity, view).setDismiss(DialogView.DISMISS_TOUCH_OUTSIDE).setPercentRate(0.8, 0.6).show();
                        break;
                    case 4:
//                        DialogView.initCenter(_mActivity, view).setDismiss(DialogView.DISMISS_NEVER).setPercentRate(0.8,1).show();
                        DialogView.initCenter(_mActivity, view).setDismiss(DialogView.DISMISS_NEVER).setPercentRate(0.8).show();
                        break;
                }
            }
        });

        debugLayout.setDebugSubClickListener(new DebugView.DebugSubClickListener() {
            @Override
            public void onClick(View v, int index) {
                switch (index) {
                    case 1:
                        View view = getLayoutInflater().inflate(R.layout.dialog_kit_test, null);
                        View back = view.findViewById(R.id.back);
                        final DialogView dialogView = DialogView.initCenter(_mActivity, view)
                                .setDismiss(DialogView.DISMISS_NEVER)
                                .setWidthHeight(300, 200)
                                .setBackEnable(false);
                        dialogView.show();
                        back.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                dialogView.dismiss();
                            }
                        });
                        break;
                    case 2:
                        View view2 = getLayoutInflater().inflate(R.layout.dialog_kit_test, null);
                        View back2 = view2.findViewById(R.id.back);
                        final DialogView dialogView2 = DialogView.initCenter(_mActivity, view2)
                                .setDismiss(DialogView.DISMISS_NEVER)
                                .setWidth(300)
                                .setBackEnable(false)
                                .setOnBackPressed(new DialogView.OnBackPressListener() {
                                    @Override
                                    public void click() {
                                        Toaster.show("OnBackPressed");
                                    }
                                });
                        dialogView2.show();
                        back2.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                dialogView2.dismiss();
                            }
                        });
                        break;
                    case 3:
                        break;
                    case 4:
                        break;
                    //........
                }
            }
        });


    }


}
