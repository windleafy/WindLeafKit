package me.windleafy.kit.test.activity.view.loading;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

import me.windleafy.kit.R;
import me.windleafy.kity.android.utils.InflaterKit;
import me.windleafy.kity.android.view.matrix.ViewArray;
import me.windleafy.kity.android.view.matrix.dynamic.ButtonArray;
import me.windleafy.kity.android.wiget.layout.LayoutUtil;
import me.windleafy.kity.java.utils.DateKit;

public class LoadingActivity2 extends AppCompatActivity {

    private View view1;
    private View view2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_loading);

        view1 = InflaterKit.inflate(this, R.layout.inflate_loading_view1);
        view2 = InflaterKit.inflate(this, R.layout.inflate_loading_view2);


        initTestMaMatrix();
    }

    /**
     * 测试ViewMatrix
     */
    private void initTestMaMatrix() {

        String[] text = new String[]{"add view1", "add view2", "hide view1", "hide view2", "hideAll"};
        ButtonArray buttonArray = new ButtonArray(this, R.id.array_container, text.length)
                .init(text);
        buttonArray.setItemOnClickListener(new ViewArray.ItemOnClickListener() {
            @Override
            public void onItemClick(int i) {
                switch (i) {
                    case 0:
                        LayoutUtil.show(LoadingActivity2.this, view1);
                        break;
                    case 1:
                        LayoutUtil.show(LoadingActivity2.this, view2);
                        break;
                    case 2:
                        LayoutUtil.hide(view1);
                        break;
                    case 3:
                        LayoutUtil.hide(view2);
                        break;
                    case 4:
                        TextView category_txt = view1.findViewById(R.id.category_txt);
                        category_txt.setText( DateKit.time());
                        break;
                }
            }
        });

    }


}
