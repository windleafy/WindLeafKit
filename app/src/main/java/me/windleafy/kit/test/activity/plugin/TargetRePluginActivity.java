package me.windleafy.kit.test.activity.plugin;

import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;

import me.windleafy.kit.R;
import me.windleafy.kity.android.base.activity.BaseSupportActivity;
import me.windleafy.kity.android.utils.BackKit;
import me.windleafy.kity.android.wiget.debug.DebugView;
import me.windleafy.kity.android.wiget.toast.Toaster;

//@Route(path = "/Test/ARouterActivity")
public class TargetRePluginActivity extends BaseSupportActivity {


    private static final String ACTION_ADD = "ACTION_ADD";
    private static final String ACTION_SUB = "ACTION_SUB";
    public static String ACTION = "to_service";
    public static String KEY_USR_ACTION = "key_usr_action";
    public static final int ACTION_PRE = 0, ACTION_PLAY_PAUSE = 1, ACTION_NEXT = 2;

    private DebugView debugLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_debug_view);

        init();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    public void init() {
        initView();
    }

    public void initView() {
        initDebug();


    }


    //放在initView中，这样屏蔽initDebug后，其它地方使用debugView的显示功能才不会报NULL
    protected void initDebug() {
        debugLayout = new DebugView(this);
        debugLayout.show(true);
        debugLayout.setVisibility(View.VISIBLE);
//        debugLayout.showEditLayout();
        debugLayout.setTitle("TargetRePluginActivity");

        //Button
        debugLayout.setButtonText("One", "Two", "Three", "Four");
        debugLayout.setDebugClickListener(new DebugView.DebugClickListener() {
            @Override
            public void onClick(View v, int index) {
                switch (index) {
                    case 1:
                        break;
                    case 2:

                        break;
                    case 3:
                        break;
                    case 4:
                        break;
                }
            }
        });

        debugLayout.setDebugSubClickListener(new DebugView.DebugSubClickListener() {
            @Override
            public void onClick(View v, int index) {
                Toaster.show("index = " + index);
                switch (index) {
                    case 1:
                        break;
                    case 2:
                        break;
                    case 3:
                        break;
                    case 4:
                        break;
                }
            }
        });

    }


    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        return BackKit.setDoubleBackPressedFinish(this, "", 1000, keyCode);
    }
}
