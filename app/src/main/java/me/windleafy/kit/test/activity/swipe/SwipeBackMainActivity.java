package me.windleafy.kit.test.activity.swipe;

import android.content.Intent;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;

import me.windleafy.kit.R;
import me.windleafy.kit.demo_flow.SwipeBackSampleActivity;
import me.windleafy.kit.demo_flow.base.MySupportActivity;
import me.windleafy.kity.android.utils.ScreenKit;
import me.windleafy.kity.android.wiget.debug.DebugView;
import me.windleafy.kity.android.wiget.toast.Toaster;

public class SwipeBackMainActivity extends MySupportActivity {


    private DebugView debugLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_debug_view);

        init();
    }

    public void init() {
        initView();
    }

    public void initView() {
        initDebug();
        initScreen();
    }

    private void initScreen() {

        DisplayMetrics metrics = ScreenKit.getDisplayMetrics(this);

        debugLayout.setScrollText("width:" + metrics.widthPixels);
        debugLayout.setScrollText("height:" + metrics.heightPixels);
        debugLayout.setScrollText("widthDp:" + metrics.widthPixels / metrics.density);
        debugLayout.setScrollText("heightDp:" + metrics.heightPixels / metrics.density);
        debugLayout.setScrollText("density:" + metrics.density);
        debugLayout.setScrollText("scaledDensity" + metrics.scaledDensity);
        debugLayout.setScrollText("densityDpi:" + metrics.densityDpi);
    }


    //放在initView中，这样屏蔽initDebug后，其它地方使用debugView的显示功能才不会报NULL
    protected void initDebug() {
        debugLayout = new DebugView(this);
        debugLayout.show(true);
        debugLayout.setVisibility(View.VISIBLE);
//        debugLayout.showEditLayout();

        debugLayout.setText("SwipeBackMainActivity");

        //Button
        debugLayout.setButtonText("Swipe","Sample");
        debugLayout.setDebugClickListener(new DebugView.DebugClickListener() {
            @Override
            public void onClick(View v, int index) {
                switch (index) {
                    case 1:
                        startActivity(new Intent(SwipeBackMainActivity.this, SwipeBackTestActivity.class));
                        break;
                    case 2:
                        startActivity(new Intent(SwipeBackMainActivity.this, SwipeBackSampleActivity.class));
                        break;
                    case 3:
                        break;
                    case 4:
                        break;
                }
            }
        });

        debugLayout.setDebugSubClickListener(new DebugView.DebugSubClickListener() {
            @Override
            public void onClick(View v, int index) {
                Toaster.show("index = " + index);
                switch (index) {
                    case 1:
                        break;
                    case 2:
                        break;
                    case 3:
                        break;
                    case 8:
                        break;
                }
            }
        });


    }


}
