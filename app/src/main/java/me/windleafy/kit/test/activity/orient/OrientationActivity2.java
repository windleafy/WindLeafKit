package me.windleafy.kit.test.activity.orient;

import android.content.res.Configuration;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;

import me.windleafy.kit.R;
import me.windleafy.kity.android.base.activity.BaseSupportActivity;
import me.windleafy.kity.android.utils.ScreenKit;
import me.windleafy.kity.android.wiget.debug.DebugView;
import me.windleafy.kity.android.wiget.toast.Toaster;

public class OrientationActivity2 extends BaseSupportActivity {


    private DebugView debugLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_orientation2);

        init();
    }

    public void init() {
        initView();
    }

    public void initView() {
        initDebug();
//        initScreen();
    }

    private void initScreen() {

        DisplayMetrics metrics = ScreenKit.getDisplayMetrics(OrientationActivity2.this);

        debugLayout.setScrollText("width:" + metrics.widthPixels);
        debugLayout.setScrollText("height:" + metrics.heightPixels);
        debugLayout.setScrollText("widthDp:" + metrics.widthPixels / metrics.density);
        debugLayout.setScrollText("heightDp:" + metrics.heightPixels / metrics.density);
        debugLayout.setScrollText("density:" + metrics.density);
        debugLayout.setScrollText("scaledDensity" + metrics.scaledDensity);
        debugLayout.setScrollText("densityDpi:" + metrics.densityDpi);
    }


    //放在initView中，这样屏蔽initDebug后，其它地方使用debugView的显示功能才不会报NULL
    protected void initDebug() {
        debugLayout = new DebugView(this);
        debugLayout.show(true);
        debugLayout.setVisibility(View.VISIBLE);
//        debugLayout.showEditLayout();

        //Button
        debugLayout.setButtonText("PORTRAIT","LANDSCAPE","FULLSCREEN","NOT FULLSCREEN");
        debugLayout.setDebugClickListener(new DebugView.DebugClickListener() {
            @Override
            public void onClick(View v, int index) {
                switch (index) {
                    case 1:
                        ScreenKit.setPortrait(OrientationActivity2.this);
                        break;
                    case 2:
                        ScreenKit.setLandscape(OrientationActivity2.this);
                        break;
                    case 3:
                        ScreenKit.setFullScreen(OrientationActivity2.this);
                        break;
                    case 4:
                        ScreenKit.setNotFullScreen(OrientationActivity2.this);
                        break;
                }
            }
        });

        debugLayout.setDebugSubClickListener(new DebugView.DebugSubClickListener() {
            @Override
            public void onClick(View v, int index) {
                Toaster.show("index = " + index);
                switch (index) {
                    case 1:
                        debugLayout.setText("11111");
                        break;
                    case 2:
                        debugLayout.setText("2222");
                        break;
                    case 3:
                        debugLayout.setScrollText("1111111");
                        break;
                    case 4:
                        debugLayout.setScrollText("2222222");
                        break;
                }
            }
        });


    }


    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);

        if (ScreenKit.getOrientation(OrientationActivity2.this) == Configuration.ORIENTATION_PORTRAIT){
            Toaster.show("PORTRAIT");
//            setContentView(R.layout.activity_orientation);
        }else if (ScreenKit.getOrientation(OrientationActivity2.this)  == Configuration.ORIENTATION_LANDSCAPE){
            Toaster.show("LANDSCAPE");
//            setContentView(R.layout.activity_orientation_land);
        }
    }
}
