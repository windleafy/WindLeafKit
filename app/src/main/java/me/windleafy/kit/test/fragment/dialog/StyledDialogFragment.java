package me.windleafy.kit.test.fragment.dialog;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.hss01248.dialog.StyledDialog;
import com.hss01248.dialog.adapter.SuperLvHolder;
import com.hss01248.dialog.interfaces.MyDialogListener;

import java.util.Timer;
import java.util.TimerTask;

import me.windleafy.kit.R;
import me.windleafy.kit.demo_flow.base.BaseBackFragment;
import me.windleafy.kity.android.wiget.debug.DebugView;
import me.windleafy.kity.android.wiget.toast.Toaster;

/**
 * 测试模板Fragment
 */
public class StyledDialogFragment extends BaseBackFragment {

    public static final String TAG = StyledDialogFragment.class.getSimpleName();

    private static final String ARG_TITLE = "arg_title";

    private Toolbar mToolbar;
    private String mTitle;
    private DebugView debugLayout;

    public static StyledDialogFragment newInstance(String title) {
        StyledDialogFragment fragment = new StyledDialogFragment();
        Bundle bundle = new Bundle();
        bundle.putString(ARG_TITLE, title);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle bundle = getArguments();
        if (bundle != null) {
            mTitle = bundle.getString(ARG_TITLE);
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_debug_view, container, false);
        initView(view);

        return view;
    }

    private void initView(View view) {
        mToolbar = (Toolbar) view.findViewById(R.id.toolbar);
        mToolbar.setTitle(mTitle);
        initToolbarNav(mToolbar);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initDebug();
    }

    protected void initDebug() {
        debugLayout = new DebugView(this, R.id.debug_container);
        debugLayout.show(true);
//        debugLayout.showEditLayout();

        //Button
        debugLayout.setButtonText("buildLoading", "buildMdAlert", "buildIosAlertVertical", "buildIosAlert");
        debugLayout.setDebugClickListener(new DebugView.DebugClickListener() {
            @Override
            public void onClick(View v, int index) {
                switch (index) {
                    case 1:
                        StyledDialog.buildLoading().show();
                        break;
                    case 2:
                        StyledDialog.buildMdAlert("title", "msg", new MyDialogListener() {
                            @Override
                            public void onFirst() {
                                Toaster.show("first");
                            }

                            @Override
                            public void onSecond() {
                                Toaster.show("currentSecond");
                            }

                            @Override
                            public void onThird() {
                                Toaster.show("third");
                            }
                        })
                                .setBtnSize(20)
                                .setBtnText("i", "b", "3")
                                .show();
                        break;
                    case 3:

                        StyledDialog.buildIosAlertVertical( "titlelll", "msg",  new MyDialogListener() {
                            @Override
                            public void onFirst() {
                                Toaster.show("first");
                            }

                            @Override
                            public void onSecond() {
                                Toaster.show("currentSecond");
                            }

                            @Override
                            public void onThird() {
                                Toaster.show("third");
                            }
                        }).show();
                        break;
                    case 4:
                        StyledDialog.buildIosAlert("title", "福建物联网附近，。没跟我闺蜜我离开哥哥们，跟我讲哦交给我。给我赶集网则更为高级", new MyDialogListener() {
                            @Override
                            public void onFirst() {
                                Toaster.show("first");
                            }

                            @Override
                            public void onSecond() {
                                Toaster.show("currentSecond");
                            }


                        })
                                //.setBtnText("sure","cancle","hhhh")
                                .setBtnText("确定")
//                                .setBtnText("cancel", "copy")
                                .setBtnColor(R.color.dialogutil_ios_btntext_blue, 0, 0)

                                //.setForceWidthPercent(0.99f)
                                //.setForceHeightPercent(0.88f)
                                //.setBgRes(R.drawable.leak_canary_icon)
//                                .setCustomContentHolder(new CustomContentHolder(_mActivity))
                                .show();
                        break;
                }
            }
        });

        //Button
        debugLayout.setButtonText2("MdLoading", "buildProgress");
        debugLayout.setDebugClickListener2(new DebugView.DebugClickListener() {
            @Override
            public void onClick(View v, int index) {
                switch (index) {
                    case 1:
                        StyledDialog.buildMdLoading().show();
                        break;
                    case 2:
                        final ProgressDialog dialog2 = (ProgressDialog) StyledDialog.buildProgress("下载中...", false).show();
                        final int[] progress2 = {0};

                        final Timer timer2 = new Timer();
                        timer2.schedule(new TimerTask() {
                            @Override
                            public void run() {
                                progress2[0] += 10;
                                StyledDialog.updateProgress(dialog2, progress2[0], 100, "progress", false);
                                if (progress2[0] > 100) {
                                    timer2.cancel();
                                }
                            }
                        }, 500, 500);
                        break;
                    case 3:
                        break;
                    case 4:
                        break;
                }
            }
        });

        debugLayout.setDebugSubClickListener(new DebugView.DebugSubClickListener() {
            @Override
            public void onClick(View v, int index) {
                Toaster.show("index = " + index);
                switch (index) {
                    case 1:
                        break;
                    case 2:
                        break;
                    case 3:
                        break;
                    case 4:
                        break;
                    //........
                }
            }
        });


    }

    public class CustomContentHolder extends SuperLvHolder {
        public CustomContentHolder(Context context) {
            super(context);
        }

        @Override
        protected void findViews() {

        }

        @Override
        protected int setLayoutRes() {
            return R.layout.github_styled_dialog_view_custom_holder;
        }

        @Override
        public void assingDatasAndEvents(Context context, Object bean) {

        }
    }


}
