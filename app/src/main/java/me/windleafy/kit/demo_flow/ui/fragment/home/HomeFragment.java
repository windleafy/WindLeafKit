package me.windleafy.kit.demo_flow.ui.fragment.home;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.GravityCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import java.util.ArrayList;

import me.windleafy.kit.R;
import me.windleafy.kit.demo_flow.adapter.HomeAdapter;
import me.windleafy.kit.demo_flow.base.BaseMainFragment;
import me.windleafy.kit.demo_flow.entity.Article;
import me.windleafy.kit.demo_flow.listener.OnItemClickListener;
import me.windleafy.kit.demo_flow.ui.fragment.CycleFragment;
import me.windleafy.kit.test.fragment.adapter.CommonAdapterFragment;
import me.windleafy.kit.test.fragment.layout.ConstraintLayoutFragment;
import me.windleafy.kit.test.fragment.dialog.SweetDialogFragment;
import me.yokeyword.fragmentation.ISupportActivity;
import me.yokeyword.fragmentation.anim.DefaultHorizontalAnimator;
import me.yokeyword.fragmentation.anim.DefaultNoAnimator;
import me.yokeyword.fragmentation.anim.DefaultVerticalAnimator;


public class HomeFragment extends BaseMainFragment implements Toolbar.OnMenuItemClickListener {
    private static final String TAG = "Fragmentation";

    private String[] mTitles;

    private String[] mContents;

    private Toolbar mToolbar;
    private RecyclerView mRecy;
    private HomeAdapter mAdapter;

    private static final int mCount = 20;
    private LinearLayoutManager mRecyManager;

    public static HomeFragment newInstance() {
        return new HomeFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        initView(view);
//        动态改动 当前Fragment的动画
//        setFragmentAnimator(fragmentAnimator);
        return view;
    }

    @Override
    public boolean onMenuItemClick(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_anim:
                final PopupMenu popupMenu = new PopupMenu(_mActivity, mToolbar, GravityCompat.END);
                popupMenu.inflate(R.menu.frag_home_pop);
                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.action_anim_veritical:
                                ((ISupportActivity) _mActivity).setFragmentAnimator(new DefaultVerticalAnimator());
                                Toast.makeText(_mActivity, R.string.anim_v, Toast.LENGTH_SHORT).show();
                                break;
                            case R.id.action_anim_horizontal:
                                ((ISupportActivity) _mActivity).setFragmentAnimator(new DefaultHorizontalAnimator());
                                Toast.makeText(_mActivity, R.string.anim_h, Toast.LENGTH_SHORT).show();
                                break;
                            case R.id.action_anim_none:
                                ((ISupportActivity) _mActivity).setFragmentAnimator(new DefaultNoAnimator());
                                Toast.makeText(_mActivity, R.string.anim_none, Toast.LENGTH_SHORT).show();
                                break;
                        }
                        popupMenu.dismiss();
                        return true;
                    }
                });
                popupMenu.show();
                break;
        }
        return true;
    }

    private void initView(View view) {
        mToolbar = (Toolbar) view.findViewById(R.id.toolbar);
        mRecy = (RecyclerView) view.findViewById(R.id.recy);

        mTitles = getResources().getStringArray(R.array.array_title);
        mContents = getResources().getStringArray(R.array.array_content);

        mToolbar.setTitle(R.string.home);
        initToolbarNav(mToolbar, true);
        mToolbar.inflateMenu(R.menu.frag_home);
        mToolbar.setOnMenuItemClickListener(this);

        mAdapter = new HomeAdapter(_mActivity);
        mRecyManager = new LinearLayoutManager(_mActivity);
        mRecy.setLayoutManager(mRecyManager);
        mRecy.setAdapter(mAdapter);

        mAdapter.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(int position, View view) {
                switch (position) {
                    case 0:
                        start(DetailFragment.newInstance(mAdapter.getItem(position).getTitle()));
                        break;
                    case 1:
                        start(SweetDialogFragment.newInstance(mAdapter.getItem(position).getTitle()));
                        break;
                    case 2:
                        start(CommonAdapterFragment.newInstance(mAdapter.getItem(position).getTitle()));
                        break;
                    case 3:
                        start(ConstraintLayoutFragment.newInstance(mAdapter.getItem(position).getTitle()));
                        break;
                    case 4:
                        start(CycleFragment.newInstance(111));
                        break;

                }

            }
        });

        // Init Datas
        ArrayList<Article> articleList = new ArrayList<>();
        articleList.add(new Article("TITLE", "CONTENT 0"));
        articleList.add(new Article("SweetDialog", "Content 1 SweetDialog"));
        articleList.add(new Article("CommonAdapter", "Content 2 CommonAdapter"));
        articleList.add(new Article("ConstraintLayout", "Content 3 ConstraintLayout"));
        articleList.add(new Article("CycleFragment", "Content 4 CycleFragment"));

        mAdapter.setDatas(articleList);

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

    }

    @Override
    public void onEnterAnimationEnd(Bundle savedInstanceState) {
        super.onEnterAnimationEnd(savedInstanceState);

    }

    /**
     * 类似于 Activity的 onNewIntent()
     */
    @Override
    public void onNewBundle(Bundle args) {
        super.onNewBundle(args);

        Toast.makeText(_mActivity, args.getString("from"), Toast.LENGTH_SHORT).show();
    }
}
