package me.windleafy.kit.demo_flow.listener;

import android.view.View;

public interface OnItemClickListener {
    void onItemClick(int position, View view);
}