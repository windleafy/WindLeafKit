package me.windleafy.kit.demo_flow.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import me.windleafy.kit.R;
import me.windleafy.kit.demo_flow.base.MySupportFragment;
import me.windleafy.kit.demo_flow.listener.OnItemClickListener;

/**
 *
 */
public class TestAdapter extends RecyclerView.Adapter<TestAdapter.MyViewHolder> {
    private List<TestObj> mItems = new ArrayList<>();
    private LayoutInflater mInflater;

    private OnItemClickListener mClickListener;

    public TestAdapter(Context context) {
        this.mInflater = LayoutInflater.from(context);
    }

    public void setDatas(List<TestObj> items) {
        mItems.clear();
        mItems.addAll(items);
        notifyDataSetChanged();
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.lv_item_pager, parent, false);
        final MyViewHolder holder = new MyViewHolder(view);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int position = holder.getAdapterPosition();
                if (mClickListener != null) {
                    mClickListener.onItemClick(position, mItems.get(position));
                }
            }
        });
        return holder;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        String item = (String) mItems.get(position).getTitle();
        holder.tvTitle.setText(item);
    }

    @Override
    public int getItemCount() {
        return mItems.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        private TextView tvTitle;

        public MyViewHolder(View itemView) {
            super(itemView);
            tvTitle = (TextView) itemView.findViewById(R.id.tv_title);
        }
    }

    public void setOnItemClickListener(OnItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    public interface OnItemClickListener {
        void onItemClick(int position, TestObj obj);
    }


    public static class TestObj<T>{
        private String title;
        private Class<? extends MySupportFragment> tClass;

        public TestObj(String title, Class<? extends MySupportFragment> tClass) {
            this.title = title;
            this.tClass = tClass;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public Class<? extends MySupportFragment> gettClass() {
            return tClass;
        }

        public void settClass(Class<? extends MySupportFragment> tClass) {
            this.tClass = tClass;
        }
    }
}
